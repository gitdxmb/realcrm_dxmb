@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase">Danh sách Quyền Menu</h3>

    <div class="">
        <table class="table table-responsive table-hover table-striped table-bordered">
            <tr class="header-tr">
                <td class="bg-success"><strong>STT</strong></td>
                <td class="bg-success"><strong>Tên</strong></td>
                <td class="bg-success"><strong>Ngày tạo</strong></td>
                <td class="bg-success"><strong>Action</strong></td>
            </tr>
            @foreach ($a_Role as $a_val)
            <tr>
                <td>    {{ $a_val->stt }}</td>
                <td>    {{ $a_val->title }}</td>
                <td>    {{ $a_val->created_at }}</td>
                <td>
                    <?php
                        if($a_val->status == 1){
                    ?>
                    <a title="Edit" href="{{ route('web-menu-config') . '?id='.$a_val->id }}" class="not-underline">
                        <i class="fa fa-edit fw"></i>
                    </a>
                    <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},1,'status')" title="Cho vào thùng rác" class="not-underline">
                    <i class="fa fa-trash fa-fw text-danger"></i>
                    </a>&nbsp;

                    <?php }else if($a_val->status == 0){ ?>
                    <a href="javascript:GLOBAL_JS.v_fRecoverRow({{ $a_val->id }},'status')"  title="Edit" class="not-underline">
                        <i class="fa fa-upload fw"></i>
                    </a>&nbsp;
                    <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},0,'status')" title="Xóa vĩnh viễn" class="not-underline">
                        <i class="fa fa-trash-o fa-fw text-danger"></i>
                    </a>&nbsp;

                    <?php }?>
                </td>
            </tr>
        @endforeach
            
        
        </table>
        
              
    </div>

<!--Hidden input-->
<input type="hidden" name="tbl" id="tbl" value="role_menu_web">
<?php echo (empty($a_search)) ? $a_Role->render(): $a_Role->appends($a_search)->render();?>

@endsection
