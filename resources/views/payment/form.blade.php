@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase">Cập nhật thanh toán</h3>
<div class="alert alert-danger hide backend"></div>
<form id="fileupload" class="form-horizontal" method="post" action="" onSubmit="if(!confirm('Đồng ý cập nhật?')){return false;}">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="tbl" value="b_o_payments">
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="project" class="col-xs-12 col-sm-3 control-label text-left">Dự án</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select required style="width:300px" disabled class="form-control">
                    <option value="{{ $data->project['id'] }}">{{ $data->project['title'] }} ({{ $data->project['code'] }})</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="building" class="col-xs-12 col-sm-3 control-label text-left">Tòa</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select required style="width:300px" disabled class="form-control">
                    <option value="{{ $data->building['id'] }}">{{ $data->building['title'] }} ({{ $data->building['code'] }})</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="apartment" class="col-xs-12 col-sm-3 control-label text-left">Căn hộ</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select required style="width:300px" disabled class="form-control">
                    <option value="{{ $data->product['id'] }}">{{ $data->product['code'] }}</option>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="type" class="col-xs-12 col-sm-3 control-label text-left">Loại phiếu</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <div class="radio">
                    <label><input disabled type="radio" name="typeOfPayment" {{ $data->type_of_payment == 'C' ? 'checked' : '' }}>Phiếu Thu</label>
                </div>
                <div class="radio">
                    <label><input disabled type="radio" name="typeOfPayment" {{ $data->type_of_payment == 'D' ? 'checked' : '' }}>Phiếu Chi</label>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="bill_code" class="col-xs-12 col-sm-3 control-label text-left">Chọn Bill</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select style="width:300px" class="form-control" readonly id="bill_id" name="bill_id">
                    <option value="{{$data->bill['id']}}">{{$data->bill['bill_code']}}</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="customer_id" class="col-xs-12 col-sm-3 control-label text-left">Khách Hàng </label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select style="width:300px" class="form-control" readonly name="customer_id">
                    <option value="{{$data->customer_id}}">{{$data->CusName}}</option>
                </select>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="request_staff_id" class="col-xs-12 col-sm-3 control-label text-left">Nhân Viên </label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select style="width:300px" class="form-control" readonly name="request_staff_id">
                    <option value="{{$data->request_staff_id}}">{{$data->StaffName}}</option>
                </select>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="pb_note" class="col-xs-12 col-sm-3 control-label text-left">Mô Tả</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <textarea class="form-control pb_note" name="pb_note" rows="5" id="pb_note" placeholder="Mô Tả..."><?php echo isset($data->pb_note) ? $data->pb_note : "" ?></textarea>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="pb_note" class="col-xs-12 col-sm-3 control-label text-left">Ghi chú</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <textarea class="form-control" name="pb_note2" rows="5" id="pb_note2" placeholder="Ghi chú..."><?php echo isset($data->pb_note2) ? $data->pb_note2 : "" ?></textarea>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="pb_request_money" class="col-xs-12 col-sm-3 control-label text-left">Số Tiền yêu cầu</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input type="text" value="<?php echo isset($data->pb_request_money) ? number_format($data->pb_request_money,0,'.',' ') : "" ?>" class="form-control formatMoney" readonly/>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="pb_response_money" class="col-xs-12 col-sm-3 control-label text-left">Số Tiền Thu/Chi</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input required type="text" id="pb_response_money" name="pb_response_money" value="<?php echo isset($data->pb_response_money) ? number_format($data->pb_response_money,0,'.',' ') : "" ?>" class="form-control formatMoney" {{ $data->pb_status == 1 ? 'readonly' : '' }} />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="method_id" class="col-xs-12 col-sm-3 control-label text-left">Chọn Phương Thức TT</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select style="width:300px" class="form-control" name="method_id" id="methodPayment" required>
                    <option value="">Chọn Phương Thức TT</option>
                    @foreach ($payment_methods as $valPMMT)
                    <option code="{{$valPMMT->pm_code}}" value="{{$valPMMT->pm_id}}" <?php if(isset($data->method_id) && $data->method_id == $valPMMT->pm_id ) echo"selected"; ?>>{{$valPMMT->pm_title}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="bank_id" class="col-xs-12 col-sm-3 control-label text-left">Chọn ngân hàng</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select style="width:300px" class="form-control js-select2" name="bank_id" id="bankCode">
                    <option value="">Chọn Ngân hàng</option>
                    @foreach ($bank_info as $bank)
                        <option value="{{$bank->id}}" <?php if(isset($data->bank_id) && $data->bank_id == $bank->id ) echo"selected"; ?>>{{$bank->bank_code}} - {{ $bank->bank_title }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="pb_bank_number_id" class="col-xs-12 col-sm-3 control-label text-left">Số Tài Khoản</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="pb_bank_number_id" readonly name="pb_bank_number_id" type="text" value="<?php echo isset($data->pb_bank_number_id) ? $data->pb_bank_number_id : "" ?>" class="form-control" placeholder="Số Tài Khoản" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="reference_code" class="col-xs-12 col-sm-3 control-label text-left">Code trên tavico</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="reference_code" name="reference_code" type="text" class="form-control" value="<?php echo isset($data->reference_code) ? $data->reference_code : "" ?>"/>
            </div>
        </div>
    </div>
    @if ($data->pb_status != -1 && $is_accountant)
    <div class="form-group">
        <div class="col-xs-6 col-sm-3 no-padding text-right">
            <input type="submit" name="submit" value="Cập nhật" class="btn btn-primary btn-sm submit">
            @if($data->pb_status == 0)
            <input type="button" value="Từ chối" class="btn btn-danger btn-sm" data-id="{{ $data->id }}" id="paymentDeny">
            @endif
        </div>
    </div>
    @endif
</form>

<br>
<?php if(count($data->pb_images)) { ?>
<div id="myCarousel" class="carousel slide" data-ride="carousel" style="max-width: 650px;">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php foreach ($data->pb_images as $index => $image) { ?>
        <li data-target="#myCarousel" data-slide-to="{{ $index }}" class="{{ $index == 0 ? ' active' : '' }}"></li>
        <?php } ?>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php foreach ($data->pb_images as $index => $image) { ?>
        <div class="item {{ $index == 0 ? ' active' : '' }}">
            <img src="{{ $image }}" >
        </div>
        <?php } ?>
    </div>
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<?php } ?>
<br>
<br>
@endsection
