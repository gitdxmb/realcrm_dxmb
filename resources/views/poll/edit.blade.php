@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase"><?php echo $i_id == '' ? 'Thêm Bình Chọn' : 'Sửa Bình Chọn' ?></h3>
<div class="alert alert-danger hide backend"></div>
<form id="fileupload" class="form-horizontal" method="post" action="">

    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id" value="<?php echo $i_id ?>">
    <input type="hidden" id="tbl" value="news">
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="title" class="col-xs-12 col-sm-3 control-label text-left">Tiêu đề</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="title" name="title" field-name="Tên" type="text" value="<?php echo isset($CustomerNoticeData->title) ? $CustomerNoticeData->title : "" ?>" class="form-control check-duplicate" placeholder="Tiêu đề" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="short_description" class="col-xs-12 col-sm-3 control-label text-left">Mô Tả ngắn</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <textarea class="form-control" name="short_description" rows="5" placeholder="Mô Tả Ngắn..."><?php echo isset($CustomerNoticeData->short_description) ? $CustomerNoticeData->short_description : "" ?></textarea>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-xs-12 col-sm-9 no-padding">
            <label for="description" class="col-xs-12 col-sm-1 control-label text-left">Mô Tả</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <textarea class="form-control description" name="description" rows="5" id="description" placeholder="Mô Tả..."><?php echo isset($CustomerNoticeData->description) ? $CustomerNoticeData->description : "" ?></textarea>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-xs-12 col-sm-12 no-padding">
            <label for="parent_id" class="col-xs-12 col-sm-1 control-label text-left">Chọn ảnh</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                <div class="row fileupload-buttonbar">
                    <div class="col-lg-7">
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <span class="btn btn-success fileinput-button">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>Add files...</span>
                            <input type="file" name="files[]" multiple>
                        </span>
                        <button type="submit" class="btn btn-primary start">
                            <i class="glyphicon glyphicon-upload"></i>
                            <span>Start upload</span>
                        </button>
                        <button type="reset" class="btn btn-warning cancel">
                            <i class="glyphicon glyphicon-ban-circle"></i>
                            <span>Cancel upload</span>
                        </button>
                        <button type="button" class="btn btn-danger delete">
                            <i class="glyphicon glyphicon-trash"></i>
                            <span>Delete</span>
                        </button>
                        <input type="checkbox" class="toggle">
                        <!-- The global file processing state -->
                        <span class="fileupload-process"></span>
                    </div>
                    <!-- The global progress state -->
                    <div class="col-lg-5 fileupload-progress fade">
                        <!-- The global progress bar -->
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                        </div>
                        <!-- The extended global progress state -->
                        <div class="progress-extended">&nbsp;</div>
                    </div>
                </div>
<!--                <strong style="color: red">(***) Lưu ý chỉ chọn 1 ảnh làm ảnh đại diện</strong>-->
                <!-- The table listing the files available for upload/download -->
                <table role="presentation" class="table table-striped"><tbody class="files">
                        <?php
                       
                        if (isset($CustomerNoticeData->images) && $CustomerNoticeData->images != '') {
                            foreach($CustomerNoticeData->images as $val){
                            ?>
                        <tr class="template-download fade in">
                                <td>
                                    <span class="preview">
                                        <a href="{{$val->img}}" title="{{$val->filename}}" download="{{$val->filename}}.jpg" data-gallery=""><img src="/server/php/files/thumbnail/{{$val->filename}}"></a>
                                    </span>
                                </td>
                                <td>
                                    <p class="name">
                                        <a href="{{$val->img}}" title="{{$val->filename}}" download="{{$val->filename}}" data-gallery="">{{$val->filename}}</a>
                                    </p>
                                </td>
                                <td>
                                    <span class="size">636.15 KB</span>
                                </td>
                                <td>
                                    <button class="btn btn-danger delete" data-type="DELETE" data-url="{{$val->deleteUrl}}">
                                        <i class="glyphicon glyphicon-trash"></i>
                                        <span>Delete</span>
                                    </button>
                                    <input type="checkbox" name="delete" value="1" class="toggle">
                                </td>
                            <input type="hidden" name="images[]" value="{{$val->img}}">
                            <input type="hidden"  name="filename[]" name="filename[]" value="{{$val->filename}}">
                            <input type="hidden"  name="deleteUrl[]" name="deleteUrl[]" value="{{$val->deleteUrl}}">
                        </tr>

                        <?php } } ?>
                    </tbody></table>
                <!-- The blueimp Gallery widget -->
                <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                    <div class="slides"></div>
                    <h3 class="title"></h3>
                    <a class="prev">‹</a>
                    <a class="next">›</a>
                    <a class="close">×</a>
                    <a class="play-pause"></a>
                    <ol class="indicator"></ol>
                </div>
                <!-- The template to display files available for upload -->
                <script id="template-upload" type="text/x-tmpl">
                    {% for (var i=0, file; file=o.files[i]; i++) { %}
                    <tr class="template-upload fade">
                    <td>
                    <span class="preview"></span>
                    </td>
                    <td>
                    <p class="name">{%=file.name%}</p>
                    <strong class="error text-danger"></strong>
                    </td>
                    <td>
                    <p class="size">Processing...</p>
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                    </td>
                    <td>
                    {% if (!i && !o.options.autoUpload) { %}
                    <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                    </button>
                    {% } %}
                    {% if (!i) { %}
                    <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                    </button>
                    {% } %}
                    </td>
                    </tr>
                    {% } %}
                </script>
                <!-- The template to display files available for download -->
                <script id="template-download" type="text/x-tmpl">
                    {% for (var i=0, file; file=o.files[i]; i++) { %}
                    <tr class="template-download fade">
                    <td>
                    <span class="preview">
                    {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                    {% } %}
                    </span>
                    </td>
                    <td>
                    <p class="name">
                    {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                    {% } else { %}
                    <span>{%=file.name%}</span>
                    {% } %}
                    </p>
                    {% if (file.error) { %}
                    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                    {% } %}
                    </td>
                    <td>
                    <span class="size">{%=o.formatFileSize(file.size)%}</span>
                    </td>
                    <td>
                    {% if (file.deleteUrl) { %}
                    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                    </button>
                    <input type="checkbox" name="delete" value="1" class="toggle">
                    {% } else { %}
                    <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                    </button>
                    {% } %}
                    </td>
                    <input type="hidden"  name="images[]" value="{%=file.url%}">
                    <input type="hidden"  name="filename[]" value="{%=file.name%}">
                    <input type="hidden"  name="deleteUrl[]" value="{%=file.deleteUrl%}">
                    </tr>
                    {% } %}
                </script>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="type" class="col-xs-12 col-sm-3 control-label text-left">Chọn Loại</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select class="form-control input-sm " id="type" name="type">
                    <option value="0"><span class="text-center">Chọn Loại</span></option>
                    @if(count($type_news) > 0)
                        @foreach($type_news as $key => $val )
                        <option value="{{$key}}" <?php echo isset($PollData->type) && $PollData->type == $key ? 'selected' : '' ?> > {{$val}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="category" class="col-xs-12 col-sm-3 control-label text-left">Chọn Danh Mục</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select class="form-control input-sm " id="category" name="category">
                    <option value="0"><span class="text-center">Chọn Danh Mục</span></option>
                    @if(count($category_news) > 0)
                        @foreach($category_news as $key => $val )
                        <option value="{{$key}}" <?php echo isset($PollData->category) && $PollData->category == $key ? 'selected' : '' ?> > {{$val}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="category" class="col-xs-12 col-sm-3 control-label text-left">Thêm bình chọn</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <div class="field_wrapper">
                    <div>
                        <input type="text" name="poll_option[]" value="" required>
                        <a href="javascript:void(0);" class="add_button" title="Add field"><img src="/add-icon.png"/></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="receive_user" class="col-xs-12 col-sm-3 control-label text-left">Nhân Viên Chỉ Định</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select class="form-control input-sm js-select2" multiple="multiple" id="receive_user" name="receive_user[]">
                    <option value="0"><span class="text-center">Chọn Nhân Viên</span></option>
                    @if(count($a_Users) > 0)
                        @foreach($a_Users as $key => $val )
                        <option value="{{$val->ub_id}}"> {{$val->ub_account_tvc}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="receive_groups" class="col-xs-12 col-sm-3 control-label text-left">Nhóm Chỉ Định</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select class="form-control input-sm js-select2" multiple="multiple"  id="receive_groups" name="receive_groups[]">
                    <option value="0"><span class="text-center">Nhóm Chỉ Định</span></option>
                    @if(count($a_userGroups) > 0)
                        @foreach($a_userGroups as $key => $val )
                        <option value="{{$val->rg_id}}"> {{$val->rg_title}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-6 col-sm-3 no-padding">
            <button type="reset" class="btn btn-default">Nhập lại</button>
            <input type="button" name="submit" VALUE="Cập nhật" class="btn btn-primary btn-sm " onclick="GLOBAL_JS.v_fSubmitProjectValidate()"/>
            <input type="submit" name="submit" class="btn btn-primary btn-sm hide submit">
        </div>
    </div>
</form>


@endsection


@section('cssfile')
<link rel="stylesheet" href="<?php echo URL::to('/'); ?>/upload_resource/css/style.css">
<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?php echo URL::to('/'); ?>/upload_resource/css/jquery.fileupload.css">
<link rel="stylesheet" href="<?php echo URL::to('/'); ?>/upload_resource/css/jquery.fileupload-ui.css">
<noscript><link rel="stylesheet" href="<?php echo URL::to('/'); ?>/upload_resource/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?php echo URL::to('/'); ?>/upload_resource/css/jquery.fileupload-ui-noscript.css"></noscript>
<!--<link href="<?php echo URL::to('/'); ?>/ckeditor/contents.css" rel="stylesheet">-->
@endsection


@section('jsfile')
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<!-- blueimp Gallery script -->
<script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/main.js"></script>


<script src="<?php echo URL::to('/'); ?>/ckeditor/ckeditor.js"></script>
<script src="<?php echo URL::to('/'); ?>/ckeditor/config.js"></script>
<script src="<?php echo URL::to('/'); ?>/ckeditor/styles.js"></script>.
<script src="<?php echo URL::to('/'); ?>/ckeditor/build-config.js"></script>
<script> CKEDITOR.replace('description');</script>
@endsection
