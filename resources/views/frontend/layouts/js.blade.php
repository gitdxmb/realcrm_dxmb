<script src="<?php echo URL::to('/');?>/frontend/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/js/jquery-ui.js"></script>
<!--loading bootstrap js-->
<script src="<?php echo URL::to('/');?>/frontend/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/js/html5shiv.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/js/respond.min.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/slimScroll/jquery.slimscroll.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/jquery-cookie/jquery.cookie.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/iCheck/icheck.min.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/iCheck/custom.min.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/jquery-notific8/jquery.notific8.min.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/jquery-highcharts/highcharts.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/js/jquery.menu.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/jquery-pace/pace.min.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/holder/holder.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/responsive-tabs/responsive-tabs.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/jquery-news-ticker/jquery.newsTicker.min.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/moment/moment.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!--CORE JAVASCRIPT-->
<script src="<?php echo URL::to('/');?>/frontend/js/main.js"></script>
<!--LOADING SCRIPTS FOR PAGE-->
<script src="<?php echo URL::to('/');?>/frontend/vendors/intro.js/intro.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/flot-chart/jquery.flot.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/flot-chart/jquery.flot.categories.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/flot-chart/jquery.flot.pie.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/flot-chart/jquery.flot.tooltip.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/flot-chart/jquery.flot.resize.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/flot-chart/jquery.flot.fillbetween.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/flot-chart/jquery.flot.stack.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/flot-chart/jquery.flot.spline.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/calendar/zabuto_calendar.min.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/sco.message/sco.message.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/vendors/intro.js/intro.js"></script>
<script src="<?php echo URL::to('/');?>/frontend/js/index.js"></script>
<script type="text/javascript">
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '../../../www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-145464-14', 'auto');
    ga('send', 'pageview');
</script>