@extends('layouts.admin.layoutAdmin')
@section('content')
    <?php
    $roleMenuWeb = \App\Http\Controllers\RoleController::getRoleMenuWebByUserId();
    ?>
    <div class="page-header">
        <div style="display: flex; justify-content: space-between;align-items: center;">
            <h2><i class="fa fa-newspaper-o"></i> Tin {{ $filter['category']==='0'? 'phòng ban' : ($filter['category']=='1'? 'dự án' : 'tức') }}</h2>
            @if(isset($roleMenuWeb['post']) && in_array('add', $roleMenuWeb['post']))
            <div class="text-right">
                <a href="{{ route('post-form') }}?category={{$filter['category']}}" class="btn btn-info"><i class="fa fa-edit"></i> Thêm mới</a>
            </div>
            @endif
        </div>
    </div>

    <form class="filterForm row" method="GET" autocomplete="off">
        <div class="col-md-3">
            <input type="text" name="keyword" class="form-control input-sm" placeholder="Nhập từ khóa..."
                   value="{{ $filter['keyword']?? '' }}"
            />
        </div>
        <div class="col-md-2">
            <select name="category" class="form-control input-sm">
                <option value="" selected>Chọn loại</option>
                <option value="0" {{ $filter['category']==='0'? 'selected' : '' }}>Tin phòng ban</option>
                <option value="1" {{ $filter['category']=='1'? 'selected' : '' }}>Tin dự án</option>
            </select>
        </div>
        <div class="col-md-2">
            <select name="priority" class="form-control input-sm">
                <option value="" selected>Mức ưu tiên</option>
                <option value="0" {{ $filter['priority']==='0'? 'selected' : '' }}>Không</option>
                <option value="1" {{ $filter['priority']=='1'? 'selected' : '' }}>Nổi bật</option>
                <option value="2" {{ $filter['priority']=='2'? 'selected' : '' }}>Ghim</option>
            </select>
        </div>
        @if(isset($projects))
            <div class="col-md-3">
                <select name="project" class="form-control js-select2" data-allow-clear="true" data-placeholder="Dự án liên quan">
                    <option value="" selected>Dự án liên quan</option>
                    @foreach($projects as $project)
                        <option value="{{ $project->id }}"{{ $filter['project']==$project->id? 'selected' : '' }}>{{ $project->name . ' ('. $project->code .')' }}</option>
                    @endforeach
                </select>
            </div>
        @endif
        @if($is_admin)
            <div class="col-md-3" style="margin-top: 10px;">
                <select name="user" class="form-control input-sm" id="userSelect2"></select>
            </div>
        @endif
        <div class="col-md-2">
            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Tìm kiếm</button>
        </div>
    </form>
    <br>
    <div>
        @if($errors->all())
            <div class="alert alert-danger backend">
                @foreach($errors->all() as $error)
                    <p><i class="fa fa-exclamation-triangle"></i> {{ $error }}</p>
                @endforeach
            </div>
        @endif
        @if(session()->get('message'))
            <div class="alert alert-success">
                <p>{{ session()->get('message') }}</p>
            </div>
        @endif
    </div>
    <div class="panel panel-info">
        <!-- Default panel contents -->
        <div class="panel-heading">Danh sách</div>
        <div class="panel-body">
            @if(isset($data))
                <!-- Table -->
                <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Tiêu đề</th>
                        <th class="text-center">Loại</th>
                        <th class="text-center">Hiển thị</th>
                        <th class="text-center">Ưu tiên</th>
                        <th class="text-center">Media</th>
                        <th class="text-center">Dự án liên quan</th>
                        <th class="text-center">Thẻ</th>
                        <th class="text-center">Loại phản hồi</th>
                        <th class="text-center">Tình trạng</th>
                        <th class="text-center">Người tạo</th>
                        <th class="text-center">Ngày tạo</th>
                        <th class="text-center">Last update</th>
                        <th width="80px" class="text-center">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $key => $item)
                    <tr class="text-center">
                        <td>{{ $data->total() - (($data->currentPage()-1) * $data->perPage() + $key) }}</td>
                        <td class="text-left">
                            <a href="{{ route('post-form', ['id' => $item->id]) }}">{{$item->title}}</a>
                        </td>
                        <td>
                            @if($item->category==1)
                                <p class="label label-warning">Tin dự án</p>
                            @else
                                <p class="label label-primary">Tin phòng ban</p>
                            @endif
                        </td>
                        <td>
                            @if($item->type==1)
                                <i title="Video" class="fa fa-video-camera text-danger"></i>
                            @elseif ($item->type==2)
                                <i title="Trích dẫn" class="fa fa-quote-left text-warning"></i>
                            @else
                                <i title="Tin ảnh" class="fa fa-image text-dark"></i>
                            @endif
                        </td>
                        <td>
                            @if($item->priority==2)
                                <p class="label label-danger">Ghim</p>
                            @elseif($item->priority==1)
                                <p class="label label-success">Nổi bật</p>
                            @else
                                <p class="label label-default">Không</p>
                            @endif
                        </td>
                        <td style="width: 90px;">
                            {!! $item->image? '<a class="fancybox" href="'.asset($item->image).'"><img class="img-responsive" src="'.asset($item->image).'" /></a>' : '(No avatar)' !!}
                            @if($item->video)
                                <p><a class="fancybox" href="{{ $item->video }}">Video <i class="fa fa-share"></i></a></p>
                            @endif
                        </td>
                        <td>
                            <span class="badge badge-pill">
                                {{ $item->bo_category_ids? count($item->bo_category_ids) : 0 }}
                            </span>
                        </td>
                        <td>
                            {{ $item->tags }}
                        </td>
                        <td>
                            <button type="button" class="btn btn-xs btn-outline btn-success"
                                    title="Danh sách phản hồi"
                                    data-url="{{ route('post-feedbacks', ['id' => $item->id ]) }}"
                                    data-toggle="modal" data-target="#feedbackModal">
                                {{ $item->feedback_type==2? 'Check-in' : ($item->feedback_type==1? 'Bình chọn' : 'Văn bản') }}
                            </button>
                            <span class="badge badge-success">{{ $item->post_feedbacks_count }}</span>
                        </td>
                        <td>
                            <small>{{ $item->status==1? 'Hiển thị' : ($item->status==0? 'Ẩn' : 'Xóa') }}</small>
                        </td>
                        <td>
                            @if($item->author)
                                <a target="_blank"
                                   class="small"
                                   href="{{ route('user-form') . '?id=' . $item->author->id }}">
                                    {{ $item->author->ub_title }}
                                </a>
                            @else
                                (N.A)
                            @endif
                        </td>
                        <td>
                            <small><i>{{ $item->created_at }}</i></small>
                        </td>
                        <td>
                            <small>{{ $item->updated_at }}</small>
                        </td>
                        <td>
                            <a href="{{ route('post-form', ['id' => $item->id]) }}" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                            <button data-role="remove" data-url="{{ route('post-delete', ['id'=>$item->id]) }}" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="3">
                            <p>{{ 'Xem: '.$data->perPage().' / Tổng số: ' . $data->total() }}</p>
                        </td>
                        <td colspan="10" class="text-right">
                            {{ $data->links() }}
                        </td>
                    </tr>
                    </tfoot>
                </table>
                </div>
            @else
                <div class="alert alert-warning">
                    <p>Không tìm thấy dữ liệu</p>
                </div>
            @endif
        </div>
    </div>

    <div id="feedbackModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Phản hồi bài viết</h4>
                </div>
                <div class="modal-body">
                    <div id="modalLoading" class="text-center hidden">
                        <p>Đang tải dữ liệu...</p>
                        @include('includes.loading')
                    </div>
                    <div id="modalContent"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('cssfile')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
@endsection


@section('jsfile')
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
    <script>
        function getFeedbackModalContent(post = null, info = {}) {
            if (!post) return '';
            let html = `<h4 class="text-primary"><i>${post.title}</i></h4>`;
            const feedbackCount = post.post_feedbacks.length;
            switch (post.feedback_type) {
                case 1:
                    const groupedFeedbacks = info.grouped_feedbacks;
                    html += '<h5 class="text-info">Bình chọn ('+feedbackCount+'):</h5>';
                    if (post.feedback_options&&post.feedback_options.length) {
                        const progressBarTypes = ['success', 'primary', 'info', 'warning', 'danger', 'secondary', 'success', 'primary', 'info', 'warning', 'danger', 'secondary'];
                        html += '<dl>';
                        $.each(post.feedback_options, (key, item) => {
                            let rate = 0, voteCount = 0;
                            if (feedbackCount > 0) {
                                voteCount = groupedFeedbacks[key]? groupedFeedbacks[key].length : 0;
                                rate = Math.round(voteCount/feedbackCount * 100);
                            }
                            html += `<dt>
                                <div class="row">
                                    <div class="col-xs-6 col-md-4 text-right">
                                        <p>${item}</p>
                                    </div>
                                    <div class="col-xs-6 col-md-8">${Custom.generateProgressBar(rate, `${voteCount} lượt (${rate}%)`, progressBarTypes[key])}</div>
                                </div>
                            </dt>`;
                        });
                        html += '</dl>';
                    } else {
                        html += '<p class="text-warning">Chưa có lựa chọn để vote!</p>';
                    }
                    break;
                case 2:
                    html += '<h5 class="text-info">Check-in (' + feedbackCount + '):</h5>';
                    break;
                default:
                    html += '<h5 class="text-info">Bình luận (' + feedbackCount + '):</h5>';
                    if (feedbackCount > 0) {
                        html += '<ul class="media-list">';
                        $.each(post.post_feedbacks, (key, feedback) => {
                            let avatar = '', title = '', comment = '';
                            if (feedback.created_by) {
                                avatar = feedback.created_by.ub_avatar;
                                title = feedback.created_by.ub_title + ' (' +feedback.created_by.ub_account_tvc+ ')';
                            }
                            comment += `<span class="label label-warning"><i class="fa fa-clock-o"></i> <i>${feedback.created_at}</i></span> ${feedback.comment}`;
                            html += Custom.generateMediaListItem(avatar, title, comment);
                        });
                        html += '</ul>';
                    } else {
                        html += '<p class="text-warning">Chưa có bình luận nào!</p>';
                    }
            }
            return html;
        }

        $(function () {
            $('button[data-role="remove"]').on('click', function () {
                let url = $(this).data('url');
                $.fancyConfirm({
                    title: 'Bạn có chắc muốn xóa bài viết?',
                    message: '',
                    okButton: 'Tiếp tục xóa',
                    noButton: 'Hủy bỏ',
                    callback: (value) => {
                        if (value) {
                            $.ajax(url, {
                                type: 'POST',
                                success: (response) => {
                                    console.log(response);
                                    if (response.success) {
                                        alert('Thành công!');
                                        location.reload();
                                    } else {
                                        alert(response.msg? response.msg : 'Xóa không thành công');
                                    }
                                },
                                error: (error) => {
                                    console.log(error);
                                    alert('Yêu cầu không hợp lệ!');
                                }
                            })
                        }
                    }
                });
            });

            $('[data-target="#feedbackModal"]').on('click', function (e) {
                e.preventDefault();
                const loading = $('#modalLoading');
                const content = $('#modalContent');
                content.empty().fadeOut();
                loading.fadeIn().removeClass('hidden');
                const url = $(this).data('url');
                let html = '';
                $.get(url, (response) => {
                    // console.log(response);
                    if (response.success) {
                        html = getFeedbackModalContent(response.data, response.info);
                    } else {
                        html = `<div class="alert alert-danger"><p>${response.msg? response.msg : 'Lỗi kết nối!'}</p></div>`;
                    }
                })
                    .fail(() => {
                        html = '<p class="text-danger">Server error!</p>';
                    })
                    .always(() => {
                        setTimeout(() => {
                            loading.fadeOut().addClass('hidden');
                            content.html(html).fadeIn();
                        }, 500);
                    });
            });
        });
    </script>
@endsection
