@extends('layouts.admin.layoutAdmin')
@section('content')

    <h3 class="col-xs-12 no-padding text-uppercase"><?php echo $investorId == 0 ? 'Thêm Chủ đầu tư' : 'Sửa chủ đầu tư' ?></h3>
    <div class="alert alert-danger hide backend"></div>
    <form id="fileupload" class="form-horizontal" method="post" action="">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="code" class="col-xs-12 col-sm-3 control-label text-left">Mã chủ đầu tư</label>
                <div class="col-xs-12 col-sm-9 no-padding">
                    <input id="code" name="code" type="text" value="<?php echo isset($investor) ? $investor->code : "" ?>" class="form-control check-duplicate" required/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="name" class="col-xs-12 col-sm-3 control-label text-left">Tên chủ đầu tư</label>
                <div class="col-xs-12 col-sm-9 no-padding">
                    <input id="name" name="name" field-name="Tên" type="text" value="<?php echo isset($investor) ? $investor->name : "" ?>" class="form-control" required />
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6 col-sm-3 no-padding text-right">
                <input type="submit" name="submit" class="btn btn-primary" value="Cập nhật">
            </div>
        </div>
    </form>
@endsection
