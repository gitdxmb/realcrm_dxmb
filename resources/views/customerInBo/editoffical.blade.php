@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase"><?php echo $i_id == 0 ? 'Thêm khách hàng' : 'Sửa khách hàng' ?></h3>
<div class="alert alert-danger hide backend"></div>
<form id="fileupload" class="form-horizontal" method="post" action="">

    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id" value="<?php echo $i_id ?>">
    <input type="hidden" id="tbl" value="b_o_customers">
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="name" class="col-xs-12 col-sm-3 control-label text-left">Tên Khách Hàng</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="cb_name" name="cb_name" field-name="Tên" type="text" value="<?php echo isset($customerData->cb_name) ? $customerData->cb_name : "" ?>" class="form-control check-duplicate" placeholder="Tên Khách Hàng" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="cb_phone" class="col-xs-12 col-sm-3 control-label text-left">Số điện thoại</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="cb_phone" name="cb_phone" field-name="Số điện thoại" type="text" value="<?php echo isset($customerData->cb_phone) ? $customerData->cb_phone : "" ?>" class="form-control check-duplicate" placeholder="Số Điện Thoại" required <?php echo $i_id == 0 ? '' : 'readonly' ?>/>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="cb_email" class="col-xs-12 col-sm-3 control-label text-left">Email</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="cb_phone" name="cb_email" field-name="cb_email" type="text" value="<?php echo isset($customerData->cb_email) ? $customerData->cb_email : "" ?>" class="form-control check-duplicate" placeholder="Email" required />
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="source_id" class="col-xs-12 col-sm-3 control-label text-left">Chọn nguồn</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select class="form-control input-sm " id="source_id" name="source_id">
                    <option value="0"><span class="text-center">Danh mục gốc</span></option>
                    @if(count($sourceCustomer) > 0)
                        @foreach($sourceCustomer as $key => $val )
                        <option value="{{$key}}" <?php echo isset($customerData->source_id) && $customerData->source_id == $key ? 'selected' : '' ?> > {{$val}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="cb_staff_tranfer" class="col-xs-12 col-sm-3 control-label text-left">Nhân viên đã phân bổ</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="cb_staff_tranfer" name="cb_staff_tranfer" multiple="multiple" class="js-select2" disabled="disabled">
                    @if(count($a_Saffs) > 0)
                        @foreach($a_Saffs as $key => $valStaff )
                        <option value="{{$valStaff->ub_id}}" <?php if(isset($strStaffTranfer) &&  in_array($valStaff->ub_id, $strStaffTranfer)) echo"selected"; ?>> {{$valStaff->ub_account_tvc}}</option>
                        @endforeach
                    @endif
                </select>

            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-3 no-padding">
            <label for="cb_status" class="col-xs-6 control-label text-left">Trạng thái</label>
            <div class="col-xs-6 no-left-padding">
                <input id="cb_status" name="cb_status" type="checkbox" class="form-control" <?php if ((isset($customerData->cb_status) && $customerData->cb_status) || $i_id == 0): ?>checked<?php endif ?>>
            </div>
        </div>
    </div>
    

    <div class="form-group">
        <div class="col-xs-6 col-sm-3 no-padding">
            <button type="reset" class="btn btn-default">Nhập lại</button>
            <input type="button" name="submit" VALUE="Cập nhật" class="btn btn-primary btn-sm " onclick="GLOBAL_JS.v_fSubmitProjectValidate()"/>
            <input type="submit" name="submit" class="btn btn-primary btn-sm hide submit">
        </div>
    </div>
</form>


@endsection
