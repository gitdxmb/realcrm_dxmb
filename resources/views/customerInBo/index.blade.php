@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase">Khách Hàng Của Tôi</h3>
<form method="get" action="" id="frmFilter" name="frmFilter"  class="form-inline">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    
    <div class="form-group">
        <input id="c_title" name="c_title" type="text" class="form-control input-sm" placeholder="Nhập tên" value="<?php echo isset($a_search['c_title'])?$a_search['c_title']:''?>">
    </div>
    
    <div class="form-group">
        <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
        <input type="submit" class="btn btn-success btn-sm submit hide">
    </div>
    <div class="form-group">
        <select class="form-control input-sm js-select2" id="staff_id" name="staff_id">
                    <option value=""><span class="text-center">Chọn Nhân Viên Phân Bổ</span></option>
                    @if(count($a_Saffs) > 0)
                        @foreach($a_Saffs as $key => $valStaff )
                        <option value="{{$valStaff->ub_id}}" <?php echo isset($customerData->cb_staff_id) && $customerData->cb_staff_id == $valStaff->ub_id ? 'selected' : '' ?> > {{$valStaff->ub_account_tvc}}</option>
                        @endforeach
                    @endif
                    
        </select>
    </div>
    <div class="form-group">
        <input type="button" class="btn btn-warning btn-sm" value="Phân Bổ" onclick="GLOBAL_JS.v_fTransferData()">
    </div>
</form>
    <div class="">
        <table class="table table-responsive table-hover table-striped table-bordered">
            <tr class="header-tr">
                <th class="bg-success"><input type="checkbox" id="check_all" class="checkAll"></th>
                <td class="bg-success"><strong>STT</strong></td>
                <td class="bg-success"><strong>Tên Khách</strong></td>
                <td class="bg-success"><strong>Số Điện Thoại</strong></td>
                <td class="bg-success"><strong>Email</strong></td>
                <td class="bg-success"><strong>Ngày Cập nhật</strong></td>
                <td class="bg-success"><strong>Action</strong></td>
            </tr>
            @foreach ($a_Customer as $a_val)
            <tr>
                <td><input type="checkbox" class="chk_item" value="<?php echo $a_val->id?>" name="check[]"/></td>
                <td>    {{ $a_val->stt }}</td>
                <td>    {{ $a_val->c_title }}</td>
                <td>    {{ $a_val->c_phone }}</td>
                <td>    {{ $a_val->email }}</td>
                <td>    {{ $a_val->updated_at }}</td>
                <td>                    
                    <a title="Edit" href="<?php echo Request::root().'/customer/addedit?id='.$a_val->id;?>" title="Edit" class="not-underline">
                        <i class="fa fa-edit fw"></i>
                    </a>                                      
                </td>
            </tr>
        @endforeach
            
        
        </table>
        
              
    </div>

<!--Hidden input-->
<input type="hidden" name="tbl" id="tbl" value="user_customer_mappings">
<?php echo (empty($a_search)) ? $a_Customer->render(): $a_Customer->appends($a_search)->render();?>

@endsection