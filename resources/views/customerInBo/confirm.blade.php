<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BO-DXMB</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo URL::to('/');?>/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="alert alert-success text-center">
            <strong class=""><?php echo $noti?></strong>
        </div>
    </div>
</body>

