@extends('layouts.admin.layoutAdmin')
@section('content')
    <h3 class="col-xs-12 no-padding text-uppercase">Danh sách Bill</h3>
    <div class="">
        <table id="table_id" class="table table-responsive table-hover table-striped table-bordered">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <thead>
                <tr class="header-tr">
                    <td class="bg-success"><strong>STT</strong></td>
                    <td class="bg-success"><strong>Thông tin căn hộ</strong></td>
                    <td class="bg-success"><strong>Mã Hợp đồng</strong></td>
                    <td class="bg-success"><strong>Ảnh khách hàng</strong></td>
                    <td class="bg-success"><strong>Ảnh hợp đồng</strong></td>
                    <td class="bg-success"><strong>Nhân viên</strong></td>
                    <td class="bg-success"><strong>Khách hàng</strong></td>
                    <td class="bg-success"><strong>Đợt thanh toán</strong></td>
                    <td class="bg-success"><strong>Ngày tạo</strong></td>
                    <td class="bg-success"><strong>Trạng thái</strong></td>
                    <td class="bg-success"><strong>Quy trình cũ</strong></td>
                    <td class="bg-success"><strong>Hành động</strong></td>
                </tr>
            </thead>
            <tbody>
                @foreach ($a_AllBill as $index =>  $a_val)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>
                            <p><strong>Mã SP:</strong> {{ isset($a_val->product) ? $a_val->product->code : ''   }}</p>
                            <p><strong>Tòa:</strong> {{ isset($a_val->product->category) ? $a_val->product->category->building : ''  }}</p>
                            <p><strong>Dự án:</strong>  {{ isset($a_val->product->category->sibling) ? $a_val->product->category->sibling->project : '' }}</p>
                        </td>
                        <td>{{ $a_val->bill_code }}</td>
                        <td>
                            <?php if(isset($a_val->bill_customer_info->images) && is_array($a_val->bill_customer_info->images) && count($a_val->bill_customer_info->images) > 0) { ?>
                            <img src="{{$a_val->bill_customer_info->images[0] }}" class="showImgBill" bill-id="{{ $a_val->id }}" show-type="bill_customer_info">
                            <?php } ?>
                        </td>
                        <td>
                            <?php if(is_array($a_val->media) && count($a_val->media) > 0) { ?>
                            <img src="{{$a_val->media[0] }}" class="showImgBill" bill-id="{{ $a_val->id }}" show-type="media">
                            <?php } ?>
                        </td>
                        <td>
                            <p><strong>Họ tên:</strong> {{ $a_val->staff->name }}</p>
                            <p><strong>Phòng ban:</strong> {{ $a_val->staff->group != NULL ? $a_val->staff->group->name : '' }}</p>
                        </td>
                        <td>
                            @foreach ($a_val->customers as $mapping)
                                @if ($mapping->id_user == $a_val->staff_id)
                                <p><strong>Họ tên:</strong> {{ $mapping->c_title }}</p>
                                <p><strong>Địa chỉ:</strong> {{ $mapping->permanent_address }}</p>
                                <p><strong>CMND:</strong> {{ $mapping->id_passport }}</p>
                                @endif
                            @endforeach
                        </td>
                        <td>
                            @if ($a_val->payments)
                                @foreach ($a_val->payments as $payment)
                                    <p>
                                        <a href="{{ route('show-payment', ['id' => $payment->id]) }}" class="btn btn-xs btn-{{ $payment->pb_status == 0 ? 'warning' : ($payment->pb_status == 1 ? 'primary' : 'danger') }} showDetailPayment" payment-id="{{ $payment->id  }}" target="_blank">
                                            <strong>{{ $payment->pb_code }}</strong><br><i>{{ date('d-m-Y H:i',strtotime($payment->created_at)) }}</i>
                                        </a>
                                    </p>
                                @endforeach
                            @endif
                        </td>
                        <td>{{ $a_val->bill_created_time }}</td>
                        <td>{{ \App\BOBill::STATUS_TEXTS_WEB[$a_val->status_id] }}</td>
                        <td>
                            <?php if($a_val->status_id == \App\BOBill::STATUSES['STATUS_REQUEST']) { ?>
                            <input type="checkbox" class="form-control old_procedure" id="{{ $a_val->bill_id }}">
                            <?php }else{  ?>
                                {{ $a_val->bill_note }}
                             <?php } ?>
                        </td>
                        <td>
                            <?php if(count($a_val->button)) { ?>
                                @foreach ($a_val->button as $btn)
                                    <p><a href="/bill/changeStatus?bill_id={{ $a_val->bill_id }}&status_id={{ $btn['VALUE'] }}" class="changeStatusBill btn btn-sm {{ $btn['SUCCESS'] ? 'btn-primary' : 'btn-danger' }}" bill-id="{{ $a_val->bill_id  }}">{{ $btn['TEXT']}}</a></p>
                                @endforeach
                            <?php } ?>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!--Hidden input-->
    <input type="hidden" name="tbl" id="tbl" value="b_o_bills">

    <div class="modal fade popupBillImg hide" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img class="img-responsive" src="">
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="detailPaymentPopup" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="list-group">
                        <li class="list-group-item active">CHI TIẾT THANH TOÁN NGÀY <strong id="paymentDate"></strong></li>
                        <li class="list-group-item">Mã hợp đồng: </li>
                        <li class="list-group-item">Số tiền yêu cầu: </li>
                        <li class="list-group-item">Số tiền thực nhận: </li>
                        <li class="list-group-item">Phương thức thanh toán: </li>
                        <li class="list-group-item">Thông tin ngân hàng: </li>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection



