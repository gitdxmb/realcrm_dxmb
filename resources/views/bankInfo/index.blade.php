@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase">Danh sách Ngân Hàng</h3>
<form method="get" action="" id="frmFilter" name="frmFilter"  class="form-inline">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
</form>
    <div class="">
        <table id="table_id" class="table table-responsive table-hover table-striped table-bordered">
            <thead>
                <tr class="header-tr">
                    <td class="bg-success"><strong>STT</strong></td>
                    <td class="bg-success"><strong>Tên </strong></td>
                    <td class="bg-success"><strong>Mã</strong></td>
                    <td class="bg-success"><strong>Chủ tài khoản</strong></td>
                    <td class="bg-success"><strong>Số tài khoản</strong></td>
                    <td class="bg-success"><strong>Action</strong></td>
                </tr>
            </thead>
            <tbody>
            @foreach ($a_BankInfo as $a_val)
                <tr>

                    <td>    {{ $a_val->stt }}</td>
                    <td>    {{ $a_val->bank_title }}</td>
                    <td>    {{ $a_val->bank_code }}</td>
                    <td>    {{ $a_val->bank_holder }}</td>
                    <td>    {{ $a_val->bank_number }}</td>
                    <td>
                        <?php
                        if($a_val->bank_status == 1){
                        ?>
                        <a title="Edit" href="<?php echo Request::root().'/bank/addedit?id='.$a_val->id;?>" title="Sửa" class="not-underline">
                            <i class="fa fa-edit fw"></i>
                        </a>
                        <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},1,'bank_status')" title="Cho vào thùng rác" class="not-underline">
                            <i class="fa fa-trash fa-fw text-danger"></i>
                        </a>
                        <?php }else if($a_val->bank_status == 0 || $a_val->bank_status == -1){ ?>
                        <a href="javascript:GLOBAL_JS.v_fRecoverRow({{ $a_val->id }},'bank_status')"  title="Khôi phục lại" class="not-underline">
                            <i class="fa fa-upload fw"></i>
                        </a>
                        <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},0,'bank_status')" title="Xóa vĩnh viễn" class="not-underline">
                            <i class="fa fa-trash-o fa-fw text-danger"></i>
                        </a>
                        <?php }?>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

<!--Hidden input-->
<input type="hidden" name="tbl" id="tbl" value="bank_infos">


@endsection