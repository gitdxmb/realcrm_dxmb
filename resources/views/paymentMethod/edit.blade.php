@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase"><?php echo $i_id == '' ? 'Thêm Phương Thức TT' : 'Sửa Phương Thức TT' ?></h3>
<div class="alert alert-danger hide backend"></div>
<form id="fileupload" class="form-horizontal" method="post" action="">

    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id" value="<?php echo $i_id ?>">
    <input type="hidden" id="tbl" value="payment_methods">
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="pm_title" class="col-xs-12 col-sm-3 control-label text-left">Tên PTTT</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="pm_title" name="pm_title" field-name="Tên" type="text" value="<?php echo isset($PayMentMTData->pm_title) ? $PayMentMTData->pm_title : "" ?>" class="form-control check-duplicate" placeholder="Tên Phương Thức" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="pm_code" class="col-xs-12 col-sm-3 control-label text-left">Code</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="pm_code" name="pm_code" field-name="Số điện thoại" type="text" value="<?php echo isset($PayMentMTData->pm_code) ? $PayMentMTData->pm_code : "" ?>" class="form-control check-duplicate" placeholder="Code" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="pm_note" class="col-xs-12 col-sm-3 control-label text-left">Mô Tả</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <textarea class="form-control pm_note" name="pm_note" rows="5" id="pm_note" placeholder="Mô Tả..."><?php echo isset($PayMentMTData->pm_note) ? $PayMentMTData->pm_note : "" ?></textarea>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-3 no-padding">
            <label for="pm_status" class="col-xs-6 control-label text-left">Trạng thái</label>
            <div class="col-xs-6 no-left-padding">
                <input id="pm_status" name="pm_status" type="checkbox" class="form-control" <?php if (isset($PayMentMTData->pm_status) && $PayMentMTData->pm_status): ?>checked<?php endif ?>>
            </div>
        </div>
    </div>
    

    <div class="form-group">
        <div class="col-xs-6 col-sm-3 no-padding">
            <button type="reset" class="btn btn-default">Nhập lại</button>
            <input type="button" name="submit" VALUE="Cập nhật" class="btn btn-primary btn-sm " onclick="GLOBAL_JS.v_fSubmitProjectValidate()"/>
            <input type="submit" name="submit" class="btn btn-primary btn-sm hide submit">
        </div>
    </div>
</form>


@endsection
@section('jsfile')
<script src="<?php echo URL::to('/'); ?>/ckeditor/ckeditor.js"></script>
<script src="<?php echo URL::to('/'); ?>/ckeditor/config.js"></script>
<script src="<?php echo URL::to('/'); ?>/ckeditor/styles.js"></script>.
<script src="<?php echo URL::to('/'); ?>/ckeditor/build-config.js"></script>
<script> CKEDITOR.replace('pm_note');</script>
@endsection
