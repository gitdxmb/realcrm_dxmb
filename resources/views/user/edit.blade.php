@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase"><?php echo $i_id == '' ? 'Thêm Nhân Viên' : 'Sửa Thông Tin Nhân Viên' ?></h3>
<div class="alert alert-danger hide backend"></div>
<form id="fileupload" class="form-horizontal" method="post" action="">

    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id" value="<?php echo $i_id ?>">
    <input type="hidden" id="tbl" value="b_o_users">
    
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="ub_title" class="col-xs-12 col-sm-3 control-label text-left">Họ Tên</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="ub_title" name="ub_title" field-name="Tên" type="text" value="<?php echo isset($userBOData->ub_title) ? $userBOData->ub_title : "" ?>" class="form-control check-duplicate" placeholder="Tên Nhân viên" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="group_ids" class="col-xs-12 col-sm-3 control-label text-left">Chọn Sàn/Chi Nhánh</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="group_ids" name="group_ids"  class="js-select2">
                    <option value="">Chọn Sàn/Chi Nhánh</option>
                    @foreach ($a_Branch as $val)
                    <option value="{{$val->gb_id}}" <?php if(isset($userBOData->group_ids) && $userBOData->group_ids == $val->gb_id) echo 'selected'; ?>>{{$val->gb_title}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="ub_email" class="col-xs-12 col-sm-3 control-label text-left">Email</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="ub_title" name="ub_email" field-name="Email" type="email" value="<?php echo isset($userBOData->ub_email) ? $userBOData->ub_email : "" ?>" class="form-control check-duplicate" placeholder="email" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-12 no-padding">
            <label for="parent_id" class="col-xs-12 col-sm-1 control-label text-left">Chọn ảnh</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                <div class="row fileupload-buttonbar">
                    <div class="col-lg-7">
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <span class="btn btn-success fileinput-button">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>Add files...</span>
                            <input type="file" name="files[]" multiple>
                        </span>
                        <button type="submit" class="btn btn-primary start">
                            <i class="glyphicon glyphicon-upload"></i>
                            <span>Start upload</span>
                        </button>
                        <button type="reset" class="btn btn-warning cancel">
                            <i class="glyphicon glyphicon-ban-circle"></i>
                            <span>Cancel upload</span>
                        </button>
                        <button type="button" class="btn btn-danger delete">
                            <i class="glyphicon glyphicon-trash"></i>
                            <span>Delete</span>
                        </button>
                        <input type="checkbox" class="toggle">
                        <!-- The global file processing state -->
                        <span class="fileupload-process"></span>
                    </div>
                    <!-- The global progress state -->
                    <div class="col-lg-5 fileupload-progress fade">
                        <!-- The global progress bar -->
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                        </div>
                        <!-- The extended global progress state -->
                        <div class="progress-extended">&nbsp;</div>
                    </div>
                </div>
<!--                <strong style="color: red">(***) Lưu ý chỉ chọn 1 ảnh làm ảnh đại diện</strong>-->
                <!-- The table listing the files available for upload/download -->
                <table role="presentation" class="table table-striped"><tbody class="files">
                        <?php
                       
                        if (isset($userBOData->ub_info_images) && $userBOData->ub_info_images != '') {
                            foreach($userBOData->ub_info_images as $val){
                            ?>
                        <tr class="template-download fade in">
                                <td>
                                    <span class="preview">
                                        <a href="{{$val->img}}" title="{{$val->filename}}" download="{{$val->filename}}.jpg" data-gallery=""><img src="/server/php/files/thumbnail/{{$val->filename}}"></a>
                                    </span>
                                </td>
                                <td>
                                    <p class="name">
                                        <a href="{{$val->img}}" title="{{$val->filename}}" download="{{$val->filename}}" data-gallery="">{{$val->filename}}</a>
                                    </p>
                                </td>
                                <td>
                                    <span class="size">636.15 KB</span>
                                </td>
                                <td>
                                    <button class="btn btn-danger delete" data-type="DELETE" data-url="{{$val->deleteUrl}}">
                                        <i class="glyphicon glyphicon-trash"></i>
                                        <span>Delete</span>
                                    </button>
                                    <input type="checkbox" name="delete" value="1" class="toggle">
                                </td>
                            <input type="hidden" name="ub_info_images[]" value="{{$val->img}}">
                            <input type="hidden"  name="filename[]" name="filename[]" value="{{$val->filename}}">
                            <input type="hidden"  name="deleteUrl[]" name="deleteUrl[]" value="{{$val->deleteUrl}}">
                        </tr>

                        <?php } } ?>
                    </tbody></table>
                <!-- The blueimp Gallery widget -->
                <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                    <div class="slides"></div>
                    <h3 class="title"></h3>
                    <a class="prev">‹</a>
                    <a class="next">›</a>
                    <a class="close">×</a>
                    <a class="play-pause"></a>
                    <ol class="indicator"></ol>
                </div>
                <!-- The template to display files available for upload -->
                <script id="template-upload" type="text/x-tmpl">
                    {% for (var i=0, file; file=o.files[i]; i++) { %}
                    <tr class="template-upload fade">
                    <td>
                    <span class="preview"></span>
                    </td>
                    <td>
                    <p class="name">{%=file.name%}</p>
                    <strong class="error text-danger"></strong>
                    </td>
                    <td>
                    <p class="size">Processing...</p>
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                    </td>
                    <td>
                    {% if (!i && !o.options.autoUpload) { %}
                    <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                    </button>
                    {% } %}
                    {% if (!i) { %}
                    <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                    </button>
                    {% } %}
                    </td>
                    </tr>
                    {% } %}
                </script>
                <!-- The template to display files available for download -->
                <script id="template-download" type="text/x-tmpl">
                    {% for (var i=0, file; file=o.files[i]; i++) { %}
                    <tr class="template-download fade">
                    <td>
                    <span class="preview">
                    {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                    {% } %}
                    </span>
                    </td>
                    <td>
                    <p class="name">
                    {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                    {% } else { %}
                    <span>{%=file.name%}</span>
                    {% } %}
                    </p>
                    {% if (file.error) { %}
                    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                    {% } %}
                    </td>
                    <td>
                    <span class="size">{%=o.formatFileSize(file.size)%}</span>
                    </td>
                    <td>
                    {% if (file.deleteUrl) { %}
                    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                    </button>
                    <input type="checkbox" name="delete" value="1" class="toggle">
                    {% } else { %}
                    <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                    </button>
                    {% } %}
                    </td>
                    <input type="hidden"  name="ub_info_images[]" value="{%=file.url%}">
                    <input type="hidden"  name="filename[]" value="{%=file.name%}">
                    <input type="hidden"  name="deleteUrl[]" value="{%=file.deleteUrl%}">
                    </tr>
                    {% } %}
                </script>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="password" class="col-xs-12 col-sm-3 control-label text-left">Password</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="password" name="password" field-name="password" type="password" value="" class="form-control check-duplicate" placeholder="password" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="re-password" class="col-xs-12 col-sm-3 control-label text-left">Re-Password</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="re-password" name="re-password" field-name="re-password" value="" type="password" class="form-control" placeholder="re-password" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="ub_staff_code" class="col-xs-12 col-sm-3 control-label text-left">Mã Nhân Viên</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="ub_staff_code" name="ub_staff_code" field-name="ub_staff_code" type="text" value="<?php echo isset($userBOData->ub_staff_code) ? $userBOData->ub_staff_code : "" ?>" class="form-control" placeholder="Mã Nhân Viên" required />
            </div>
        </div>
    </div>    
    
    <div class="form-group">
        <div class="col-xs-12 col-sm-3 no-padding">
            <label for="ub_status" class="col-xs-6 control-label text-left">Trạng thái</label>
            <div class="col-xs-6 no-left-padding">
                <input id="ub_status" name="ub_status" type="checkbox" class="form-control" <?php if (isset($userBOData->ub_status) && $userBOData->ub_status): ?>checked<?php endif ?>>
            </div>
        </div>
    </div>
    
    
    <div class="form-group">
        <div class="col-xs-6 col-sm-3 no-padding">
            <button type="reset" class="btn btn-default">Nhập lại</button>
            <input type="button" name="submit" VALUE="Cập nhật" class="btn btn-primary btn-sm " onclick="GLOBAL_JS.v_fSubmitUserInBoValidate()"/>
            <input type="submit" name="submit" class="btn btn-primary btn-sm hide submit">
        </div>
    </div>
</form>
@section('cssfile')
<link rel="stylesheet" href="<?php echo URL::to('/'); ?>/upload_resource/css/style.css">
<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?php echo URL::to('/'); ?>/upload_resource/css/jquery.fileupload.css">
<link rel="stylesheet" href="<?php echo URL::to('/'); ?>/upload_resource/css/jquery.fileupload-ui.css">
<noscript><link rel="stylesheet" href="<?php echo URL::to('/'); ?>/upload_resource/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?php echo URL::to('/'); ?>/upload_resource/css/jquery.fileupload-ui-noscript.css"></noscript>
<!--<link href="<?php echo URL::to('/'); ?>/ckeditor/contents.css" rel="stylesheet">-->
@endsection



<!-- The file upload form used as target for the file upload widget -->
<!-- Redirect browsers with JavaScript disabled to the origin page -->


<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->

@section('jsfile')
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<!-- blueimp Gallery script -->
<script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script src="<?php echo URL::to('/'); ?>/upload_resource/js/main.js"></script>

@endsection

@endsection
