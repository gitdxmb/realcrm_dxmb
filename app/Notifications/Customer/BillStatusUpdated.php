<?php

namespace App\Notifications\Customer;

use App\BOBill;
use App\Http\Controllers\API\Customer\AuthController;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class BillStatusUpdated extends Notification implements ShouldQueue
{
    use Queueable;
    protected $bill;

    /**
     * BillStatusUpdated constructor.
     * @param BOBill $bill
     */
    public function __construct(BOBill $bill)
    {
        $this->bill = $bill;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'bill' => $this->bill->{BOBill::ID_KEY},
            'by'   => AuthController::getCurrentUID(),
            'status' => $this->bill->status_id,
        ];
    }
}
