<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCheckin extends Model
{
    protected $fillable = [
        'event_id', 'user_id', 'check_in', 'check_out', 'created_at', 'updated_at'
    ];
}
