<?php

namespace App\Http\Controllers;
use App\BOBill;
use App\BOCustomer;
use App\BOProduct;
use App\BOTransaction;
use App\BOUser;
use App\Http\Controllers\API\LogController;
use App\TemplateEmail;
use App\BOCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
class TemplateEmailController extends Controller
{
    public function __construct() {

    }
    public function index(){
        $a_Data = TemplateEmail::getAll();
        $a_DataView['a_Tmp'] = $a_Data;
        $a_Project = BOCategory::getAllProject();
        $arrProject = array();
        foreach ($a_Project as $o_project){
            $arrProject[$o_project->cb_id] = $o_project->cb_title;
        }
        $a_DataView['a_Project'] = $arrProject;
        return view('templateEmail.index',$a_DataView);
    }

    public function addedit(){
        $a_DataView = array();
        $tempId = (int) Input::get('id', 0);
        $isDefault = Input::get('is_default', null);
        if($isDefault == null){
            return redirect('template_email/addedit?is_default=0')->with('status', 'Cập nhật thành công!');
        }

        $a_DataView['isDefault'] = $isDefault;
        if($tempId!= 0){
            $temp = TemplateEmail::find($tempId);
            if(!$temp) return redirect('template_email')->with('status', 'Không có template này!');
            $a_DataView['i_id'] = $tempId;
        }else{
            $temp = new TemplateEmail();
        }
        $data = Input::get();
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            $temp->code = $data['code'];
            $temp->title = $data['title'];
            $temp->project = isset($data['project']) ? $data['project'] : $temp->project;
            $temp->is_apartment = isset($data['is_apartment']) ? ($data['is_apartment'] == 1 ? true : false) : Null;
            $temp->type = $data['type'];
            $temp->is_default = $isDefault;
            $temp->html = strip_tags($data['html'], '<br><p><strong><table><tr><td><th><tbody><b><em><u><img><div>');
            $temp->html_pdf = strip_tags($data['html_pdf'], '<br><p><strong><table><tr><td><th><tbody><b><em><u><img><div>');
            if($temp->save()){
                return redirect('template_email')->with('status', 'Cập nhật thành công!');
            }
        }
        $a_DataView['temp'] = $temp;
        $a_Project = BOCategory::getAllProject();
        $a_DataView['a_Project'] = $a_Project;
        return view('templateEmail.addedit', $a_DataView );
    }

    /**
     * @param BOBill $bill
     * @param $status
     * @param bool $customer_update
     * @param bool $customer_hide
     * @param bool $api_gate
     * @return array
     */
    public static function generateBillInvoice(BOBill $bill, $status, $customer_update = false, $customer_hide=false, $api_gate=true) {
        if (!$bill || !$bill->product_id || !$bill->bill_customer_info) return self::jsonError('Hợp đồng không hợp lệ!');
        /** @var $product */
        $product = BOProduct::select([
            "pb_id",
            "pb_title AS apartment", "pb_code AS code",
            "category_id",
            "pb_required_money AS required_money",
            "pb_used_s", "pb_built_up_s", "pb_code_real", "pb_price_total", "book_type",
            "pb_door_direction", "note"
        ])
            ->where(BOProduct::ID_KEY, $bill->product_id)
            ->with([ "category" => function($building) {
                return $building->select([
                    "id", "cb_id", "parent_id", "cb_title AS building", "send_mail"
                ])
                    ->where("cb_level", 2)
                    ->with(["sibling" => function($project) {
                        return $project->select(["id", "cb_id", "parent_id", "investor_id", "cb_title AS project", "apartment_grid"])
                            ->where("cb_level", 1)
                            ->with(["investor" => function($investor) {
                                return $investor->select(["id", "code", "name"]);
                            }]);
                    }]);
            }])->first();
        if (!$product) {
            LogController::logBill('[Mail] Không thể gửi mail HĐ cho khách hàng do không tìm thấy sản phẩm', 'warning', $bill->{BOBill::ID_KEY}, true, $api_gate);
            return false;
        }
        /** @var $building */
        $building = $product->category;
        if ($building->send_mail == 0) {
            LogController::logBill('[Mail] Tòa này chưa cho gửi Email', 'warning', $bill->{BOBill::ID_KEY}, true, $api_gate);
            return self::jsonError('Không cho gửi Email');
        }
        /** @var $project */
        $project = $building&&$building->sibling? $building->sibling : null;
        if (!$project) {
            LogController::logBill('[Mail] Không thể gửi mail HĐ cho khách hàng do không tìm thấy dự án sản phẩm', 'warning', $bill->{BOBill::ID_KEY}, true, $api_gate);
            return false;
        }
        /** @var $staff */
        $staff = $bill->staff;
        if (!$staff) {
            LogController::logBill('[Mail] Không thể gửi mail HĐ cho khách hàng do không tìm thấy thông tin nhân viên', 'warning', $bill->{BOBill::ID_KEY}, true, $api_gate);
            return false;
        }

        /** @var $investor */
        $investor = $project&&$project->investor? $project->investor->name : null;
        /** @var $apartment_code */
        $apartment_code = $product->code;
        $floor = (string)substr($apartment_code, -6, 3);
        $label = (string)substr($apartment_code, -3);
        $building_code = substr($apartment_code, 0, 6);
        /** @var $email_template */
        $email_template = TemplateEmail::where([
            'project' => $project->{BOCategory::ID_KEY},
            'type'    => $bill->type == BOBill::TYPE_DEPOSIT ? 1 : ($product->book_type == 1 ? 2 : 3)
        ])->first();
        if (!$email_template) {
            $email_template = TemplateEmail::where([
                'is_default' => 1,
                'is_apartment' => $project->apartment_grid,
                'type' => $bill->type == BOBill::TYPE_DEPOSIT ? 1 : ($product->book_type == 1 ? 2 : 3),
            ])->first();
        }
        if (!$email_template) {
            LogController::logBill('[Mail] Không thể gửi mail HĐ cho khách hàng do không tìm thấy Email Template', 'error', $bill->{BOBill::ID_KEY}, true, $api_gate);
            return false;
        }

        /** @var $customer_info */
        $customer_info = $bill->bill_customer_info;
        /** @var $random_password */
        $random_password = self::randString();
        /** @var $customer_update */
        $customer_update = $customer_update && BOCustomer::where([BOCustomer::ID_KEY => $bill->customer_id])
                ->where('cb_status', '!=', env('STATUS_ACTIVE', 1))
                ->update(['cb_password' => bcrypt($random_password), 'cb_status' => 1]);
        /** @var $amount_to_text */
        $amount_to_text = ucfirst(self::convert_number_to_words($bill->bill_total_money));
        /** @var $deposit */
        $transaction = $bill->transaction;
        $deposit = $transaction? (int) str_replace(',','', $transaction->deposit) : 0;
        /** @var $deposit_to_text */
        $deposit_to_text = ucfirst(self::convert_number_to_words($deposit));
        /** @var $remain_amount */
        $remain_amount = (int)$bill->bill_required_money - (int)$deposit;
        $remain_amount = $remain_amount <= 0 ? 0 : $remain_amount;

        $empty_image = '<img style="width: 100px; height: 100px" src="">';

        /** @var $signature_staff */
        $signature_staff = $staff->signature? '<img style="width: 100px; height: 100px" src="'.asset($staff->signature).'">' :$empty_image;

        /** @var $logs */
        $logs = $bill->bill_approval_log;
        /** Get Signatures */
        $signature_secretary = ''; ///Chu ky TKKD
        $secretary = ''; //Ten TKKD
        $signature_director = ''; // Chu ky QLS
        $director_name = ''; // Ten QLS
        if($status == BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']){
            if(count($logs)){
                foreach ($logs as $log){
                    if($log['VALUE'] == BOBill::STATUSES['STATUS_APPROVED']){
                        $createdUser = $log['CREATED_BY'];
                        $user = BOUser::where([BOUser::ID_KEY => $createdUser])->first();
                        if($user){
                            $signature_secretary = $user->signature ? '<img style="width: 100px; height: 100px" src="'.asset($user->signature).'">' : $empty_image;
                            $secretary = $user->ub_title;
                        }
                    }
                }
            }
            $signature_director = BOUser::getCurrent('signature', $api_gate);
            $signature_director = $signature_director ? '<img style="width: 100px; height: 100px" src="'.asset($signature_director).'">' : $empty_image;
            $director_name = BOUser::getCurrent('ub_title', $api_gate);
            if($bill->type != BOBill::TYPE_DEPOSIT){
                $ngaydatcho = date('d-m-Y',strtotime($bill->bill_created_time));
            }
        }elseif($status!=BOBill::STATUSES['STATUS_REQUEST']){
            if(count($logs)){
                foreach ($logs as $log){
                    if($log['VALUE'] == BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']){
                        $createdUser = $log['CREATED_BY'];
                        $user = BOUser::where([BOUser::ID_KEY => $createdUser])->first();
                        if($user){
                            $signature_director = $user->signature != '' ? '<img style="width: 100px; height: 100px" src="http://bo.dxmb.vn/'.$user->signature.'">' : $empty_image;
                            $director_name = $user->ub_title;
                        }
                    }
                }
            }
            $signature_secretary = BOUser::getCurrent('signature', $api_gate);
            $signature_secretary = $signature_secretary != '' ? '<img style="width: 100px; height: 100px" src="'.asset($signature_secretary).'">' : $empty_image;
            $secretary = BOUser::getCurrent('ub_title', $api_gate);
        }

        /** @var $content */
        $arrReplace = array(
            '@sohopdong' => $bill->bill_code,
            '@tenkhachhang' => $customer_info->name,
            '@cmnd' => $customer_hide? '' : $customer_info->id_passport,
            '@ngaycap' => $customer_hide?'' : $customer_info->issue_date,
            '@noicap' => $customer_hide? '' : $customer_info->issue_place,
            '@diachi' => $customer_hide? '' : $customer_info->address,
            '@thuongtru' => $customer_hide? '' : $customer_info->permanent_address,
            '@sodienthoai' => $customer_hide? substr($customer_info->phone, 0, 3) . '****' : $customer_info->phone,
            '@email' => $customer_hide? '****@' . str_after($customer_info->email, '@') : $customer_info->email,
            '@duan' => $project? $project->project : '',
            '@chudautu' => $investor,
            '@macanho' => $product->code,
            '@malo' => $product->code,
            '@tang' => $floor,
            '@toa' => $building_code,
            '@canso' => $label,
            '@huong' => $product->pb_door_direction,
            '@tonggia' => number_format($bill->bill_total_money),
            '@dttimtuong' => $product->pb_built_up_s == Null || $product->pb_built_up_s == 0 ? '-' : $product->pb_built_up_s,
            '@dtdat' => $product->pb_built_up_s == Null || $product->pb_built_up_s == 0 ? '-' : $product->pb_built_up_s,
            '@dtthongthuy' => $product->pb_used_s == Null || $product->pb_used_s == 0 ? '-' : $product->pb_used_s,
            '@giatimtuong' => $product->pb_price_per_s == Null || $product->pb_price_per_s == 0 ? '-' : number_format($product->pb_price_per_s),
            '@giathongthuy' =>  $product->price_used_s == Null || $product->price_used_s == 0 ? '-' : number_format($product->price_used_s),
            '@tennhanvien' => $staff->ub_title,
            '@manhanvien' => $staff ? $staff->ub_tvc_code : '',
            '@thuocdonvi' => $staff->group ? $staff->group->gb_title : '',
            '@tiendattruoc' => number_format($deposit),
            '@dattruocbangchu' => $deposit_to_text,
            '@tongtienbangchu' => $amount_to_text,
            '@matkhau' => $customer_update? $random_password : '(Mật khẩu hiện tại)',
            '@chukynhanvien' => $signature_staff,
            '@tienconlai' => $remain_amount == 0 ? '-' : number_format($remain_amount),
            '@conlaibangchu' => $remain_amount == 0 ? '-' : ucfirst(self::convert_number_to_words($remain_amount)),
            '@sotienquydinh' => number_format($bill->bill_required_money),
            '@tienquydinhbangchu' => ucfirst(self::convert_number_to_words($bill->bill_required_money)),
            '@chukytkkd' => $signature_secretary,
            '@tenthuky' => $secretary,
            '@chukygiamdocsan' => $signature_director,
            '@giamdocsan' => $director_name,
            '@ngaydatcho' => isset($ngaydatcho) ? $ngaydatcho : '',
            '@ngayguimail' => date("d"),
            '@thangguimail' => date("m"),
            '@namguimail' => date("Y"),
            '@loaimatduong' => $product->note
        );
        $content = $email_template->html;
        $contentPdf = $email_template->html_pdf;
        foreach ($arrReplace as $strFrom => $strTo){
            $content = str_replace($strFrom, $strTo, $content);
            $contentPdf = str_replace($strFrom, $strTo, $contentPdf);
        }
        return ['mail' => $content, 'pdf' => $contentPdf];
    }
}
