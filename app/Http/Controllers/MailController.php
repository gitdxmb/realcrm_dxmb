<?php

namespace App\Http\Controllers;

use App\BOBill;
use App\BOCategory;
use App\BOCustomer;
use App\BOPayment;
use App\BOProduct;
use App\BOTransaction;
use App\BOUser;
use App\Http\Controllers\API\LogController;
use App\Jobs\SendEmailJob;
use App\Mail\BillCreated;
use App\Mail\PaymentCustomer;
use App\Mail\StatusBillCustomer;
use App\TemplateEmail;
use PDF;
class MailController extends Controller
{
    /**
     * @param string $mail_to
     * @return \Illuminate\Http\JsonResponse
     */
    public static function test($mail_to = 'lienhe@dxmb.vn') {
        $pdf = public_path('pdf/hop-dong-1541733483.pdf');
        $mail = SendEmailJob::dispatch($mail_to, new BillCreated('Test', 0, 3, 0, $pdf))
            ->delay(now()->addSeconds(5));
        LogController::logBill('[Mail] Đã gửi mail HĐ test cho khách hàng: '.$mail_to, 'info');
        return self::jsonSuccess($mail, 'Gửi mail hợp đồng thành công! '.$mail_to);
    }

    /**
     * @param BOBill $bill
     * @param $status
     * @param bool $api_gate
     * @return \Illuminate\Http\JsonResponse
     */
    public static function customerContract(BOBill $bill, $status, $api_gate = true) {
        if (!$bill || !$bill->product_id || !$bill->bill_customer_info) return self::jsonError('Hợp đồng không hợp lệ!');
        /** @var $product */
        $product = BOProduct::select([
            "pb_id",
            "pb_title AS apartment", "pb_code AS code",
            "category_id",
            "pb_required_money AS required_money",
            "pb_used_s", "pb_built_up_s", "pb_code_real", "pb_price_total", "book_type",
            "pb_door_direction", "note"
        ])
            ->where(BOProduct::ID_KEY, $bill->product_id)
            ->with([ "category" => function($building) {
                return $building->select([
                    "id", "cb_id", "parent_id", "cb_title AS building", "send_mail"
                ])
                ->where("cb_level", 2)
                ->with(["sibling" => function($project) {
                    return $project->select(["id", "cb_id", "parent_id", "investor_id", "cb_title AS project", "apartment_grid"])
                    ->where("cb_level", 1)
                    ->with(["investor" => function($investor) {
                        return $investor->select(["id", "code", "name"]);
                    }]);
                }]);
            }])->first();
        if (!$product) {
            LogController::logBill('[Mail] Không thể gửi mail HĐ cho khách hàng do không tìm thấy sản phẩm', 'warning', $bill->{BOBill::ID_KEY}, true, $api_gate);
            return self::jsonError('Không tìm thấy sản phẩm');
        }
        /** @var $building */
        $building = $product->category;
        if ($building->send_mail == 0) {
            LogController::logBill('[Mail] Tòa này chưa cho gửi Email', 'warning', $bill->{BOBill::ID_KEY}, true, $api_gate);
            return self::jsonError('Không cho gửi Email');
        }
        /** @var $project */
        $project = $building&&$building->sibling? $building->sibling : null;
        if (!$project) {
            LogController::logBill('[Mail] Không thể gửi mail HĐ cho khách hàng do không tìm thấy dự án sản phẩm', 'warning', $bill->{BOBill::ID_KEY}, true, $api_gate);
            return self::jsonError('Không có thông tin dự án');
        }

        $staff = BOUser::select([
            'ub_id',
            'ub_tvc_code',
            "group_ids"
        ])
        ->where("ub_id", $bill->staff_id)
        ->with(["group" => function($group) {
            return $group->select(["gb_title","gb_id"]);
        }])->first();
        if (!$staff) {
            LogController::logBill('[Mail] Không thể gửi mail HĐ cho khách hàng do không tìm thấy thông tin nhân viên', 'warning', $bill->{BOBill::ID_KEY}, true, $api_gate);
            return self::jsonError('Không tìm thấy nhân viên');
        }

        /** @var $investor */
        $investor = $project&&$project->investor? $project->investor->name : null;
        /** @var $apartment_code */
        $apartment_code = $product->code;
        $floor = (string)substr($apartment_code, -6, 3);
        $label = (string)substr($apartment_code, -3);
        $building_code = substr($apartment_code, 0, 6);
        /** @var $email_template */
        $email_template = TemplateEmail::where([
            'project' => $project->{BOCategory::ID_KEY},
            'type'    => $bill->type == BOBill::TYPE_DEPOSIT ? 1 : ($product->book_type == 1 ? 2 : 3)
        ])->first();
        if (!$email_template) {
            $email_template = TemplateEmail::where([
                'is_default' => 1,
                'is_apartment' => $project->apartment_grid,
                'type' => $bill->type == BOBill::TYPE_DEPOSIT ? 1 : ($product->book_type == 1 ? 2 : 3),
            ])->first();
        }
        if (!$email_template) {
            LogController::logBill('[Mail] Không thể gửi mail HĐ cho khách hàng do không tìm thấy Email Template', 'error', $bill->{BOBill::ID_KEY}, true, $api_gate);
            return self::jsonError('Không thể gửi mail do chưa có template!');
        }

        /** @var $customer_info */
        $customer_info = $bill->bill_customer_info;
        /** @var $random_password */
        $random_password = self::randString();
        /** @var $customer_update */
        $customer_update = BOCustomer::where([BOCustomer::ID_KEY => $bill->customer_id])->where('cb_status', '!=', env('STATUS_ACTIVE', 1))
                    ->update(['cb_password' => bcrypt($random_password), 'cb_status' => 1]);
        /** @var $amount_to_text */
        $amount_to_text = ucfirst(self::convert_number_to_words($bill->bill_total_money));
        /** @var $deposit */
        $transaction = BOTransaction::select('trans_deposit AS deposit')->where(BOTransaction::ID_KEY, $bill->transaction_id)->first();
        $deposit = $transaction? (int) str_replace(',','', $transaction->deposit) : 0;
        /** @var $deposit_to_text */
        $deposit_to_text = ucfirst(self::convert_number_to_words($deposit));
        /** @var $remain_amount */
        $remain_amount = (int)$bill->bill_required_money - (int)$deposit;
        $remain_amount = $remain_amount <= 0 ? 0 : $remain_amount;
        /** @var $signature_staff */
        $signature_staff = $bill->staff->signature != '' ? '<img style="width: 100px; height: 100px" src="http://bo.dxmb.vn/'.$bill->staff->signature.'">' :'<img style="width: 100px; height: 100px" src="">';

        /** @var $logs */
        $logs = $bill->bill_approval_log;
        /** Get Signatures */
        $signature_secretary = ''; ///Chu ky TKKD
        $secretary = ''; //Ten TKKD
        $signature_director = ''; // Chu ky QLS
        $director_name = ''; // Ten QLS
        if($status == BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']){
            if(count($logs)){
                foreach ($logs as $log){
                    if($log['VALUE'] == BOBill::STATUSES['STATUS_APPROVED']){
                        $createdUser = $log['CREATED_BY'];
                        $user = BOUser::where([BOUser::ID_KEY => $createdUser])->first();
                        if($user){
                            $signature_secretary = $user->signature != '' ? '<img style="width: 100px; height: 100px" src="http://bo.dxmb.vn/'.$user->signature.'">' : '<img style="width: 100px; height: 100px" src="">';
                            $secretary = $user->ub_title;
                        }
                    }
                }
            }
            $signature_director = BOUser::getCurrent('signature', $api_gate);
            $signature_director = $signature_director != '' ? '<img style="width: 100px; height: 100px" src="http://bo.dxmb.vn/'.$signature_director.'">' : '<img style="width: 100px; height: 100px" src="">';
            $director_name = BOUser::getCurrent('ub_title', $api_gate);
            if($bill->type != BOBill::TYPE_DEPOSIT){
                $ngaydatcho = date('d-m-Y',strtotime($bill->bill_created_time));
            }
        }else{
            if(count($logs)){
                foreach ($logs as $log){
                    if($log['VALUE'] == BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']){
                        $createdUser = $log['CREATED_BY'];
                        $user = BOUser::where([BOUser::ID_KEY => $createdUser])->first();
                        if($user){
                            $signature_director = $user->signature != '' ? '<img style="width: 100px; height: 100px" src="http://bo.dxmb.vn/'.$user->signature.'">' : '<img style="width: 100px; height: 100px" src="">';
                            $director_name = $user->ub_title;
                        }
                    }
                }
            }
            $signature_secretary = BOUser::getCurrent('signature', $api_gate);
            $signature_secretary = $signature_secretary != '' ? '<img style="width: 100px; height: 100px" src="http://bo.dxmb.vn/'.$signature_secretary.'">' : '<img style="width: 100px; height: 100px" src="">';
            $secretary = BOUser::getCurrent('ub_title', $api_gate);
        }

        /** @var $content */
        $arrReplace = array(
            '@sohopdong' => $bill->bill_code,
            '@tenkhachhang' => $customer_info->name,
            '@cmnd' => $customer_info->id_passport,
            '@ngaycap' => $customer_info->issue_date,
            '@noicap' => $customer_info->issue_place,
            '@diachi' => $customer_info->address,
            '@thuongtru' => $customer_info->permanent_address,
            '@sodienthoai' => $customer_info->phone,
            '@email' => $customer_info->email,
            '@duan' => $project? $project->project : '',
            '@chudautu' => $investor,
            '@macanho' => $product->code,
            '@malo' => $product->code,
            '@tang' => $floor,
            '@toa' => $building_code,
            '@canso' => $label,
            '@huong' => $product->pb_door_direction,
            '@tonggia' => number_format($bill->bill_total_money),
            '@dttimtuong' => $product->pb_built_up_s == Null || $product->pb_built_up_s == 0 ? '-' : $product->pb_built_up_s,
            '@dtdat' => $product->pb_built_up_s == Null || $product->pb_built_up_s == 0 ? '-' : $product->pb_built_up_s,
            '@dtthongthuy' => $product->pb_used_s == Null || $product->pb_used_s == 0 ? '-' : $product->pb_used_s,
            '@giatimtuong' => $product->pb_price_per_s == Null || $product->pb_price_per_s == 0 ? '-' : number_format($product->pb_price_per_s),
            '@giathongthuy' =>  $product->price_used_s == Null || $product->price_used_s == 0 ? '-' : number_format($product->price_used_s),
            '@tennhanvien' => $bill->staff->name,
            '@manhanvien' => $staff ? $staff->ub_tvc_code : '',
            '@thuocdonvi' => $staff->group ? $staff->group->gb_title : '',
            '@tiendattruoc' => number_format($deposit),
            '@dattruocbangchu' => $deposit_to_text,
            '@tongtienbangchu' => $amount_to_text,
            '@matkhau' => $customer_update? $random_password : '(Mật khẩu hiện tại)',
            '@chukynhanvien' => $signature_staff,
            '@tienconlai' => $remain_amount == 0 ? '-' : number_format($remain_amount),
            '@conlaibangchu' => $remain_amount == 0 ? '-' : ucfirst(self::convert_number_to_words($remain_amount)),
            '@sotienquydinh' => number_format($bill->bill_required_money),
            '@tienquydinhbangchu' => ucfirst(self::convert_number_to_words($bill->bill_required_money)),
            '@chukytkkd' => $signature_secretary,
            '@tenthuky' => $secretary,
            '@chukygiamdocsan' => $signature_director,
            '@giamdocsan' => $director_name,
            '@ngaydatcho' => isset($ngaydatcho) ? $ngaydatcho : '',
            '@ngayguimail' => date("d"),
            '@thangguimail' => date("m"),
            '@namguimail' => date("Y"),
            '@loaimatduong' => $product->note
        );
        $content = $email_template->html;
        $contentPdf = $email_template->html_pdf;
        foreach ($arrReplace as $strFrom => $strTo){
            $content = str_replace($strFrom, $strTo, $content);
            $contentPdf = str_replace($strFrom, $strTo, $contentPdf);
        }

//        $contentPdf = preg_replace("#<div id=\"pdf\">(.*?)</div>#is", "", $content);
        $pdfUrl = null;
        if ($contentPdf && trim($contentPdf) != '') {
            /** @var $data */
            $data = ['content' => $contentPdf];
            $pdf_path = 'pdf/hop-dong-' . $bill->{BOBill::ID_KEY} . '.pdf';
            PDF::loadView('pdf/index', compact('data'))->save($pdf_path);
            $pdfUrl = public_path($pdf_path);
        }
        /** @var $mail */
        $mail = SendEmailJob::dispatch($customer_info->email, new BillCreated($content, $bill->{BOBill::ID_KEY}, $status, $bill->customer_id, $pdfUrl?? null))
                ->delay(now()->addSeconds(10));
        LogController::logBill('[Mail] Đã gửi mail HĐ '.$bill->bill_code.' cho khách hàng: '.$customer_info->email, 'info', $bill->{BOBill::ID_KEY}, true, $api_gate);
        return self::jsonSuccess($mail, 'Gửi mail hợp đồng thành công!');
    }

    
    /**
     * @param BOPayment $payment
     * @return \Illuminate\Http\JsonResponse
     */
    public static function customerPayment(BOPayment $payment) {
        /** @var $bill */
        $bill = BOBill::where('bill_code',$payment->bill_code)->with([
            'product' => function($apartment) {
                $apartment
                ->select([
                    "id",
                    "pb_id",
                    "pb_title",
                    "pb_code",
                    "category_id",
                    "pb_required_money",
                ])
                    ->with([
                    "category" => function($building) {
                        $building
                        ->select([
                            "id", "cb_id", "parent_id", "cb_title", "send_mail"
                        ])
                        ->where("cb_level", 2)
                        ->with(["sibling" => function($project) {
                            $project
                                ->select(["id", "cb_id", "parent_id", "cb_title"])
                                ->where("cb_level", 1);
                        }]);
                    }]);
            }
        ])->first();
        /** @var $id */

        $id = $payment->{BOPayment::ID_KEY};
        if (!$bill) {
            LogController::logPayment('[Mail] Không thể gửi mail khi tạo thanh toán cho khách hàng do không tìm thấy HĐ: '.$payment->bill_code, 'error', $id);
            return self::jsonError('Không tìm thấy Bill');
        }
        /** @var $email */
        $email = $bill->bill_customer_info&&$bill->bill_customer_info->email? $bill->bill_customer_info->email : '';

        if (!$email) {
            LogController::logPayment('[Mail] Không thể gửi mail khi tạo thanh toán cho khách hàng do không tìm thấy Email', 'error', $id);
            return self::jsonError('Không tìm thấy sản phẩm');
        }
        /** @var $product */
        $product = $bill->product;
        if (!$product) {
            LogController::logPayment('[Mail] Không thể gửi mail khi tạo thanh toán cho khách hàng do không tìm thấy thông tin sản phẩm', 'error', $id);
            return self::jsonError('Không tìm thấy sản phẩm');
        }

        /** @var $building */
        $building = $product->category;
        if ($building->send_mail == 0) {
            LogController::logPayment('[Mail] Tòa này không cho gửi Email', 'error', $id);
            return self::jsonError('Không cho gửi Email');
        }
        /** @var $project */
        $project = $building->sibling;
        if (!$project) {
            LogController::logPayment('[Mail] Không thể gửi mail khi tạo thanh toán cho khách hàng do không tìm thấy thông tin dự án', 'error', $id);
            return self::jsonError('Không tìm thấy dự án');
        }
        /** @var $mail */
        $mail = SendEmailJob::dispatch($email, new PaymentCustomer($payment, $product, $building, $project, $bill))
            ->delay(now()->addSeconds(10));
        return self::jsonSuccess($mail, 'Gửi mail khi tạo/duyệt yêu cầu thanh toán cho khách hàng thành công!');
    }

    /**
     * @param BOBill $bill
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     */
    public static function customerStatusBill(BOBill $bill, $status) {
        /** @var $product */
        $product = BOProduct::select([
            "pb_id",
            "pb_title AS apartment", "pb_code AS code",
            "category_id",
        ])
            ->where(BOProduct::ID_KEY, $bill->product_id)
            ->with([ "category" => function($building) {
                return $building->select([
                    "id", "cb_id", "parent_id", "cb_title AS building", "send_mail"
                ])
                    ->where("cb_level", 2)
                    ->with(["sibling" => function($project) {
                        return $project->select(["id", "cb_id", "parent_id", "cb_title"])
                            ->where("cb_level", 1)
                            ->with(["investor" => function($investor) {
                                return $investor->select(["code", "name"]);
                            }]);
                    }]);
            }])
            ->first();
        if (!$product) {
            LogController::logBill('[Mail] Không thể gửi mail HĐ cho khách hàng do không tìm thấy sản phẩm', 'warning', $bill->{BOBill::ID_KEY});
            return self::jsonError('Không tìm thấy sản phẩm');
        }

        /** @var $building */
        $building = $product->category;
        if ($building->send_mail == 0) {
            LogController::logBill('[Mail] Tòa này không cho gửi Email', 'warning', $bill->{BOBill::ID_KEY});
            return self::jsonError('Không cho gửi Email');
        }
        /** @var $project */
        $project = $building&&$building->sibling? $building->sibling : null;
        if (!$project) {
            LogController::logBill('[Mail] Không thể gửi mail HĐ cho khách hàng do không tìm thấy dự án sản phẩm', 'warning', $bill->{BOBill::ID_KEY});
            return self::jsonError('Không có thông tin dự án');
        }

        $email = $bill->bill_customer_info&&$bill->bill_customer_info->email? $bill->bill_customer_info->email : '';
        if (!$email) {
            LogController::logPayment('[Mail] Không thể gửi mail khi tạo thanh toán cho khách hàng do không tìm thấy Email', 'error', $bill->bill_id);
            return self::jsonError('Không tìm thấy sản phẩm');
        }

        /** @var $mail */
        $mail = SendEmailJob::dispatch($email, new StatusBillCustomer($bill, $product->code, $project->cb_title , $status))
            ->delay(now()->addSeconds(10));
        return self::jsonSuccess($mail, 'Gửi mail cho khách hàng thành công!');
    }
}
