<?php

namespace App\Http\Controllers\API;

use App\BOBill;
use App\BOCategory;
use App\BOCustomer;
use App\BOProduct;
use App\BOTransaction;
use App\BOUser;
use App\BOUserGroup;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\TemplateEmailController;
use App\Jobs\ReleaseApartmentJob;
use App\UserCustomerMapping;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class BOTransactionController extends Controller
{
    /**
     * BOTransactionController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $DXMB_MD5 = $request->header("Content-MD5")?? null;
        if ($DXMB_MD5 != APIController::BO_PRIORITY_CODE) {
            $this->middleware("laravel.jwt");
            $this->middleware("CheckStoredJWT");
        }
        $this->middleware('CheckTVCLogin')->only(['addTransactionCustomer', 'TVCAddBillToContract']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function index(Request $request) {
        $options = $request->all();
        $page = isset($options['page'])? (int) $options['page'] : 1;
        $page_size = isset($options['size'])? (int) $options['size'] : env("PAGE_SIZE", 15);
        $filter_status = $request->input('status', null);
        $keyword = $request->input('keyword', null);

        $offset = ($page-1)*$page_size;
        /** @var $data = Transaction + Customer + Product + Staff */
        $data = BOTransaction::select([
            'trans_id AS id',
            'trans_status AS status',
            'trans_code AS status_code',
            'trans_title AS title',
            'staff_code',
            'product_id',
            'trans_created_time AS created_at'
        ])
            ->where("trans_status", "!=", env("STATUS_DELETE", -1));
        $show_filter = false;
        /** todo: get where conditions based on User Role */
        if (AuthController::is_cs_staff()) {
            /** Customer service staff filter */
            $role = 'customer_service_staff';
            $data = $data->where('trans_code', BOTransaction::STATUS_CODES['BILL_REQUEST']);
        } elseif (AuthController::is_accountant()) {
            /** Accountant filter */
            $role = 'accountant';
            $data = $data->where('trans_code', BOTransaction::STATUS_CODES['BILL_REQUEST']);
        } elseif (AuthController::is_product_manager()) {
            $role = 'PM';
            /** @var $projects */
            $projects = AuthController::getObjectsByUserRole('project', self::ROLE_GROUPS['PRODUCT_MANAGER']);
            /** @var $apartments */
            $apartments = BOProductController::getApartmentsByProject($projects);
            /** @var $staff */
            $staff = BOUserController::getStaffByGroups(BOUser::getCurrent('group_ids'));
            $data = $data->where(function ($query) use ($apartments, $staff) {
                $query->whereIn('product_id', $apartments)->orWhereIn('created_user_id', $staff);
            });
            /** Filter status query */
            if ($filter_status) $data = $data->where('trans_code', $filter_status);
            $show_filter = true;
        } else {
            $role = 'sale';
            $data = $data->where(['created_user_id' => AuthController::getCurrentUID()]);
            /** Filter status query */
            if ($filter_status!==null) $data = $data->where('trans_code', $filter_status);
            $show_filter = true;
        }
        if ($keyword) {
            $filter_products = BOProduct::where("pb_code", 'LIKE', "%$keyword%")->pluck(BOProduct::ID_KEY)->toArray();
            $data = $data->whereIn('product_id', $filter_products);
        }
        $data = $data->skip($offset)->take($page_size)
            ->with([
                "staff" => function($staff) {
                    return $staff->select(["ub_id", "ub_account_name AS account", "ub_title AS name", "group_ids"])
                        ->with(["group" => function($group) {
                            return $group->select(['gb_id', 'gb_title AS name']);
                        }]);
                },
                "product" => function ($product) {
                    return $product->select([
                        "pb_id",
                        "pb_title AS apartment", "pb_code AS code",
                        "category_id",
                        "pb_required_money AS required_money"
                    ])->with([ "category" => function($category) {
                        return $category->select([
                            "id", "cb_id", "parent_id", "cb_title AS building"
                        ])->where("cb_level", 2)
                            ->with(["sibling" => function($project) {
                                return $project->select(["id", "cb_id", "parent_id", "cb_title AS project"])->where("cb_level", 1);
                            }]);
                    }]);
                }
            ])
            ->orderBy("trans_updated_time", "DESC")->get();


        return $data? self::jsonSuccess(
            $data,
            "Thành công!",
            [
                "page" => $page,
                "size" => $page_size,
                'statuses' => BOTransaction::STATUSES,
                'status_colors' => BOTransaction::STATUS_COLORS,
                'role' => $role,
                'show_filter' => $show_filter
            ]
        ) : self::jsonError();
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id=0) {
        if (!$id) {
            return self::jsonError("Không tìm thấy ID giao dịch");
        }
        /** @var $data */
        $data = BOTransaction::where(["trans_id" => (int) $id])
            ->with(
                [
                "staff" => function($staff) {
                    return $staff->select(["ub_id", "ub_account_name AS account", "ub_title AS name", "group_ids"])
                    ->with(["group" => function($group) {
                        return $group->select([BOUserGroup::ID_KEY, 'gb_title AS name']);
                    }]);
                },
                "product" => function($product) {
                    return $product->select([
                        BOProduct::ID_KEY,
                        "pb_title AS apartment", "pb_code AS code",
                        "pb_required_money AS required_money",
                        "category_id", "bedroom",
                        "pb_door_direction AS door_direction",
                        "pb_balcony_direction AS balcony_direction",
                        "pb_price_total AS total_price",
                        "pb_price_per_s AS price_per_square",
                        "pb_used_s AS use_area",
                        "pb_built_up_s AS construction_area"
                    ])->with([ "category" => function($category) {
                        return $category->select([
                            "id", "cb_id", "parent_id", "cb_title AS building"
                        ])->where("cb_level", 2)
                            ->with(["sibling" => function($project) {
                                return $project->select(["id", "cb_id", "parent_id", "cb_title AS project"])->where("cb_level", 1);
                            }]);
                    }]);
                }
                ]
            )
            ->first();
        if ($data) {
            /** @var $buttons */
            $buttons = self::_getActionButtonsByRole($data->trans_code, $data->product? $data->product->{BOProduct::ID_KEY} : null);
            /** @var $customer_form */
            $customer_form = AuthController::is_sale_staff()
                &&
                ($data->trans_code==BOTransaction::STATUS_CODES['LOCKED_AUTO']||$data->trans_code==BOTransaction::STATUS_CODES['LOCKED_MANUAL']);
            $info = compact('buttons');
            /** @var $product_managers */
            $product_managers = BOProductController::getProductManagersByApartment($data->product_id);
            if (!$product_managers) $product_managers = [];
            /** @var $users */
            $users = BOUser::select(["ub_title AS name", "ub_email AS email"])
                ->where('ub_status', env('STATUS_ACTIVE',1))
                ->whereIn(BOUser::ID_KEY, $product_managers)
                ->get();
            $info['product_managers'] = $users;
            $info['customer_form'] = $customer_form;
            $info['statuses'] = BOTransaction::STATUSES;
            $info['status_colors'] = BOTransaction::STATUS_COLORS;

            /** @var $remindable_statuses */
            $remindable_statuses = [
                BOTransaction::STATUS_CODES['REQUEST_LOCK'],
                BOTransaction::STATUS_CODES['REQUEST_EXTEND'],
                BOTransaction::STATUS_CODES['REQUEST_CANCEL'],
            ];
            $info['allowReminding'] = AuthController::is_sale_staff() && in_array($data->trans_code, $remindable_statuses);

            return self::jsonSuccess($data, 'Thành công', $info);
        } else {
            return self::jsonError('Không tìm thấy giao dịch', ['item' => $id]);
        }
    }

    /**
     * @param $transaction_status
     * @return array
     */
    private static function _getActionButtonsByRole($transaction_status, $apartment = 0) {
        $buttons = [];
        if (AuthController::is_cs_staff() || AuthController::is_accountant()) {
            $buttons = [];
        } elseif (AuthController::is_product_manager()) {
            /** @var $product_managers */
            $product_managers = BOProductController::getProductManagersByApartment($apartment);
            /** Check responsible product managers */
            if (!$product_managers || !in_array(AuthController::getCurrentUID(), $product_managers)) return [];
            switch ($transaction_status) {
                case BOTransaction::STATUS_CODES['REQUEST_LOCK']:
                    $buttons[] = APIController::generateAppButtons(
                        BOTransaction::STATUS_CODES['CANCELED_MANUAL'],
                        'Từ chối lock',
                        false
                    );
                    $buttons[] = APIController::generateAppButtons(
                        BOTransaction::STATUS_CODES['LOCKED_MANUAL'],
                        'Duyệt lock',
                        true
                    );
                    break;
                case BOTransaction::STATUS_CODES['LOCKED_AUTO']:
                case BOTransaction::STATUS_CODES['LOCKED_MANUAL']:
                    $buttons[] = APIController::generateAppButtons(
                        BOTransaction::STATUS_CODES['CANCELED_MANUAL'],
                        'Huỷ lock',
                        false
                    );
                    break;
                case BOTransaction::STATUS_CODES['REQUEST_CANCEL']:
                    $buttons[] = APIController::generateAppButtons(
                        BOTransaction::STATUS_CODES['CANCELED_MANUAL'],
                        'Đồng ý huỷ lock',
                        false
                    );
                    break;
                default:
            }
        } else {
            switch ($transaction_status) {
                case BOTransaction::STATUS_CODES['REQUEST_LOCK']:
                    $buttons[] = APIController::generateAppButtons(
                        BOTransaction::STATUS_CODES['CANCELED_MANUAL'],
                        'Huỷ yêu cầu lock',
                        false
                    );
                    break;
                case BOTransaction::STATUS_CODES['LOCKED_AUTO']:
                case BOTransaction::STATUS_CODES['LOCKED_MANUAL']:
                    $buttons[] = APIController::generateAppButtons(
                        BOTransaction::STATUS_CODES['REQUEST_CANCEL'],
                        'Y/C huỷ lock',
                        true
                    );
                    break;
//                case BOTransaction::STATUS_CODES['CANCELED_AUTO']:
//                    $buttons[] = APIController::generateAppButtons(
//                        BOTransaction::STATUS_CODES['REQUEST_EXTEND'],
//                        'Y/C gia hạn lock',
//                        true
//                    );
//                    break;
                default:
            }
        }
        return $buttons;
    }

    /** Create Lock Request Transaction
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitForm(Request $request) {
        $form_data = $request->all();
        /** @var $product_id */
        $product_id = $form_data['product_id']?? 0;
        if (!$product_id) return self::jsonError("Yêu cầu không hợp lệ!", $form_data);
        /** @var $lock_apartment */
        $lock_apartment = BOProductController::lockApartment($product_id, false);
        if (!$lock_apartment) return self::jsonError('Không thành công! Kiểm tra tình trạng căn hộ...');
        /** @var $status_logs */
        $status_logs = [
            BOTransaction::generateStatusLog(BOTransaction::STATUS_CODES['REQUEST_LOCK']),
            BOTransaction::generateStatusLog(BOTransaction::STATUS_CODES['LOCKED_AUTO']),
        ];
        /** @var $query */
        $query = new BOTransaction;
        $query->created_user_id = AuthController::getCurrentUID();
        $query->trans_created_time = now();
        $query->staff_code = AuthController::getCurrentUID();
        $query->booking_list_id = null;
        $query->{BOTransaction::ID_KEY} = null;
        $query->trans_status = env("STATUS_ACTIVE", 1);
        if ($lock_apartment->status_id === BOProduct::STATUSES['LOCKED']) {
            $query->trans_code = BOTransaction::STATUS_CODES['LOCKED_AUTO'];
            $query->trans_title = BOTransaction::STATUSES[BOTransaction::STATUS_CODES['LOCKED_AUTO']];
            $query->auto_cancel_time = time() + BOTransaction::LIMIT_TIME;
        } else {
            $query->trans_code = BOTransaction::STATUS_CODES['REQUEST_LOCK'];
            $query->trans_title = BOTransaction::STATUSES[BOTransaction::STATUS_CODES['REQUEST_LOCK']];
        }
        $query->trans_note = $form_data['note']?? null;
        $query->product_id = $product_id;
        $query->product_tvc_code = $form_data['product_tvc_code']?? null;
        $query->updated_user_id = AuthController::getCurrentUID();
        $query->trans_updated_time = now();
        $query->trans_status_log = $status_logs;
        if (!$query->save()) {
            BOProductController::releaseApartment($product_id, false);
            return self::jsonError('Lock không thành công!');
        } else {
            if ($query->trans_code == BOTransaction::STATUS_CODES['LOCKED_AUTO']) {
                /** @var $fcm_message */
                NotificationController::notifyApartmentLocked($lock_apartment);
                /** todo: Set auto unlock timer */
                self::autoCancelTransaction($query->{BOTransaction::ID_KEY}, $product_id);
            } else {
                //** FCM send lock request */
                NotificationController::notifyLockRequest($query, $lock_apartment);
            }
            return self::jsonSuccess($query, 'Gửi yêu cầu lock thành công', compact('lock_apartment'));
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus(Request $request, $id) {
        $status = $request->input('status');
        if ($id > 0 || !$status) {
            if (!in_array($status, BOTransaction::STATUS_CODES)) {
                return self::jsonError('Trạng thái không hợp lệ', ['available_statuses' => BOTransaction::STATUS_CODES]);
            }
            /** @var $query */
            $query = BOTransaction::where(BOTransaction::ID_KEY, '=', $id)
                    ->with(['created_by:ub_id,ub_title,ub_tvc_code,ub_account_tvc', 'product:pb_code,'.BOProduct::ID_KEY])
                    ->first();
            if (!$query) {
                return self::jsonError('Không tìm thấy giao dịch', ['item' => $id]);
            }
            if (!$query->product) return self::jsonError('Sản phẩm không tồn tại!');
            $release = false;
            //** todo: Pusher on apartment status changed */
            switch ($status) {
                case BOTransaction::STATUS_CODES['CANCELED_AUTO']:
                case BOTransaction::STATUS_CODES['CANCELED_MANUAL']:
                    /** @var $apartment_release */
                    $release = BOProductController::releaseApartment($query->product_id, true, $query);
                    break;
                case BOTransaction::STATUS_CODES['LOCKED_MANUAL']:
                    if ($query->product) {
                        /** Lock & notify */
                        BOProductController::lockApartment($query->product->{BOProduct::ID_KEY}, true, true, $query->created_user_id);
                        /** todo: Set auto unlock timer */
                        self::autoCancelTransaction($id);
                    }
                    $query->auto_cancel_time = time() + BOTransaction::LIMIT_TIME;
                    break;
                default:
            }
            $current_log = $query->trans_status_log;
            $current_log[] = BOTransaction::generateStatusLog($status);
            $query->trans_code = $status;
            $query->trans_title = BOTransaction::STATUSES[$status];
            $query->updated_user_id = null;
            $query->trans_updated_time = null;
            $query->trans_status_log = $current_log;
            return $query->save()? self::jsonSuccess($query, "$query->trans_title thành công", ['new_status' => BOTransaction::STATUSES[$status], 'release' => $release]) : self::jsonError("Không thành công", $query);
        } else {
            return self::jsonError('Yêu cầu không hợp lệ', ['item' => $id]);
        }
    }

    /**
     * @param UserCustomerMapping $info
     * @return array
     */
    private static function retrieveBillCustomerInfo(UserCustomerMapping $info) {
        return [
            'name' => $info->c_name,
            'email' => $info->email,
            'phone' => $info->c_phone,
            'address' => $info->c_address,
            'permanent_address' => $info->permanent_address,
            'id_passport' => $info->id_passport,
            'issue_place' => $info->issue_place,
            'issue_date' => $info->issue_date,
            'images' => $info->c_images,
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addTransactionCustomer(Request $request) {
        /** @var $logs */
        $logs = [];
        /** @var $form_data */
        $form_data = $request->validate([
            'transaction' => 'required',
            'customer' => 'required',
            'email' => 'required|string|email',
            'name' => 'required|string',
            'phone' => 'required',
            'id_passport' => 'required',
            'issue_date' => 'required',
            'issue_place' => 'required|string',
            'amount' => 'required',
            'address' => 'required|string',
            'permanent_address' => 'required|string',
            'images' => 'required'
        ]);

        /** @var $customer_name */
        $customer_name = trim($form_data['name']);
        /** @var $customer_phone */
        $customer_phone = trim($form_data['phone']);

        /** @var $query_transaction */
        $query_transaction = BOTransaction::where([
            BOTransaction::ID_KEY => $form_data['transaction'],
            'staff_code'          => AuthController::getCurrentUID(),
            'trans_status'        => env('STATUS_ACTIVE', 1)
        ])->with('product')->first();
        if (!$query_transaction) return self::jsonError('Không tìm thấy giao dịch');
        if ($query_transaction->trans_code !== BOTransaction::STATUS_CODES['LOCKED_AUTO'] && $query_transaction->trans_code !== BOTransaction::STATUS_CODES['LOCKED_MANUAL']) {
            return self::jsonError('Yêu cầu không hợp lệ! Giao dịch phải ở trạng thái Đã lock và Chưa có TT khách hàng!');
        }

        /** @var $product */
        $product = $query_transaction->product;
        /** *********************************************************************** */
        /** *************** 1. UPDATE CUSTOMER BO - TVC ************************ */
        /** *********************************************************************** */
        /** @var $query_customer */
        $query_customer = UserCustomerMapping::where([
            'id' => (int) $form_data['customer'],
            'id_user'     => AuthController::getCurrentUID()
        ])->first();
        if (!$query_customer)  return self::jsonError('Không tìm thấy khách hàng');

        /** @var $customer */
        $customer = BOCustomer::where('cb_id_passport', $form_data['id_passport'])->first();
        /** @var $issue_date */
        $issue_date = Carbon::createFromFormat('d/m/Y', $form_data['issue_date'])->format('Y-m-d');
        if (!$customer) {
            $customer = new BOCustomer();
            $customer_id = strtotime('now');
            $customer->{BOCustomer::ID_KEY} = $customer_id;
            $customer->cb_name = $form_data['name'];
            $customer->cb_phone = trim($form_data['phone']);
            $customer->cb_account = trim($form_data['phone']);
            $customer->cb_password = AuthController::makeHash(trim($form_data['phone']));
            $customer->cb_email = $form_data['email'];
            $customer->cb_permanent_address = $form_data['permanent_address'];
            $customer->cb_id_passport = trim($form_data['id_passport']);
            $customer->cb_issue_date = $issue_date;
            $customer->cb_issue_place = $form_data['issue_place'];
            $customer->tc_created_by = null;
            $customer->cb_status = env('STATUS_INACTIVE', 0);
            $customer->source_id = BOCustomer::CUSTOMER_SOURCES['staff'];
            $logs[] = LogController::logCustomer('[addTransactionCustomer] Chuẩn bị thêm khách hàng', 'info', $customer_id, false);
        } else {
            $customer_id = $customer->{BOCustomer::ID_KEY};
            $phone_variants = $customer->phone_variants;
            $phone_variants[] = BOCustomer::generatePhoneVariant($form_data['phone']);
            $customer->phone_variants = $phone_variants;
            $logs[] = LogController::logCustomer('[addTransactionCustomer] Chuẩn bị cập nhật khách hàng', 'info', $customer_id, false);
        }
        if (!$customer->save()) return self::jsonError("Xảy ra lỗi khi khởi tạo khách hàng", $customer, $logs);

        /** Get TVC Potential Customer Code */
        $tvc_potential_code = APIController::TVC_GET_POTENTIAL_CUSTOMER_BY_IDCARD($form_data['id_passport']);
        if (!$tvc_potential_code) {
            $logs[] = LogController::logTransaction('[addTransactionCustomer] Chuẩn bị thêm KHTN trên TVC: '.$form_data['id_passport'].' - '.$form_data['phone'], 'info', $form_data['transaction'], false);
            /** @var $tvc_potential_customer */
            $tvc_potential_customer = APIController::TVC_ADD_POTENTIAL_CUSTOMER(
                $customer_name,
                $customer_phone,
                $form_data['id_passport'],
                $form_data['email'],
                $form_data['permanent_address'],
                $form_data['address'],
                null,
                true
            );
            if ($tvc_potential_customer) return self::jsonError('Xảy ra lỗi khi tạo khách hàng tiềm năng Tavico', $form_data, $logs);
            $tvc_potential_code = $tvc_potential_customer['prospect'];
        }
        $query_customer->id_customer = $customer_id;
        $query_customer->c_phone = $customer_phone;
        $query_customer->c_address = $form_data['address'];
        $query_customer->permanent_address = $form_data['permanent_address'];
        $query_customer->id_passport = $form_data['id_passport'];
        $query_customer->issue_place = $form_data['issue_place'];
        $query_customer->email = $form_data['email'];
        $query_customer->c_name = $customer_name;
        $query_customer->issue_date = $issue_date;
        $query_customer->potential_reference_code = $tvc_potential_code;
        if ($form_data['images'] && count($form_data['images'])>0) {
            $images = [];
            foreach ($form_data['images'] as $image) {
                if (starts_with($image, url(''))) {
                    $images[] = str_after($image, url(''));
                } else {
                    $image = str_replace(' ', '+', $image);
                    $image = str_after($image, 'data:image/jpeg;base64,');
                    $imageName = time() . str_random(2) . '.jpg';
                    $root_path = public_path('uploads' . '/' . $imageName);
                    File::put($root_path, base64_decode($image));
                    $images[] = '/uploads/' . $imageName;
                }
            }
            $query_customer->c_images = $images;
        }
        /** todo: Update customer info */
        if (!$query_customer->save()) {
            $logs[] = LogController::logTransaction('Cập nhật thông tin khách hàng không thành công: '. $form_data['customer'], 'error', $form_data['transaction'], false);
            return self::jsonError('Cập nhật thông tin khách hàng không thành công!', $query_customer, $logs);
        }
        $logs[] = LogController::logTransaction('Cập nhật thông tin khách hàng thành công: '. $form_data['customer'], 'info', $form_data['transaction'], false);

        /** *********************************************************************** */
        /** *************** 2. SYNC TRANSACTION PRODUCT - BUILDING - PROJECT ************** */
        /** *********************************************************************** */
        $usernameTVC = BOUser::getCurrent('ub_account_tvc');
        $branch = str_after($usernameTVC, "@");
        $sync_branch_item = APIController::TVC_SYNC_BRANCH_ITEMS($branch, $product, $query_transaction->{BOTransaction::ID_KEY});
        if (!$sync_branch_item) {
            $logs[] = LogController::logTransaction('[Transaction] Đồng bộ thông tin sản phẩm TVC sàn '.$branch.' không thành công!', 'error', $form_data['transaction'], false);
            LogController::saveLogs($logs);
            return self::jsonError('Xảy ra lỗi khi đồng bộ sản phẩm các sàn! Đã cập nhật TT khách hàng.');
        }

        /** *********************************************************************** */
        /** ****************** 3. UPDATE TRANSACTION ********************* */
        /** *********************************************************************** */
        /** Set updated transaction object */
        $query_transaction->trans_code = BOTransaction::STATUS_CODES['BILL_REQUEST'];
        $transaction_logs = $query_transaction->trans_status_log?? [];
        $transaction_logs[] = BOTransaction::generateStatusLog(BOTransaction::STATUS_CODES['BILL_REQUEST']);
        $query_transaction->trans_status_log = $transaction_logs;
        $query_transaction->trans_deposit = (int) $form_data['amount'];
        $query_transaction->trans_updated_time = null;
        $query_transaction->updated_user_id = null;
        /** todo: Update transaction status */
        if (!$query_transaction->save()) {
            $logs[] = LogController::logTransaction('[BO-addTransactionCustomer] Cập nhật trạng thái giao dịch không thành công', 'error', $form_data['transaction'], false);
            LogController::saveLogs($logs);
            return self::jsonError('Cập nhật giao dịch không thành công!');
        }

        /** *********************************************************************** */
        /** ****************** 4. CREATE NEW BILL TVC - BO **************************** */
        /** *********************************************************************** */
        /** Handle Product info */
        $price = 0;
        if ($product) {
            $price = $product->pb_price_total?? 0;
            $required_money = $product->pb_required_money?? 0;
        };
        $note = "Tên khách hàng: ".$customer_name." SDT: ".$customer_phone;
        /** @var $tvc_potential_code */
        
        /** @var $tvc_add_bill */
        $tvc_add_bill = APIController::TVC_BILL_CREATE($product? $product->pb_code : null, $tvc_potential_code, $product->stage, null, $note);
        if (!$tvc_add_bill) {
            $logs[] = LogController::logTransaction('[addTransactionCustomer] Tạo phiếu Tavico không thành công!', 'warning', $form_data['transaction'], false);
            $tvc_add_bill = [ 'systemno' => null];
        } else {
            $logs[] = LogController::logTransaction('[addTransactionCustomer] Tạo phiếu Tavico thành công!', 'info', $form_data['transaction'], false);
        };
        /** @var $bill */
        $bill = new BOBill();
        $bill->{BOBill::ID_KEY} = null;
        $bill->bill_status = env('STATUS_ACTIVE', 1);
        $bill->bill_title = null;
        $bill->staff_id = AuthController::getCurrentUID();
        $bill->customer_id = $customer_id;
        $bill->status_id = BOBill::STATUSES['STATUS_REQUEST'];
        $bill->bill_created_time = null;
        $bill->bill_updated_time = null;
        $bill->bill_total_money = $price;
        $bill->bill_required_money = $required_money;
        $bill->type = $product&&$product->stage? $product->stage : BOBill::TYPE_BOOK_ONLY;
        $bill->bill_code = BOBill::generateBillCode($bill->type, $product);
        $bill->product_id = $query_transaction->product_id;
        $bill->transaction_id = $query_transaction->{BOTransaction::ID_KEY};
        $bill->bill_approval_log = [BOBill::generateApprovalLog(BOBill::STATUSES['STATUS_REQUEST'])];
        $bill->bill_customer_info = self::retrieveBillCustomerInfo($query_customer);
        $bill->bill_reference_code = $tvc_add_bill&&$tvc_add_bill['systemno']? $tvc_add_bill['systemno'] : null;
        $bill->bill_log_edit = [self::generateEditLog('type', $bill->type)];
        /** todo: Save new bill */
        if (!$bill->save()) {
            $msg = 'Khởi tạo Hợp đồng ' . BOBill::TYPE_TEXTS[$bill->type] . ' không thành công';
            $logs[] = LogController::logTransaction($msg, 'error', $form_data['transaction'], false);
            LogController::saveLogs($logs);
            return self::jsonError($msg);
        }
        /** todo: Store & Pusher on apartment status changed */
        BOProductController::changeStatus($query_transaction->product_id, BOProduct::STATUSES['ADD_CUSTOMER'], true);
        $logs[] = LogController::logTransaction('Đã bổ sung khách hàng cho GD & Khởi tạo hợp đồng '. BOBill::TYPE_TEXTS[$bill->type] . ' '. $bill->bill_code. '!', 'info', $form_data['transaction'], false);
        LogController::saveLogs($logs);
        /** todo: Notify Bill Created */
        NotificationController::notifyTransactionCustomerAdded($query_transaction, $product);
        NotificationController::notifyBillCreated($bill, $product);
        /** @var $invoice */
        $invoice = TemplateEmailController::generateBillInvoice($bill, $bill->status_id, false, true);
        $pdf = null;
        if ($invoice && $invoice['pdf']) {
            /** @var $data */
            $data = ['content' => $invoice['pdf']];
            $pdf_path = 'pdf/hop-dong-' . $bill->{BOBill::ID_KEY} . '.pdf';
            PDF::loadView('pdf/index', compact('data'))->save($pdf_path);
            $pdf = asset($pdf_path);
        }
        $msg = 'Cập nhật khách hàng thành công, đã tạo Hợp đồng ' . BOBill::TYPE_TEXTS[$bill->type] . ' ' . $bill->bill_code;
        return self::jsonSuccess($bill, $msg, [
            'pdf' => $pdf
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function TVCAddBillToContract(Request $request) {
        if (!AuthController::is_sale_staff()) return self::jsonError('Thao tác chỉ dành cho NVKD!');
        $request->validate([
            'customer'  => 'required',
            'bill'      => 'required',
        ]);
        /** @var $bill */
        $bill = BOBill::where([
            BOBill::ID_KEY => (int) $request->input("bill"),
            'staff_id'      => AuthController::getCurrentUID()
        ])->with('product')->first();
        if (!$bill) return self::jsonError('Không tìm thấy hợp đồng!');
        if ($bill->bill_reference_code) return self::jsonError('Hợp đồng đã tồn tại phiếu! '.$bill->bill_reference_code);
        /** @var $product */
        $product = $bill->product;
        if (!$product || !$product->pb_code) return self::jsonError('Không tìm thấy sản phẩm!');
        /** @var $customer */
        $customer = UserCustomerMapping::where([
            'id_user' => AuthController::getCurrentUID(),
            'id_customer' => (int) $request->input("customer")
        ])->first();
        if (!$customer) return self::jsonError('Không tìm thấy khách hàng!');
        if (!$customer->potential_reference_code) return self::jsonError('Không tìm thấy mã khách hàng tiềm năng!');
        /** @var $customer_info */
        $customer_info = $bill->bill_customer_info;
        $customer_name = $customer_info->name;
        $customer_phone = $customer_info->phone;
        if (!$customer_name || !$customer_phone) return self::jsonError('Thông tin khách hàng thiếu!', $customer_info);
        /** @var $note */
        $note = "Tên khách hàng: ".$customer_name." SDT: ".$customer_phone;
        /** @var $tvc_potential_code */
        $tvc_potential_code = $customer->potential_reference_code?? null;
        /** @var $tvc_add_bill */
        $tvc_add_bill = APIController::TVC_BILL_CREATE($product->pb_code, $tvc_potential_code, $product->stage, null, $note);
        if (!$tvc_add_bill) {
            Log::error('Tạo giao dịch Tavico không thành công!', array_wrap($tvc_add_bill));
            return self::jsonError('Tạo phiếu TVC không thành công!');
        }
        $bill->bill_reference_code = $tvc_add_bill['systemno'];
        if ($bill->save()) return self::jsonSuccess('Gán phiếu cho HĐ thành công. Code: '.$bill->bill_reference_code);
        return self::jsonError('Tạo phiếu cho HĐ thất bại!');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function TVCUpdateBillStatus(Request $request) {
        if (!AuthController::is_product_manager()) return self::jsonError('Thao tác chỉ dành cho QLSP!');
        $request->validate([
            'bill' => 'required',
            'tvc_status' => 'required'
        ]);
        /** @var $allowed_statuses */
        $allowed_statuses = [
            'XNDCO',
            'XNDCH'
        ];
        if (!in_array($request->input('tvc_status'), $allowed_statuses)) return self::jsonError('Trạng thái phiếu không hợp lệ!');
        /** @var $bill */
        $bill = BOBill::where([
            BOBill::ID_KEY  => (int) $request->input("bill")
        ])->with('staff')->first();
        if (!$bill) return self::jsonError('Không tìm thấy hợp đồng!', $request->all());
        if (!$bill->bill_reference_code) return self::jsonError('Hợp đồng không tồn tại phiếu TVC!', $bill);
        if (!$bill->staff) return self::jsonError('Không tìm thấy NVKD!', $bill);
        /** @var $branch_product_manager */
        $branch_product_manager = BOUser::getCurrent('group_ids');
        /** @var $staff */
        $staff = $bill->staff;
        $branch_check = $staff && $staff->group_ids == $branch_product_manager ? 1 : 0;
        $branch = $staff? str_after($staff->ub_account_tvc, '@') : null;
        $tvc_query = APIController::TVC_UPDATE_BOOKING_STATUS(
            $bill->bill_reference_code,
            $request->input('tvc_status'),
            $request->input('bill'),
            $branch_check,
            $branch
        );
        if (!$tvc_query || !$tvc_query['success']) return self::jsonError('Không thành công, vui lòng xem log!', $request->all());
        return self::jsonSuccess($tvc_query, 'Cập nhật thành công!');
    }

    /**
     * @param int $transaction
     * @param int $apartment
     * @return \Illuminate\Foundation\Bus\PendingDispatch
     */
    public static function autoCancelTransaction($transaction, $apartment = 0) {
        return ReleaseApartmentJob::dispatch($transaction, $apartment)->delay(now()->addSeconds(BOTransaction::LIMIT_TIME));
    }
}
