<?php

namespace App\Http\Controllers\API;

use App\BOCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BOCategoryController extends Controller
{
    /**
     * BOCategoryController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $DXMB_MD5 = $request->header("Content-MD5")?? null;
        if ($DXMB_MD5 != APIController::BO_PRIORITY_CODE) {
            $this->middleware("laravel.jwt")->except(['getCategoryAjax']);
            $this->middleware("CheckStoredJWT")->except(['getCategoryAjax']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request) {
        $options = $request->all();
        $page = isset($options['page'])? (int) $options['page'] : 1;
        $page_size = isset($options['size'])? (int) $options['size'] : env("PAGE_SIZE", 15);
        /** @var $offset */
        $offset = ($page-1)*$page_size;
        /** @var $order */
        $order = $request->input('order', []);
        /** @var $keyword */
        $keyword = $request->input('keyword', null);
        /** @var $data */
        $data = BOCategory::select([
            BOCategory::ID_KEY . ' as id',
            'parent_id',
            'cb_code as code',
            'reference_code',
            'cb_title AS title',
            'cb_description AS description',
            'type', 'apartment_grid', 'active_booking',
            'ub_updated_time AS updated_time'
        ])
            ->where("cb_status", env("STATUS_ACTIVE", 1))
            ->where('cb_level', 1)
            ->whereNotNull('cb_title')
            ->where('cb_title', '!=', '');
        if ($keyword) $data = $data->where('cb_title', 'LIKE', "%$keyword%");
        if ($order) {
            foreach ($order as $field => $value) {
                $data = $data->orderBy($field, $value);
            }
        } else {
            $data = $data->orderBy('cb_title', 'ASC');
        }
        $data = $data
            ->skip($offset)
            ->take($page_size)
            ->get();
        if (!$data) return self::jsonError('Không tìm thấy dữ liệu!');
        return self::jsonSuccess($data, "Thành công!");
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id=0) {
        if (!$id) {
            return self::jsonError("Không tìm thấy ID hóa đơn");
        }
        $data = BOCategory::where([BOCategory::ID_KEY => (int) $id])->first();
        if ($data) {
            return self::jsonSuccess($data);
        } else {
            return self::jsonError('Không tìm thấy hóa đơn', ['item' => $id]);
        }
    }

    /***
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function submitForm(Request $request, $id = 0) {
        if ($id>0) {
            $query = BOCategory::where([BOCategory::ID_KEY => $id])->first();
            if (!$query) {
                return self::jsonError('Không tìm thấy hóa đơn', ['item' => $id]);
            }
        } else {
            $query = new BOCategory;
            $query->ub_created_time = now();
            $query->created_user_id = null;
        }
        $form_data = $request->all();
        $query->{BOCategory::ID_KEY} = $id>0? $id : null;
        if ($form_data["status"]) {
            $query->cb_status = (int)$form_data["status"];
        }
        $query->ub_updated_time = now();
        $query->cb_title = $form_data["title"]?? null;
        $query->reference_code = $form_data["reference_code"]?? null;
        $query->parent_id = $form_data["parent"]? (int)$form_data["parent"] : null;
        $query->cb_description = $form_data["description"]?? null;
        $query->cb_code = $form_data["code"]?? null;
        $query->updated_user_id = null;

        return $query->save()? self::jsonSuccess($query, $id? 'Cập nhật thành công!' : 'Thêm dữ liệu thành công!') : self::jsonError("Lưu không thành công!", $query);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getCategoryAjax(Request $request) {
        $level = $request->input('level', 1);
        $page = $request->input('page',1);
        $size = $request->input('size', 15);
        /** @var $keyword */
        $keyword = $request->input('keyword', null);
        /** @var $offset */
        $offset = ($page - 1) * $size;
        /** @var $data */
        $data = BOCategory::select([
            BOCategory::ID_KEY . ' AS id',
            'cb_code AS code',
            'cb_title AS title',
            'parent_id'
        ])
            ->where('cb_status', env('STATUS_ACTIVE',1))
            ->where('cb_level', (int) $level);
        if ($keyword) $data = $data->where(function ($query) use ($keyword) {
            $query->where('cb_code', 'LIKE', "%$keyword%")->orWhere('cb_title', 'LIKE', "%$keyword%");
        });
        $data = $data
            ->skip($offset)
            ->take($size)
            ->orderBy('cb_code', 'ASC')
            ->get();
        if (!$data) return self::jsonError('Không thể truy vấn dữ liệu!');
        return self::jsonSuccess($data, 'Done', $request->all());
    }

    /***
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getBuildingsByProjectCode(Request $request) {
        /** @var $project_code */
        $project_code = $request->input('code', null);
        if (!$project_code) return self::jsonError('Yêu cầu không hợp lệ!');
        /** @var $project */
        $project = BOCategory::select(['id', 'reference_code'])->where('reference_code', $project_code)->first();
        if($project){
            $buildings = BOCategory::select(['cb_id AS id', 'cb_code AS code', 'cb_title AS title'])
                ->where([
                    'cb_level' => 2,
                    'parent_id' => $project->id,
                    'cb_status' => env("STATUS_ACTIVE", 1)
                ])
                ->get();
            return $buildings&&count($buildings)>0? self::jsonSuccess($buildings) : self::jsonSuccess('Không tìm thấy toà nhà thuộc dự án này!');
        }else{
            return self::jsonError('Dự án chưa cập nhật bảng hàng!', ['code' => $project_code]);
        }
    }
}
