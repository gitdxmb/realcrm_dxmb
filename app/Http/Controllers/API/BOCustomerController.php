<?php

namespace App\Http\Controllers\API;

use App\BOCustomer;
use App\BOUser;
use App\UserCustomerMapping;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
class BOCustomerController extends Controller
{
    /**
     * BOCustomerController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $DXMB_MD5 = $request->header("Content-MD5")?? null;
        if ($DXMB_MD5 != APIController::BO_PRIORITY_CODE) {
            $this->middleware("laravel.jwt");
            $this->middleware("CheckStoredJWT");
        }
//        $this->middleware('CheckTVCLogin')->only('submitForm');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request) {
        $options = $request->all();
        $page = isset($options['page'])? (int) $options['page'] : 1;
        $page_size = isset($options['size'])? (int) $options['size'] : null;
        /** @var $offset */
        $offset = ($page-1)*$page_size;
        /** @var $keyword */
        $keyword = $request->input('keyword', null);
        /** @var $type */
        $type = $request->input('type', null);

        /** @var $data */
        $data = UserCustomerMapping::select([
            'id',
            'id_customer',
            'c_title AS title',
            'c_phone AS phone',
            'c_address AS address',
            'type'
        ])->where('id_user', AuthController::getCurrentUID());
        if ($keyword) {
            $data = $data->where(function ($query) use ($keyword) {
                $query->where('c_title', 'LIKE', '%' . trim($keyword) . '%')
                ->orWhere('c_name', 'LIKE', '%' . trim($keyword) . '%')
                ->orWhere('id_passport', 'LIKE', '%' . trim($keyword) . '%')
                ->orWhere('c_phone', 'LIKE', '%' . trim($keyword) . '%');
            });
        }
        if ($type!=null) {
            $data = $data->where('type', (int) $type);
        }
        /** todo: Paginate */
        if ($page_size) $data = $data->take($page_size)->skip($offset);
        /** todo: Order list */
        if (isset($options['orderBy']) && $options['orderBy']=='updated_at') {
            $data = $data->orderBy('updated_at', 'DESC');
        } else {
            $data = $data->orderBy('title', 'ASC');
        }
        $data = $data->get();

        if (!$data) return self::jsonError("Không tìm thấy khách hàng nào.");
        return self::jsonSuccess($data, "Thành công!", ["page" => $page, "size" => $page_size, "keyword" => $keyword, "type" => $type]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id=0) {
        if (!$id) {
            return self::jsonError("Không tìm thấy ID khách hàng");
        }
        /** @var $where */
        $where = ['id' => (int) $id, 'id_user' => AuthController::getCurrentUID()];
        /** @var $data */
        $data = UserCustomerMapping::where($where)->first();
        if (!$data) return self::jsonError('Không tìm thấy khách hàng', ['item' => $id]);

        /** @var $images */
        $images = $data->c_images?? [];
        foreach ($images as &$image) {
            $image = url($image);
        }
        $data->c_images = $images;
        return self::jsonSuccess($data, 'Lấy thông tin khách hàng thành công!', $where);
    }

    /***
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function submitForm(Request $request, $id=0) {
        $form_data = $request->all();
        if (!$form_data['title'] || !$form_data['phone']) {
            return self::jsonError("Không được để trống các trường bắt buộc (*)");
        }
        $form_data['phone'] = str_replace(' ', '', $form_data['phone']);
        /** Validate customer existence */
        if ($id==0) {
            $where = [
                'id_user' => AuthController::getCurrentUID(),
            ];
            if ($form_data['id_passport']) $where['id_passport'] = $form_data['id_passport'];
            $invalid = UserCustomerMapping::where($where)->first();
            if ($invalid) return self::jsonError('CMND đã tồn tại với khách hàng khác!', [$invalid, $where]);
            $customer = new UserCustomerMapping();
        } else {
            $customer = UserCustomerMapping::find($id);
            if (!$customer || $customer->id_user!==AuthController::getCurrentUID()) return self::jsonError('Không tìm thấy khách hàng');
            $customer->id_user = BOUser::getCurrentUID();
        }
        /** @var $logs */
        $logs = [];
        //** todo: FindOrAdd Potential Customer on TVC */
//        $tvc_potential_data = APIController::TVC_GET_POTENTIAL_CUSTOMER_BY_PHONE_RETURN_ALL_DATA($form_data['phone']);
//        $tvc_potential_code = $tvc_potential_data&&$tvc_potential_data['prospect']? $tvc_potential_data['prospect'] : null;
//        if (!$tvc_potential_code) {
//            $logs[] = LogController::logCustomer('[BO-CustomerSubmit] Chuẩn bị thêm KHTN với SĐT '.$form_data['phone'], 'info', $id, false);
//            $tvc_add_potential_customer = APIController::TVC_ADD_POTENTIAL_CUSTOMER($form_data['name']?? $form_data['title'], $form_data['phone']);
//            if (!$tvc_add_potential_customer) {
//                $msg = '[BO-CustomerSubmit] Xảy ra lỗi khi thêm khách hàng trên Tavico. Hãy đăng nhập lại!';
//                $logs[] = LogController::logCustomer($msg, 'error', $id, false);
//                return self::jsonError($msg, [], $logs);
//            } else {
//                $tvc_potential_code = $tvc_add_potential_customer["prospect"];
//                $msg = '[BO-CustomerSubmit] Đã thêm Khách hàng tiềm năng trên Tavico: '.$tvc_potential_code;
//                $logs[] = LogController::logCustomer($msg, 'info', $id, false);
//            }
//        }else{
//            $logs[] = LogController::logCustomer('[BO-CustomerSubmit] Đã tìm thấy khách hàng tiềm năng: '.$tvc_potential_code.'. Chuẩn bị cập nhật Tavico...', 'info', $id, false);
//            if(BOUser::getCurrent('ub_tvc_code') ==  $tvc_potential_data['employee']){
//                $aryUpdatePotential = array();
//                $aryUpdatePotential['prospect'] = $tvc_potential_code ? $tvc_potential_code : '';
//                $aryUpdatePotential['idcard'] = trim($form_data['id_passport']) ? trim($form_data['id_passport']) : '';
//                $aryUpdatePotential['address'] = $form_data['permanent_address'] ? $form_data['permanent_address'] : '';
//                $aryUpdatePotential['comments'] = $form_data['address'] ? $form_data['address'] : '';
//                $aryUpdatePotential['name'] = $form_data['name'] ? $form_data['name'] : '';
//                $aryUpdatePotential['email'] = $form_data['email'] ? $form_data['email'] : '';
//                APIController::TVC_UPDATE_POTENTIAL($aryUpdatePotential);
//            }
//
//        }
        $customer_id = 0;
        if ($form_data['id_passport']) {
            /** @var $bo_init */
            $bo_init = BOCustomer::where('cb_id_passport', $form_data['id_passport'])->first();
            $issue_date = $form_data['issue_date']? Carbon::createFromFormat('d/m/Y', $form_data['issue_date'])->format('Y-m-d') : null;
            if (!$bo_init) {
                $bo_init = new BOCustomer;
                $customer_id = strtotime('now');
                $bo_init->{BOCustomer::ID_KEY} = $customer_id;
                $bo_init->cb_name = $form_data['name']?? $form_data['title']?? null;
                $bo_init->cb_phone = trim($form_data['phone']);
                $bo_init->cb_account = trim($form_data['phone']);
                $bo_init->cb_password = AuthController::makeHash(trim($form_data['phone']));
                $bo_init->cb_email = $form_data['email']?? null;
                $bo_init->cb_permanent_address = $form_data['permanent_address']?? null;
                $bo_init->cb_id_passport = trim($form_data['id_passport']);
                $bo_init->cb_issue_date = $issue_date;
                $bo_init->cb_issue_place = $form_data['issue_place']?? null;
                $bo_init->tc_created_by = null;
                $bo_init->cb_status = env('STATUS_INACTIVE', 0);
                $bo_init->source_id = BOCustomer::CUSTOMER_SOURCES['staff'];
                $logs[] = LogController::logCustomer('[BO-CustomerSubmit] Đã thêm Khách hàng BO với mật khẩu mặc định', 'info', $id, false);
            } else {
                $customer_id = $bo_init->{BOCustomer::ID_KEY};
                if ($form_data['phone']) {
                    $phone_variants = $bo_init->phone_variants;
                    $phone_variants[] = BOCustomer::generatePhoneVariant($form_data['phone']);
                    $bo_init->phone_variants = $phone_variants;
                }
            }
            if (!$bo_init->save()) return self::jsonError("Xảy ra lỗi khi khởi tạo khách hàng", $bo_init);
        }
        //** todo: Store tvc code to BO */
        $customer->type = 0;
        $customer->id_customer = $customer_id;
        $customer->c_title = trim($form_data['title']);
        $customer->c_name = $form_data['name']?? null;
        $customer->email = $form_data['email']?? null;
        $customer->c_phone = trim($form_data['phone']);
        $customer->c_address = $form_data['address']?? null;
        $customer->permanent_address = $form_data['permanent_address']?? null;
        $customer->c_note = $form_data['note']?? null;
        $customer->c_rating = $form_data['rating']?? 0;
        $customer->id_passport = trim($form_data['id_passport']);
        $customer->issue_date = $issue_date;
        $customer->issue_place = $form_data['issue_place']?? null;
        $success = false;
        if ($form_data['images'] && count($form_data['images'])>0) {
            $images = [];
            foreach ($form_data['images'] as $image) {
                if (starts_with($image, url(''))) {
                    $images[] = str_after($image, url(''));
                } else {
                    $image = str_replace(' ', '+', $image);
                    $image = str_after($image, 'data:image/jpeg;base64,');
                    $imageName = time() . str_random(2) . '.jpg';
                    $root_path = public_path('uploads' . '/' . $imageName);
                    File::put($root_path, base64_decode($image));
                    $images[] = '/uploads/' . $imageName;
                }
            }
            $customer->c_images = $images;
        }
        $msg = "Lưu không thành công!";
        if ($customer->save()) {
            $success = true;
            $msg = ($id>0? "Cập nhật" : "Thêm mới") . " thành công";            
        }
        return $success? self::jsonSuccess($customer, $msg, $logs) : self::jsonError($msg, $customer, $logs);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function updateCustomerMapping(Request $request) {
        $form_data = $request->all();
        $customer_id = $form_data['customer'];
        $staff = BOUser::getCurrentUID();
        $query = UserCustomerMapping::where([
            'id_user' => $staff,
            'id_customer' => $customer_id
        ])->first();
        if (!$customer_id || !$query) {
            return self::jsonError('Không tồn tại khách hàng hoặc nhân viên không có quyền cập nhật khách hàng', [
                'customer' => $customer_id
            ]);
        } else {
            $query->c_title = $form_data['name']?? $query->c_title;
            $query->c_phone = $form_data['phone']? str_replace(' ', '', $form_data['phone']) : $query->c_phone;
            $query->c_address = $form_data['address']?? $query->c_address;
            $query->email = $form_data['email']?? $query->email;
            $query->c_note = $form_data['note']?? $query->c_note;
            $query->id_passport = $form_data['id_passport']?? $query->id_passport;
            $query->issue_date = $form_data['issue_date']?? $query->issue_date;
            $query->issue_place = $form_data['issue_place']?? $query->issue_place;
            $query->permanent_address = $form_data['permanent_address']?? $query->permanent_address;
            return $query->save()? self::jsonSuccess($query) : self::jsonError("Cập nhật khách hàng không thành công!", $form_data);
        }
    }

}
