<?php

namespace App\Http\Controllers\API\Customer;

use App\BOBill;
use App\BOPayment;
use App\BOProduct;
use App\BOUser;
use App\BOUserGroup;
use App\Http\Controllers\API\APIController;
use App\Http\Controllers\API\BOPaymentController;
use App\Http\Controllers\API\BOProductController;
use App\Http\Controllers\NotificationController;
use App\PaymentMethod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BOBillController extends Controller
{
    /** Bill list by Customer
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request) {
        $options = $request->all();
        $page = isset($options['page'])? (int) $options['page'] : 1;
        $page_size = isset($options['size'])? (int) $options['size'] : env("PAGE_SIZE", 15);

        $offset = ($page-1)*$page_size;
        $data = BOBill::select("*")->where([
            ["bill_status", "!=", env("STATUS_DELETE", -1)],
            ['status_id', '>', BOBill::STATUSES['STATUS_REQUEST']],
            ['customer_id', '=', AuthController::getCurrentUID()]
        ])
            ->has('product')
            ->with([
                'staff' => function($staff) {
                    $staff->select([
                        'ub_id',
                        'ub_title AS name',
                        'ub_account_name AS account',
                        'ub_email AS email',
                        'ub_avatar AS avatar'
                    ]);
                },
                'product' => function($apartment) {
                    return $apartment->select([
                        "pb_id",
                        "pb_title AS apartment", "pb_code AS code",
                        "category_id",
                        "pb_required_money AS required_money"
                    ])
                        ->with([ "category" => function($building) {
                            return $building->select([
                                "id", "cb_id", "parent_id", "cb_title AS building"
                            ])
                                ->where("cb_level", 2)
                                ->with(["sibling" => function($project) {
                                    return $project->select(["id", "cb_id", "parent_id", "cb_title AS project"])
                                        ->where("cb_level", 1);
                                }]);
                        }]);
                }
            ])
            ->orderBy('bill_updated_time', 'DESC')
            ->skip($offset)->take($page_size)
            ->get();
        return self::jsonSuccess($data, "Thành công!", ['filter' => $options, 'statuses' => BOBill::STATUS_TEXTS, 'status_colors' => BOBill::STATUS_COLORS]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id=0) {
        if (!$id) {
            return self::jsonError("Không tìm thấy ID hóa đơn");
        }
        /** @var $item */
        $item = BOBill::select(['id', 'staff_id'])->where([BOBill::ID_KEY => (int) $id, 'customer_id' => AuthController::getCurrentUID()])->first();
        if (!$item) return self::jsonError('Không tìm thấy hóa đơn', ['item' => $id]);

        /** @var $staff_id */
        $staff_id = $item->staff_id;

        /** @var $data */
        $data = BOBill::select([
            '*',
            'bill_required_money AS required_money',
            'bill_total_money AS total_money',
        ])
            ->where('id', $item->id)->with([
            'staff' => function($staff) {
                $staff->select([
                    'ub_id',
                    'ub_title AS name',
                    'ub_account_name AS account',
                    'ub_email AS email',
                    'ub_avatar AS avatar',
                    'group_ids'
                ])->with(['group' => function($group) {
                    $group->select([BOUserGroup::ID_KEY, 'gb_title AS name']);
                }]);
            },
            'customer' => function($customer) use ($staff_id) {
                $customer->select([
                    'id_user',
                    'id_customer',
                    'c_title AS name',
                    'c_phone AS phone',
                    'c_address AS address',
                    'id_passport',
                    'issue_date',
                    'issue_place',
                    'email',
                    'permanent_address'
                ])->where('id_user', $staff_id);
            },
            'product' => function($apartment) {
                return $apartment->select([
                    "pb_id", "bedroom",
                    "pb_title AS apartment", "pb_code AS code",
                    "category_id", "pb_door_direction AS door_direction",
                    "pb_balcony_direction AS balcony_direction",
                    "pb_price_total AS total_price",
                    "pb_price_per_s AS price_per_square",
                    "price_used_s AS price_in_use",
                    "pb_used_s AS use_area",
                    "pb_built_up_s AS construction_area",
                    "pb_required_money AS required_money"
                ])
                    ->with([ "category" => function($building) {
                        return $building->select([
                            "id", "cb_id", "parent_id", "cb_title AS building"
                        ])
                            ->where("cb_level", 2)
                            ->with(["sibling" => function($project) {
                                return $project->select(["id", "cb_id", "parent_id", "cb_title AS project"])
                                    ->where("cb_level", 1);
                            }]);
                    }]);
            },
            'transaction:trans_id,trans_deposit',
            'payments' => function($payments) {
                $payments->select(['bill_code', 'pb_response_money AS amount'])->where('pb_status', BOPayment::STATUS['APPROVED']);
            }
        ])->first();
        if ($data) {
            /** @var $paid */
            $paid = 0;
            foreach ($data->payments as $payment) {
                $paid += (int) $payment->amount;
            }
            /** @var $unpaid */
            $unpaid = ($data->bill_required_money? (int) $data->bill_required_money : 0) - $paid;
            $pdf = null;
            if (file_exists(public_path('pdf/hop-dong-'.$id.'.pdf'))) $pdf = url('pdf/hop-dong-'.$id.'.pdf');
            $info = [
                'statuses' => BOBill::STATUS_TEXTS,
                'status_colors' => BOBill::STATUS_COLORS,
                'buttons' => [],
                'paid' => number_format($paid),
                'unpaid' => number_format($unpaid),
                'pdf'    => $pdf
            ];
            switch ($data->status_id) {
                case BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']:
                    $info['buttons'] = [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_APPROVED_BY_CUSTOMER'], 'Xác nhận HĐ', true, 'policy'),
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_DISAPPROVED_BY_CUSTOMER'], 'Từ chối HĐ', false),
                    ];
                    break;
                default:
            }
            return self::jsonSuccess($data, 'Thành công', $info);
        } else {
            return self::jsonError('Không tìm thấy hóa đơn', ['item' => $id]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function makePayment(Request $request) {
        /** @var $form_data */
        $form_data = $request->validate([
            'contract' => 'required',
            'amount' => 'required'
        ]);
        $contract = BOBill::where("bill_status", "!=", env("STATUS_DELETE", -1))
            ->where([
                BOBill::ID_KEY => (int) $form_data['contract'],
                'customer_id' => AuthController::getCurrentUID(),
            ])
            ->first();
        if (!$contract) return self::jsonError('Không tìm thấy hợp đồng!');
        if ($contract->status_id!==BOBill::STATUSES['STATUS_PAYING']) {
            return self::jsonError('Không thể thanh toán do tình trạng hợp đồng!');
        }
        return self::jsonError('Tính năng đang phát triển, vui lòng thử lại sau!');
        $vnpay_url = BOPaymentController::vnpay($contract->bill_code, 'Thanh toán hợp đồng '.$contract->bill_code, $form_data['amount']);
        return $vnpay_url? self::jsonSuccess(['url' => $vnpay_url, 'code' => '00']) : self::jsonError('Xảy ra lỗi khi thanh toán qua VNPay!');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(Request $request, $id=0) {
        //** Check ID */
        if (!$id) {
            return self::jsonError('Không tìm thấy ID hợp đồng');
        }
        $status = $request->input('status', null);
        $allowed_status = [
            BOBill::STATUSES['STATUS_DISAPPROVED_BY_CUSTOMER'],
            BOBill::STATUSES['STATUS_APPROVED_BY_CUSTOMER']
        ];
        //** Check whether customer's action is permitted */
        if ($status===null || !in_array($status, $allowed_status)) {
            return self::jsonError('Thao tác không hợp lệ!', compact('status'));
        }
        //** Check bill existence */
        $bill = BOBill::where([BOBill::ID_KEY => $id, 'customer_id' => AuthController::getCurrentUID()])
                        ->with('transaction:trans_id,bill_id,trans_deposit')->first();
        if (!$bill) return self::jsonError('Không tìm thấy hợp đồng!');
        //** Check bill status */
        if ($bill->status_id != BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']) {
            return self::jsonError('Không thể thao tác với hợp đồng!');
        }
        /** todo: Update Bill status */
        $bill->status_id = (int) $status;
        $approval_logs = $bill->bill_approval_log?? [];
        $approval_logs[] = BOBill::generateApprovalLog((int) $status, AuthController::getCurrentUID());
        $bill->bill_approval_log = $approval_logs;
        if($status == BOBill::STATUSES['STATUS_DISAPPROVED_BY_CUSTOMER']){
            $staff = BOUser::where('ub_id', $bill->staff_id)->first();
            $branch = $staff? str_after($staff->ub_account_tvc, '@') : null;            
            APIController::TVC_CANCEL_BOOKING_REQUEST($bill->bill_reference_code, $id, true, $branch);
        }
        if (!$bill->save()) return self::jsonError('Cập nhật hợp đồng không thành công!');
        if($status == BOBill::STATUSES['STATUS_APPROVED_BY_CUSTOMER']){
            BOProductController::changeStatus($bill->product_id, BOProduct::STATUSES['CUSTOMER_CONFIRM'], true, true);
        } else {
            BOProductController::changeStatus($bill->product_id, BOProduct::STATUSES['LOCKED'], true, true);
        }
        NotificationController::notifyBillStatusUpdated($bill);
        return self::jsonSuccess($bill);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLog($id) {
        /** @var $bill */
        $bill = BOBill::select([
            BOBill::ID_KEY,
            'bill_code',
            'staff_id',
            'status_id',
            'bill_created_time',
            'bill_completed_time',
            'bill_approval_log',
            'bill_updated_time',
            'bill_log_edit'
        ])
            ->where('customer_id', AuthController::getCurrentUID())
            ->where(BOBill::ID_KEY, (int) $id)
            ->with([
                'payments' => function($payments) {
                    $payments->select('*')->with([
                        'paymentMethod:pm_id,pm_title',
                        'responseStaff' => function($accountant) {
                            $accountant->select([BOUser::ID_KEY, 'ub_title AS name', 'group_ids'])->with('group:gb_id,gb_title');
                        }
                    ])->orderBy('request_time', 'DESC');
                }
            ])
            ->first();
        if (!$bill) return self::jsonError('Không tìm thấy hợp đồng!', ['item' => $id]);
        $users = [$bill->staff_id];
        $info = ['statuses' => BOBill::STATUS_TEXTS];
        //** todo: Format Approval Logs */
        $approval_logs = $bill->bill_approval_log;
        foreach ($approval_logs as &$item) {
            $item['TIME'] = $item['TIME']? date('H:i d-m-Y', $item['TIME']) : null;
            $users[] = $item['CREATED_BY'];
            $item['VALUE'] = isset($item['VALUE'])? (string) $item['VALUE'] : null;
        }
        $bill->bill_approval_log = array_reverse($approval_logs);

        //** todo: Collect Customer + Users involved in Bill Logs */
        $info['users'] = BOUser::select([BOUser::ID_KEY, 'ub_title AS name', 'group_ids', 'ub_staff_code AS staff_code', 'ub_account_tvc AS tvc_account'])
            ->whereIn(BOUser::ID_KEY, $users)
            ->with('group:gb_id,gb_title')
            ->get()->keyBy(BOUser::ID_KEY);
        $info['users'][AuthController::getCurrentUID()] = [
            'name' => AuthController::getCurrentUser('cb_name')
        ];
        return self::jsonSuccess($bill, 'Thành công', $info);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function InitPayment($id = 0) {
        if (!$id) return self::jsonError('Không tìm thấy ID hợp đồng');
        /** @var $bill */
        $bill = BOBill::select(['*', 'bill_total_money AS total'])->where([
            BOBill::ID_KEY => (int) $id,
            'customer_id' => AuthController::getCurrentUID()
        ])->with([
            'product' => function($product) {
                $product->select(['pb_id', 'pb_code AS code']);
            },
            'transaction:trans_id,trans_deposit',
            'payments' => function($payments) {
                $payments->select(['bill_code', 'pb_response_money AS amount'])->where('pb_status', BOPayment::STATUS['APPROVED']);
            }
        ])->first();
        if (!$bill) return self::jsonError('Không tìm thấy hợp đồng');
        switch ($bill->status_id) {
            case BOBill::STATUSES['STATUS_DISAPPROVED_BY_MANAGER']:
            case BOBill::STATUSES['STATUS_DISAPPROVED_BY_CUSTOMER']:
            case BOBill::STATUSES['STATUS_DISAPPROVED']:
            case BOBill::STATUSES['STATUS_CANCELED']:
                return self::jsonError('Không thể thanh toán với hợp đồng đã huỷ!');
                break;
            case BOBill::STATUSES['STATUS_BOOKED']:
            case BOBill::STATUSES['STATUS_DEPOSITED']:
                return self::jsonError('Hợp đồng đã đặt chỗ/cọc thành công. Không thể thanh toán!');
                break;
            case BOBill::STATUSES['STATUS_FINALIZED']:
            case BOBill::STATUSES['STATUS_SUCCESS']:
                return self::jsonError('Hợp đồng đã kết thúc. Không thể thanh toán!');
                break;
            case BOBill::STATUSES['STATUS_REQUEST']:
                return self::jsonError('Hợp đồng chưa có hiệu lực. Không thể thanh toán!');
                break;
            default:
                /** @var $paid */
                $paid = 0;
                foreach ($bill->payments as $payment) {
                    $paid += (int) $payment->amount;
                }
                /** @var $unpaid */
                $unpaid = ($bill->bill_total_money? (int) $bill->bill_total_money : 0) - $paid;
                /** @var $payment_methods */
                $payment_methods = PaymentMethod::select(['pm_id AS id', 'pm_title AS title', 'pm_note AS note'])
                    ->where('pm_status', env('STATUS_ACTIVE', 1))->get();
                return self::jsonSuccess($bill, 'Thành công', [
                    'methods' => $payment_methods,
                    'paid' => number_format($paid),
                    'unpaid' => number_format($unpaid),
                    'first_payment' => $paid==0
                ]);
        }
    }

}
