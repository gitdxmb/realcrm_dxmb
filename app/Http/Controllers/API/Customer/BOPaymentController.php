<?php

namespace App\Http\Controllers\API\Customer;

use App\BOBill;
use App\BOPayment;
use App\BOUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BOPaymentController extends Controller
{

    public function submitForm(Request $request) {
        $form_data = $request->validate([
            'bill_id' => 'required',
            'amount' => 'required',
            'method_id' => 'required',
        ]);
        /** @var $bill */
        $where = [BOBill::ID_KEY => (int) $form_data['bill_id']];
        if (AuthController::is_sale_staff()) $where['staff_id'] = AuthController::getCurrentUID();
        $bill = BOBill::where($where)->first();
        if (!$bill) return self::jsonError('Không tìm thấy hợp đồng');

        $query = new BOPayment;
        $query->{BOPayment::ID_KEY} = null;
        $query->pb_status = BOPayment::STATUS['PENDING'];
        $query->pb_code = null;
        $query->reference_code = null;
        $query->bill_code = $bill["bill_code"];
        $query->pb_note = $form_data["note"]?? null;
        $query->customer_id = AuthController::getCurrentUID();
        $query->request_time = now();
        $query->pb_request_money = (double) $form_data["amount"];
        $query->method_id =(int) $form_data["method_id"];
        $query->pb_bank_number_id = null;
        $query->pb_images = $form_data["pb_images"]?? null;
        if (!$query->save()) self::jsonError("Tạo yêu cầu không thành công!", $query);
        /** todo: Update bill status */
        $bill->status_id = BOBill::STATUSES['STATUS_PAYING'];
        $bill->save();
        return self::jsonSuccess($query, 'Thêm dữ liệu thành công!');
    }

    /**
     * @param Request $request
     * @param int $bill_id
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getPaymentsByBill(Request $request, $bill_id = 0) {
        if (!$bill_id) return self::jsonError('Không tồn tại hợp đồng!');
        /** @var $bill */
        $bill = BOBill::select([
            BOBill::ID_KEY,
            'bill_code',
            'status_id',
        ])
            ->where('customer_id', AuthController::getCurrentUID())
            ->where(BOBill::ID_KEY, (int) $bill_id)
            ->first();
        if (!$bill) return self::jsonError('Không tìm thấy hợp đồng!', ['item' => $bill_id]);
        /** @var $data */
        $data = BOPayment::select([
            'id', 'pb_id',
            'pb_status AS status',
            'pb_code AS code',
            'bill_code',
            'pb_note AS note',
            'customer_id', 'request_staff_id', 'request_time', 'response_staff_id',
            'pb_request_money AS request_money',
            'pb_response_time AS response_time',
            'pb_response_money AS response_money',
            'method_id', 'pb_bank_number_id AS bank_number',
            'pb_images'
        ])
            ->where(['bill_code' => $bill->bill_code])->with([
                'paymentMethod:pm_id,pm_title',
                'responseStaff' => function($accountant) {
                    $accountant->select([BOUser::ID_KEY, 'ub_title AS name', 'group_ids'])->with('group:gb_id,gb_title');
                },
                'requestStaff' => function($sale) {
                    $sale->select([BOUser::ID_KEY, 'ub_title AS name', 'group_ids'])->with('group:gb_id,gb_title');
                },
            ])
            ->orderBy('request_time', 'DESC')->get();

        $info = ['statuses' => BOPayment::STATUS_TEXTS];
        if ($bill->status_id === BOBill::STATUSES['STATUS_APPROVED_BY_CUSTOMER'] || $bill->status_id === BOBill::STATUSES['STATUS_PAYING']) {
           $info['addPaymentRequest'] = false;
        } else {
            $info['addPaymentRequest'] = false;
        }
        return self::jsonSuccess($data, 'Thành công', $info);
    }
}
