<?php

namespace App\Http\Controllers\API;

use App\BOCategory;
use App\BOProduct;
use App\BORoleGroup;
use App\BOTransaction;
use App\BOUser;
use App\Http\Controllers\NotificationController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pusher\Laravel\Facades\Pusher;
class BOProductController extends Controller
{
    /**
     * BOProductController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $DXMB_MD5 = $request->header("Content-MD5")?? null;
        if ($DXMB_MD5 != APIController::BO_PRIORITY_CODE) {
            $this->middleware("laravel.jwt")->except(['getProductsAjax']);
            $this->middleware("CheckStoredJWT")->except(['getProductsAjax']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request) {
        $options = $request->all();
        $page = $options['page']? (int) $options['page'] : 1;
        $page_size = $options['size']? (int) $options['size'] : env("PAGE_SIZE", 15);

        $offset = ($page-1)*$page_size;
        $data = BOProduct::select("*")->where("pb_status", ">", 0)->skip($offset)->take($page_size)
            ->with("category")->get();
        return self::jsonSuccess($data);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id=0) {
        if (!$id) {
            return self::jsonError("Không tìm thấy ID sản phẩm");
        }
        /** @var $data */
        $data = BOProduct::select([
            "pb_id", "status_id AS status", "pb_title AS title", "pb_code AS code",
            "category_id", "bedroom",
            "pb_required_money AS required_money", "pb_door_direction AS door_direction",
            "pb_balcony_direction AS balcony_direction",
            "pb_price_total AS total_price",
            "pb_price_per_s AS price_per_square",
            "pb_used_s AS use_area",
            "pb_built_up_s AS construction_area",
            "note"
        ])
        ->where([BOProduct::ID_KEY => (int) $id])->with(["category" => function($query) {
            return $query->select([
                'cb_id',
                'parent_id',
                'cb_code AS code',
                'cb_title AS title',
                'cb_level AS level',
                'active_booking',
                'enable_list_price'
            ]);
        }])
        ->first();
        if ($data) {
            /** @var $type */
            $type = 'apartment';
            /** @var $project */
            $project = null;
            if ($data->category && $data->category->parent_id) {
                $project = BOCategory::select([
                    'id', 'cb_code AS code', 'cb_title AS title', 'cb_level AS level', 'apartment_grid'
                ])->where(['cb_level' => 1])->find($data->category->parent_id);
                if ($project && $project->apartment_grid == false) {
                    $type = 'villa';
                }
            }
            $data['project'] = $project;
            if($data->category->enable_list_price != Null && $data->category->enable_list_price > time()){
                $data->total_price = Null;
            }
            
            /** @var $info */
            $info = [];
            $info['lock_permitted'] = $data->status === BOProduct::STATUSES['MBA'] && $data->category && $data->category->active_booking==1;
            /** @var $product_managers */
            $product_managers = self::getProductManagersByApartment($id);
            if (!$product_managers) $product_managers = [];
            /** @var $users */
            $users = BOUser::select(["ub_title AS name", "ub_email AS email", "ub_phone AS phone"])
                    ->where('ub_status', env('STATUS_ACTIVE',1))
                    ->whereIn(BOUser::ID_KEY, $product_managers)
                    ->get();
            $info['product_managers'] = $users;
            $info['display_status'] = BOProduct::STATUS_DISPLAY;
            $info['type'] = $type;

            return self::jsonSuccess($data, 'Thành công', $info);
        } else {
            return self::jsonError('Không tìm thấy sản phẩm', ['item' => $id]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showByCode(Request $request) {
        $code = $request->input('code', null);
        if (!$code) return self::jsonError('Yêu cầu không hợp lệ!');
        $query = BOProduct::where(['pb_status'=>env('STATUS_ACTIVE', 1), 'pb_code' => $code])
                ->with(['category' => function($building) {
                    $building->select([ BOCategory::ID_KEY, 'enable_list_price', 'active_booking' ]);
                }])->first();
        if (!$query) {
            return self::jsonError('Không tìm thấy dữ liệu!', compact('code'));
        } else {
            $lock_permitted = $query->status_id === BOProduct::STATUSES['MBA'] && $query->category;
            return self::jsonSuccess($query, 'Thành công', compact('lock_permitted'));
        }
    }

    /***
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitForm(Request $request, $id = 0) {
        if ($id>0) {
            $query = BOProduct::where([BOProduct::ID_KEY => $id])->first();
            if (!$query) {
                return self::jsonError('Không tìm thấy sản phẩm', ['item' => $id]);
            }
        } else {
            $query = new BOProduct;
        }
        $form_data = $request->all();
        $query->{BOProduct::ID_KEY} = $id>0? $id : null;
        $query->pb_status = $form_data["status"]? (int)$form_data["status"] : 0;
        $query->pb_title = $form_data["title"]?? null;
        $query->product_id = $form_data["product"]? (int)$form_data["product"] : 0;
        $query->pb_code = $form_data["code"]?? null;
        $query->pb_required_money = $form_data["required_money"]? (double)$form_data["required_money"] : null;
        $query->viewed_group_ids = $form_data["view_groups"]?? null;
        $query->booked_group_ids = $form_data["book_groups"]?? null;
        $query->approved_group_ids = $form_data["approve_groups"]?? null;
        $query->category_id = $form_data["category"]? (int) $form_data["category"] : null;
        $query->pd_reference_codes = $form_data["preference_code"]??null;
        $query->pb_door_direction = $form_data["door_direction"]??null;
        $query->pb_balcony_direction = $form_data["balcony_direction"]??null;
        $query->pb_price_total = $form_data["total"]? (double) $form_data["total"] : null;
        $query->pb_price_per_s = $form_data["price_per_square"]? (double) $form_data["price_per_square"] : null;
        $query->pb_used_s = $form_data["used_area"]? (int) $form_data["used_area"] : null;
        $query->pb_built_up_s = $form_data["construction_area"]? (int) $form_data["construction_area"] : null;
        $query->permission_group_ids = $form_data["permission_groups"]?? null;
        $query->permission_staff_ids = $form_data["permission_staffs"]?? null;

        return $query->save()? self::jsonSuccess($query, $id? 'Cập nhật thành công!' : 'Thêm dữ liệu thành công!') : self::jsonError("Lưu không thành công!", $query);
    }

    /**
     * @param int $id
     * @param bool $manually
     * @param bool $notifiable
     * @param null|int $staff
     * @return bool | BOProduct
     */
    public static function lockApartment($id = 0, $manually = false, $notifiable = false, $staff=null) {
        if (!$id) return false;
        $method = $manually? 'TKKD Duyệt lock' : 'NVKD Lock';
        /** @var $product */
        $product = BOProduct::where(BOProduct::ID_KEY, $id)
                    ->with(['category' => function($building) {
                        $building->select([BOCategory::ID_KEY, 'type'])->where('active_booking', 1);
                    }])->first();
        if (!$product || !$product->category) return false;
        if ($product->status_id !== BOProduct::STATUSES['MBA'] && $product->status_id !== BOProduct::STATUSES['CHLOCK']) return false;
        if ($product->stage==null) {
            LogController::logProduct('[lockApartment] Không thể lock do không rõ giai đoạn căn hộ!', 'error', $id);
            return false;
        }
        /** @var todo: Update product status_id */
        $product->status_id = ($manually||$product->category->type==BOCategory::TYPE_PRIVATE)? BOProduct::STATUSES['LOCKED'] : BOProduct::STATUSES['CHLOCK'];
        if (!$product->save()) {
            LogController::logProduct('Lock căn hộ không thành công! Phương thức: '.$method, 'error', $id);
            return false;
        } else {
            LogController::logProduct('Lock căn hộ thành công! Phương thức: '.$method, 'info', $id);
        }

        /** todo: Pusher on apartment status changed */
        self::pusherToApartment($id, $product->status_id);

        /** todo: Notify apartment locked */
        if ($notifiable && $product->status_id==BOProduct::STATUSES['LOCKED']) {
            NotificationController::notifyApartmentLocked($product, $staff);
        }
        return $product;
    }

    /**
     * @param int $id
     * @param bool $notifiable
     * @param array $targets
     * @return bool
     */
    public static function releaseApartment($id=0, $notifiable = true, $targets = []) {
        if (!$id) return false;
        /** @var $product */
        $product = BOProduct::where(BOProduct::ID_KEY, $id)->first();
        if (!$product) return false;
        $product->status_id = BOProduct::STATUSES['MBA'];
        if (!$product->save()) return false;

        /** todo: Pusher on apartment status changed */
        self::pusherToApartment($id, BOProduct::STATUSES['MBA']);

        /** todo: Notify apartment unlocked */
        if ($notifiable) {
            $targets = array_wrap($targets);
            $product_managers = self::getProductManagersByApartment($id)?? [];
            NotificationController::notifyApartmentUnlocked($product, array_merge($targets, $product_managers));
        }
        return $product;
    }

    /***
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getApartmentsByBuilding(Request $request) {
        $building = $request->input('building', null);
        if (!$building || (int)$building<=0) return self::jsonError('Yêu cầu không hợp lệ!');

        //** Check building existence */
        $building_query = BOCategory::where([BOCategory::ID_KEY => $building, 'cb_level' => 2])
            ->with('sibling:id,apartment_grid')
            ->first();
        if (!$building_query) return self::jsonError('Toà nhà không tồn tại!');

        //** Query all apartments of the building */
        $apartments = BOProduct::select(['pb_id', 'status_id AS status', 'pb_title AS title', 'pb_code', 'pb_price_total AS price'])
        ->where(['category_id' => (int) $building, 'pb_status' => env('STATUS_ACTIVE', 1)])->get()->keyBy('pb_code');

        if (!$apartments || count($apartments)==0) return self::jsonError('Không tìm thấy căn hộ!', ['building' => $building]);
        /** Check Grid type */
        $grid = $building_query->sibling&&$building_query->sibling->apartment_grid==false? 'villa' : 'apartment';
        /** @var $labels */
        $labels = [];
        $floors = [];
        $prefix = null;
        foreach ($apartments as &$apartment){
            $prefix = $prefix?? substr($apartment->pb_code, 0, 6);
            $_label = (string) substr($apartment->pb_code, -3);
            $_label = (substr($_label, 0, 1)=='0')? substr($_label, -2) : $_label;
            $_floor = (string) substr($apartment->pb_code, -6, 3);
            $_floor = (substr($_floor, 0, 1)=='0')? substr($_floor, -2) : $_floor;
            $labels[] = $_label;
            $floors[] = $_floor;
            $apartment->title = $_floor . $_label;
            if(!$building_query->enable_list_price || $building_query->enable_list_price <= time()){
                $apartment->price = $apartment->price? substr($apartment->price, 0, -6) : null;
            }else{
                $apartment->price =  null;
            }
        }
        $labels = array_unique($labels);
        $floors = array_unique($floors);
        sort($labels);
        sort($floors);
        return self::jsonSuccess(
            ['labels' => $labels, 'floors' => $floors, 'prefix' => $prefix, 'apartments' => $apartments ],
            'Thành công',
            ['statuses' => BOProduct::STATUS_LIST, 'statusDisplay' => BOProduct::STATUS_DISPLAY, 'grid' => $grid]
        );
    }

    /**
     * @param $id
     * @param $new_status
     * @param bool $pusher
     * @param bool $is_customer
     * @param bool $api_gate
     * @return bool
     */
    public static function changeStatus($id, $new_status, $pusher = true, $is_customer = false, $api_gate = true) {
        $apartment_update = BOProduct::where([BOProduct::ID_KEY =>$id])->update(['status_id' => (int) $new_status]);
        if(!$apartment_update && !$is_customer){
            $log = ['new_status' => $new_status, 'is_customer' => $is_customer];
            LogController::logProduct('Cập nhật trạng thái căn hộ không thành công! Debug: '.json_encode($log), 'error', $id ,true, $api_gate);
            return false;
        }
        if ($pusher) {
            self::pusherToApartment($id, $new_status);
        }
        return $apartment_update;
    }

    /**
     * @param int $product
     * @param array $fields
     * @return bool
     */
    public static function findProjectByApartment($product = 0, $fields = ['*']) {
        if (!$product) return false;
        /** @var $query */
        $query = BOProduct::select([BOProduct::ID_KEY, 'category_id'])->where(BOProduct::ID_KEY, $product)->first();
        if (!$query) return false;

        /** @var $category */
        $category = $query->category_id;
        $building = BOCategory::select([BOCategory::ID_KEY, 'parent_id'])->where([BOCategory::ID_KEY => $category, 'cb_level' => 2])->first();
        if (!$building) return false;

        /** @var  $project */
        $project = BOCategory::select($fields)->find($building->parent_id);
        if (!$project) return false;

        return $project;
    }

    /**
     * @param int $product
     * @return array | bool
     */
    public static function getProductManagersByApartment($product = 0) {
        /** @var $project */
        $project = self::findProjectByApartment($product, [BOCategory::ID_KEY, 'cb_title']);
        if (!$project) return false;
        /** @var $project_id */
        $project_id = $project->{BOCategory::ID_KEY};
        /** @var $managers */
        $managers = BOUserController::getManagerIDsByProject($project_id);
        return $managers;
    }

    /**
     * @param string $role
     * @param bool $api_gate
     * @return array
     */
    public static function getProjectsByUserRole($role='view', $api_gate=true) {
        $role_groups = AuthController::getObjectsByUserRole('project', $role, $api_gate);
        return $role_groups;
    }
    /**
     * @param string $role
     * @return array
     */
    public static function getProjectsByUserRoleWeb($role='view') {
        $role_groups = AuthController::getObjectsByUserRole('project', $role, false);
        return $role_groups;
    }

    /**
     * @param array|string $projects
     * @return array
     */
    public static function getApartmentsByProject($projects) {
        /** @var $buildings */
        $buildings = BOCategory::select(['id', BOCategory::ID_KEY, "parent_id", "cb_title"])
            ->where('cb_level', 2)->whereHas('sibling', function ($building) use ($projects) {
                if (is_array($projects)) {
                    $building->whereIn(BOCategory::ID_KEY, $projects);
                } else {
                    $building->where(BOCategory::ID_KEY, $projects);
                }
            })->get()->keyBy(BOCategory::ID_KEY);
        $buildings = $buildings? $buildings->toArray() : [];
        /** @var $categories */
        $categories = array_keys($buildings);
        /** @var $apartments */
        $apartments = BOProduct::select(['id', BOProduct::ID_KEY])->whereIn('category_id', $categories)->get()->keyBy(BOProduct::ID_KEY);
        $apartments = $apartments? $apartments->toArray() : [];
        return $apartments? array_keys($apartments) : [];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getProductsAjax(Request $request) {
        $page = $request->input('page', 1);
        $size = $request->input('size', 15);
        $keyword = $request->input('keyword', null);
        /** @var $offset */
        $offset = ($page - 1)*$size;
        /** @var $data */
        $data = BOProduct::select([BOProduct::ID_KEY . ' AS id', 'pb_code AS text']);
        if ($keyword) $data = $data->where('pb_code', 'LIKE', "%$keyword%");
        $data = $data->skip($offset)
                ->take($size)
                ->get();
        if ($data) {
            return self::jsonSuccess($data);
        } else {
            return self::jsonError('Không thể truy vấn căn hộ!');
        }
    }
}
