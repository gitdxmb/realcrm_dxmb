<?php

namespace App\Http\Controllers\API;

use App\BOUser;
use App\BOUserGroup;
use App\EventCheckin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventCheckinController extends Controller
{
    /**
     * EventCheckinController constructor.
     */
    public function __construct() {
        $this->middleware("laravel.jwt");
        $this->middleware("CheckStoredJWT");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function checkIn(Request $request) {
        $event = $request->input('event', null);
        $title = $request->input('title', null);
        if (!$event) {
            return self::jsonError('Sự kiện không tồn tại!');
        }
        $checkin = EventCheckin::firstOrCreate([
            'event_id' => (int) $event,
            'user_id'  => AuthController::getCurrentUID()
        ], [
            'check_in' => now(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        if (!$checkin) return self::jsonError('Xảy ra lỗi khi check-in sự kiện!');
        return self::jsonSuccess($event, 'Check-in sự kiện '.($title??'').' thành công!');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getParticipants(Request $request, $id = 0) {
        /** @var $participants */
        $participants = EventCheckin::where('event_id', 1)->get()->pluck('user_id');
        if (!$participants) return self::jsonError('Chưa có người điểm danh sự kiện này!');
        /** @var $users */
        $users = BOUser::select([
            BOUser::ID_KEY,
            'ub_title AS name',
            'group_ids',
            'ub_tvc_code AS code',
            'ub_avatar',
            'ub_account_tvc AS account'
        ])
            ->whereIn(BOUser::ID_KEY, $participants)
            ->with('group:gb_title,'.BOUserGroup::ID_KEY)
            ->get()->keyBy('code');
        if (!$users) return self::jsonError('Không tải được danh sách nhân viên!');
        $codes = $users->pluck('code');
        return self::jsonSuccess($users, 'Done', compact('codes'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendCongrats(Request $request, $id = 0) {
        /** @var $winner */
        $winner = $request->input('winner', null);
        /** @var $prize */
        $prize = $request->input('prize', 0);
        if (!$winner || !$prize) return self::jsonError('Gửi thông báo không thành công!', $request->all());
        /** @var $user */
        $user = BOUser::where(BOUser::ID_KEY, (int) $winner)->first();
        if (!$user) return self::jsonError('Người dùng không tồn tại!', $request->all());
        /** @var $fcm */
        $fcm = [
            'token' => $user->ub_token
        ];
        switch ((int) $prize) {
            case 1:
                $fcm['title'] = 'Chúc mừng bạn đã nhận được Giải Nhất';
                break;
            case 2:
                $fcm['title'] = 'Chúc mừng bạn đã nhận được Giải Nhì';
                break;
            case 3:
                $fcm['title'] = 'Chúc mừng bạn đã nhận được Giải Ba';
                break;
            default:
                $fcm['title'] = 'Chúc mừng bạn đã nhận được Lì Xì';
        }
        $fcm['body'] = 'Hãy giữ lại thông báo này để Đổi thưởng';
        /** @var $send */
        $send = APIController::pushFCM(array_wrap($fcm['token']), $fcm['title'], $fcm['body'], [], 'success_1');
        return self::jsonSuccess($fcm, 'Gửi thông báo thành công!', $send);
    }
}
