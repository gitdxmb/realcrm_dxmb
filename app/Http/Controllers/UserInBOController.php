<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;
use App\BOUser;
use App\BOUserGroup;
use Illuminate\Support\Facades\Auth;
class UserInBOController extends Controller
{
    private $o_User;
    public function __construct() {
        $this->o_User = new BOUser();
    }
    
    public function ImportUser(){
        
        $a_Res = array();
        if (Input::hasFile('excel')) {
            
            $filename = Input::file('excel')->getClientOriginalName();
            $extension = Input::file('excel')->getClientOriginalExtension();
            
            if($extension == 'xlsx' || $extension == 'xls'){
                Input::file('excel')->move('uploads/', $filename);
                $sz_FileDir = 'uploads'."/".$filename;
                $a_Res = $this->o_User->ImportExcel($sz_FileDir);
                $strRes = "";
                foreach ($a_Res as $key => $val){
                    $strRes .=" ".$val;
                }
            }else{
                $strRes = "Cần nhập đúng định dạng file (xls, xlsx)!!!!";
            }

            return view('user.import',['a_Res'=>$strRes]);

        }else{
            return view('user.import');
        }
        
    }
    
    public function ImportUserGroups(){
        
        $a_Res = array();
        if (Input::hasFile('excel')) {
            
            $filename = Input::file('excel')->getClientOriginalName();
            $extension = Input::file('excel')->getClientOriginalExtension();
            
            if($extension == 'xlsx' || $extension == 'xls'){
                Input::file('excel')->move('uploads/', $filename);
                $sz_FileDir = 'uploads'."/".$filename;
                $a_Res = $this->o_User->ImportUserGroups($sz_FileDir);
                $strRes = "";
                foreach ($a_Res as $key => $val){
                    $strRes .=" ".$val;
                }
            }else{
                $strRes = "Cần nhập đúng định dạng file (xls, xlsx)!!!!";
            }

            return view('user.importUserGroup',['a_Res'=>$strRes]);
        }else{
            return view('user.importUserGroup');
        }
        
    }
    
    public function ImportUserChild(){
        
        $a_Res = array();
        if (Input::hasFile('excel')) {
            
            $filename = Input::file('excel')->getClientOriginalName();
            $extension = Input::file('excel')->getClientOriginalExtension();
            
            if($extension == 'xlsx' || $extension == 'xls'){
                Input::file('excel')->move('uploads/', $filename);
                $sz_FileDir = 'uploads'."/".$filename;
                $a_Res = $this->o_User->ImportUserChild($sz_FileDir);
                $strRes = "";
                foreach ($a_Res as $key => $val){
                    $strRes .=" ".$val;
                }
            }else{
                $strRes = "Cần nhập đúng định dạng file (xls, xlsx)!!!!";
            }

            return view('user.importChid',['a_Res'=>$strRes]);
        }else{
            return view('user.importChid');
        }
        
    }
    
    /*

     * @auth: DIenct
     * @since: 17/09/2018
     * @des: get all user DXMB in BO
     *      */
    
    public function addEditUserInBO(){
        $a_DataView = array();
        $a_DataView['a_Branch'] = BOUserGroup::getAllBranch();
        
        $UserId = (int) Input::get('id', 0);
        $a_DataView['i_id'] = $UserId;
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            $this->o_User->ProcessAddEditUser($UserId);
                return redirect('userBO')->with('status', 'Cập nhật thành công!');
        }
        
        $a_DataView['userBOData'] = $UserId != 0 ? $this->o_User->getUserById($UserId) : array();
        return view('user.edit', $a_DataView );
        
    }
    public function getAllUserInBO(){
        $a_Data = $this->o_User->getAllSearch();
        $a_Branch = BOUserGroup::getAllBranch();
        foreach ($a_Branch as $val){
            $a_branchAry[$val->gb_id] = $val->gb_title;
        }
        $Data_view['a_Branch'] = $a_branchAry;
        $Data_view['a_Users'] = $a_Data['a_data'];
        $Data_view['a_search'] = $a_Data['a_search'];        
        return view('user.index',$Data_view);
        
        
    }
    /**

     * @auth: Dienct
     * @Since: 07/09/2017
     * @Des: register
     */
    
    public function loginUser(){
//        header('Content-type:image/png');
//        $data = [];
//        $curl = curl_init();
//        $url = 'https://erp.dxmb.vn/captcha/getimage';
//        
////        $url = sprintf("%s?%s", $url,count($data) >0 ? http_build_query($data):null);
//                
//        $file = "/tmp/TKKDTEST_API@DXMBT-cookies.txt";
//        curl_setopt($curl, CURLOPT_URL, $url);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//        curl_setopt($curl, CURLOPT_COOKIEJAR, $file);
//        curl_setopt($curl, CURLOPT_COOKIEFILE, $file);
//
//        // EXECUTE:
//        $result = curl_exec($curl);
//        print_r($result);
//        die;
        
        $flagSubmit = Input::get('submit');
        
        if (isset($flagSubmit) && $flagSubmit != "") {
            
            $ary['username'] = Input::get('username','');
            $ary['pwd'] = Input::get('pwd','');
//            $ary['captcha'] = Input::get('captcha','');
            

            
            $loginCheck = $this->o_User->loginUser($ary['username'], $ary['pwd']);            
            if($loginCheck){
                $encryptPass = self::encrypte($ary['pwd'], self::ENCRYPTION_KEY);
                session(['encryptPass' => $encryptPass]);
                return redirect('/customer')->with('error', 'Something went wrong.');
            }else{
                return redirect('/sign-in')->with('error', 'Something went wrong.');
            }
        }
        return view('user.login');
        
    }
    
    /*
     * 
     *      */
    public function getSignature(){
    set_time_limit(5500);
    $config_account = config('cmconst.account_CRM');
    $success = 0;

    foreach ($config_account as $key => $val ){
            if ($key != 'DXMBT') {
                $user1 = BOUser::where('ub_account_tvc', $val['username'])->first();
                $login_info = [
                    "id" => 1,
                    "username" => $val['username'],
                    "password" => $val['pwd'],
                    "appLog" => "Y",
                    "securityKey" => $user1->security_code
                ];
                $bodyLogin = array(
                    'action' => 'ConnectDB',
                    'method' => 'getConfig',
                    'data' => [$login_info],
                    'type' => 'rpc',
                    'tid' => 998,
                );
                // Process Login
                $dataLogin = API\APIController::TVC_POST_DECODE_GET_SIGNATURE($bodyLogin, '', $val['username']);

                $info = [
                    "reportcode" => "apiEMP02",
                    "limit" => 30000,
                    "start" => 0,
                    "queryFilters" => array(
                        array("name" => "employee", "value" => ""),
                        array("name" => "dtb", "value" => "DXMB"),
                    )
                ];
                $body = array(
                    'action' => 'FrmFdReport',
                    'method' => 'qry',
                    'data' => [$info],
                    'type' => 'rpc',
                    'tid' => 1,
                );
                $dataSignature = API\APIController::TVC_POST_DECODE_GET_SIGNATURE($body, '', $val['username']);                
                if (isset($dataSignature['data']) && is_array($dataSignature['data']) && count($dataSignature['data']) > 0) {
                    foreach ($dataSignature['data'] as $user) {
                        $data = [];
                        $curl = curl_init();
                        $url = "https://ees.dxmb.vn/frmfddocument/getFile?id={$user['docnum']}";
                        $file = "/tmp/{$val['username']}-cookies.txt";
                        curl_setopt($curl, CURLOPT_URL, $url);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                        curl_setopt($curl, CURLOPT_COOKIEJAR, $file);
                        curl_setopt($curl, CURLOPT_COOKIEFILE, $file);

                        // EXECUTE:
                        $result = curl_exec($curl);
                        $myfile = fopen("images-signature/" . $user['filename'], "w") or die("Unable to open file!");
                        file_put_contents("images-signature/" . $user['filename'], $result);

                        // udpate signature to USerBO
                        $tvcCode = str_replace('.png', '', $user['filename']);
                        DB::table('b_o_users')->where('ub_tvc_code', $tvcCode)->update(['signature' => 'images-signature/' . $user['filename']]);
                        $success ++;
                    }
                }
            }
        }
        echo "Thành công {$success} chữ ký";
    }

    public function logoutUser(){
        setcookie('current_user', null);
        Auth::guard('loginfrontend')->logout();
        return redirect('/sign-in');
    }
    
    /*

     * @Auth: Dienct
     * @des: check customer reference code TVC
     * @since: 11/10/2018
     *      */
    public function LoginTVC($ary_Login) {
        $info = [
            "id"=> 1,
            "username"=> $ary_Login['username'],
            "password"=> $ary_Login['pwd'],
            "appLog"=> "Y",
            "securitycode"=> $ary_Login['captcha'],
        ];
        $body = array(
            'action' => 'ConnectDB',
            'method' => 'getConfig',
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 31,
        );
        $DataLgin =  API\APIController::TVC_POST($body);
//        $a_Cus = isset(json_decode($DataCheck->original['data'])->result->data)? json_decode($DataCheck->original['data'])->result->data : array();
        return $DataLgin;
        
    }
    
    
}
