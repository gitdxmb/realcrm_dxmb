<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $timestamps = true;
    protected $casts = [
        'created_at' => 'datetime:H:i d/m/Y',
        'updated_at' => 'datetime:H:i d/m/Y',
    ];

    const TYPE_IMAGE = 0;
    const TYPE_VIDEO = 1;
    const TYPE_QUOTE = 2;

    public function author() {
        return $this->belongsTo(BOUser::class, 'created_by', BOUser::ID_KEY);
    }

    public function updated_by() {
        return $this->belongsTo(BOUser::class, 'updated_by', BOUser::ID_KEY);
    }

    public function creator_department() {
        return $this->belongsTo(BOUserGroup::class, 'creator_department', BOUserGroup::ID_KEY);
    }

    public function post_target() {
        return $this->hasOne(PostTarget::class);
    }

    public function post_feedbacks() {
        return $this->hasMany(PostFeedback::class);
    }

    public function getCreatedAtAttribute($value) {
        return $value? Util::timeAgo($value) : $value;
    }
    /**
     * @param $value
     * @return mixed
     */
    public function getBoCategoryIdsAttribute($value) {
        return $value? json_decode($value)->value : $value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getFeedbackOptionsAttribute($value) {
        return $value? json_decode($value)->value : $value;
    }

    /**
     * @param $value
     * @return string
     */
    public function getImageAttribute($value) {
        if ($value) {
            return starts_with($value, 'http')? $value : asset($value);
        }
        return $value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getPicturesAttribute($value) {
        if ($this->attributes['type']!=self::TYPE_IMAGE) return null;
        if ($value) {
            $images = json_decode($value)->value;
            foreach ($images as &$image) {
                $image = asset($image);
            }
            return $images;
        }
        return $value;
    }

    public function getVideoAttribute($value) {
        return $this->attributes['type']!=self::TYPE_VIDEO? null : $value;
    }

    /**
     * @param $value
     * @return string|string[]|null
     */
    public function getContentAttribute($value) {
        $headline = '';
        if ($this->attributes['description']) {
            $headline = '<em><strong>'. $this->attributes['description'] . '</strong></em>';
        }
        if ($value) {
            $value = preg_replace('/(<table)(.)+(<\/table>)/', '', $value);
            $value = preg_replace('/text-align[:\\? ?\'?"?]+start["?;?]/', '', $value);
            $value = preg_replace('/(font-family|line-height|font-variant):[\w\s?\'?,?\-?]+;/', '', $value);
            $value = preg_replace('/(height|width)[=\?\'?"?0-9?]+/', '', $value);
            return htmlspecialchars($headline . $value);
        }
        return $value;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed|void
     */
    public function setAttribute($key, $value) {
        switch ($key) {
            case 'bo_category_ids':
                if (is_array($value)) {
                    if (count($value)>0) {
                        foreach ($value as &$item) {
                            $item = (int) $item;
                        }
                        $this->attributes[$key] = json_encode(['value' => $value]);
                    } else {
                        $this->attributes[$key] = null;
                    }
                } else {
                    $this->attributes[$key] = $value;
                }
                break;
            case 'pictures':
            case 'feedback_options':
                if (is_array($value)) {
                    if (count($value)>0) {
                        $this->attributes[$key] = json_encode(['value' => array_values($value)]);
                    } else {
                        $this->attributes[$key] = null;
                    }
                } else {
                    $this->attributes[$key] = $value;
                }
                break;
            case 'video':
                if ($value && str_contains($value, 'youtube.com/watch?v=')) {
                    $value = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","https://www.youtube.com/embed/$1", $value);
                }
                $this->attributes[$key] = $value;
                break;
            default:
                $this->attributes[$key] = $value;
        }
    }

    /**
     * @param int|array $category_ids
     * @param array $where
     * @return array
     */
    public static function getPostsByProjects($category_ids = 0, $where=[]) {
        if (!$category_ids) return [];
        $where['status'] = $where['status']?? env('STATUS_ACTIVE', 1);
        return self::where($where)->whereJsonContains("bo_category_ids->value", $category_ids)->get();
    }
}
