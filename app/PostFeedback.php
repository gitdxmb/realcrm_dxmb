<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostFeedback extends Model
{
    public $timestamps = true;

    public function created_by() {
        return $this->belongsTo(BOUser::class, 'created_by', BOUser::ID_KEY);
    }

    public function getCreatedAtAttribute($value) {
        return $value? Util::timeAgo($value) : $value;
    }
}
