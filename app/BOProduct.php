<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class BOProduct extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'pb_id', 'pb_status', 'pb_title', 'product_id',
        'status_id',
        'pb_code',
        'pb_code_real',
        'pb_required_money',
        'viewed_group_ids',
        'booked_group_ids',
        'approved_group_ids',
        'category_id',
        'pd_reference_codes',
        'pb_door_direction',
        'pb_balcony_direction',
        'extra_ids',
        'pb_price_total',
        'pb_price_per_s',
        'pb_used_s',
        'pb_built_up_s',
        'price_used_s',
        'bedroom',
        'view',
        'corner',
        'price_maintenance',
        'price_min',
        'price_max',
        'payment_methods',
        'note',
        'permission_group_ids',
        'permission_staff_ids',
        'last_sync_tvc',
        'number_lock',
        'stage',
        'company',
        'book_type'
    ];

    const ID_KEY = "pb_id";
    //** Trường stage */
    const STAGE_DEPOSIT = 1; //Đặt cọc
    const STAGE_BOOK_TO_DEPOSIT = 2; // Đặt chỗ, có thể lên cọc
    const STAGE_BOOK_ONLY = 3; // Đặt chỗ, phải Huỷ chỗ để sang Cọc mới.
    //** Trường Book_type */
    const BOOK_WITH_PENALTY = 1; // Đặt chỗ thưởng phạt;
    const BOOK_WITHOUT_PENALTY = 2; // Đặt chỗ KO thưởng phạt
    /** @var array */
    const STATUS_DISPLAY = [
        '-4' => [
            'text'     => 'Đã Lock',
            'icon-web' => null,
            'icon-app' => null,
            'background-color'    => 'green',
            'color' => '#fff'
        ],
        '-1' => [
            'text' => 'Trống',
            'icon-web' => null,
            'icon-app' => null,
            'background-color'    => 'white',
            'color' => '#4c4c4c'
        ],
        '0' => [
            'text'     => 'Trống',
            'icon-web' => null,
            'icon-app' => null,
            'background-color'   => 'white',
            'color' => '#4c4c4c'
        ],
        '1' => [
            'text'     => 'Đã Lock',
            'icon-web' => 'fa fa-clock',
            'icon-app' => 'time',
            'background-color'    => '#FAEE5F',
            'color' => '#4c4c4c'
        ],
        '2' => [
            'text'     => 'Đã Lock',
            'icon-web' => 'fa fa-pencil',
            'icon-app' => 'create',
            'background-color'    => '#FAEE5F',
            'color' => '#4c4c4c'
        ],
        '3' => [
            'text'     => 'Đã Lock',
            'icon-web' => 'fa fa-user',
            'icon-app' => 'person',
            'background-color'    => '#FAEE5F',
            'color' => '#4c4c4c'
        ],
        '4' => [
            'text'     => 'Đã Lock',
            'icon-web' => 'fa fa-usd',
            'icon-app' => 'logo-usd',
            'background-color'    => '#FAEE5F',
            'color' => '#4c4c4c'
        ],
        '5' => [
            'text'     => 'Đặt Cọc',
            'icon-web' => null,
            'icon-app' => null,
            'background-color'    => '#0000FF',
            'color' => '#fdfcfc'
        ],
        '6' => [
            'text'     => 'HĐMB',
            'icon-web' => null,
            'icon-app' => null,
            'background-color'    => '#FF0000',
            'color' => '#fdfcfc'
        ],
        '7' => [
            'text'     => 'Thu hồi',
            'icon-web' => null,
            'icon-app' => null,
            'background-color'    => '#4d1217',
            'color' => '#fdfcfc'
        ],
        '-2' => [
            'text'     => 'Khác',
            'icon-web' => null,
            'icon-app' => null,
            'background-color'    => '#ddd',
            'color' => '#ddd'
        ],
    ];

    /** @var array - Status list for NOTEs */
    const STATUS_LIST = [
        [
            'color' => 'white',
            'text'  => 'Trống'
        ],
        [
            'color' => '#FAEE5F',
            'text'  => 'Đã Lock'
        ],
        [
            'color' => '#0000FF',
            'text'  => 'Đặt Cọc'
        ],
        [
            'color' => '#FF0000',
            'text'  => 'HĐMB'
        ],
        [
            'color' => '#4d1217',
            'text'  => 'Thu hồi'
        ],
    ];
    // Status IDs available
    const STATUSES = [
        'HUY' => 7, ///CĐT thu hồi
//        'CBA' => 0, // Important: cập nhật vào cột pb_status = 0!
        'MBA' => 0, // Đang mở bán
        'CHLOCK' => -1, /// Đang chờ duyệt Lock
        'LOCKED' => 1, /// Đã lock thành công
        'ADD_CUSTOMER' => 2, /// ĐÃ THÊM THÔNG TIN KHÁCH HÀNG
        'CUSTOMER_CONFIRM' => 3, /// Khách hàng xác nhận yêu cầu
        'DCH' => -4, // Đặt chỗ, chờ chuyển cọc
        'CDDCO' => 4, // Chờ duyệt Đặt cọc
        'DCO' => 5,// Đặt cọc
        'HDO' => 6, // Hợp đồng mua bán,
        'KHAC' => -2 /** CDHDC - CDHDO */
    ];

    protected $casts = [
        'viewed_group_ids' => 'array',
        'booked_group_ids' => 'array',
        'approved_group_ids' => 'array',
        'extra_ids' => 'array',
        'permission_group_ids' => 'array',
        'permission_staff_ids' => 'array',
        'ub_updated_time' => 'datetime:d/m/Y H:i',
    ];

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed|void
     */
    public function setAttribute($key, $value)
    {
        switch ($key) {
            case "viewed_group_ids":
            case "booked_group_ids":
            case "approved_group_ids":
            case "extra_ids":
            case "permission_group_ids":
            case "permission_staff_ids":
                $this->attributes[$key] = is_array($value)? json_encode($value) : null;
                break;
            case "pb_id":
//                $this->attributes[$key] = $value?? strtotime("now");
                usleep(10);
                $this->attributes[$key] = Util::microtime_float();
                break;
            default:
                $this->attributes[$key] = $value;
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getRequiredMoneyAttribute($value) {
        return $value? number_format($value) : $value;
    }

    /**
     * @param $value
     * @return string
     */
    public function getTotalPriceAttribute($value) {
        $temp_append = isset($this->attributes['note'])? " ({$this->attributes['note']})" : '';
        return $value? number_format($value) . $temp_append : $value;
    }

    /** Round integer price
     * @param $value
     * @return int
     */
    public function getPriceAttribute($value) {
        return $value? (int) $value : $value;
    }

    /**
     * @param $value
     * @return string
     */
    public function getPricePerSquareAttribute($value) {
        return $value? number_format($value) : $value;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category() {
        return $this->belongsTo(BOCategory::class, "category_id", BOCategory::ID_KEY);
    }

    /**
     * @param $value
     * @return string
     */
    public function getPbTitleAttribute($value) {
        return $value&&$value!=''? $value : Util::convertApartmentCode($this->pb_code);
    }
    public function getTitleAttribute($value) {
        return $value&&$value!=''? $value : Util::convertApartmentCode($this->code);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updated_by() {
        return $this->belongsTo(BOUser::class, "updated_user_id", BOUser::ID_KEY);
    }

    /**
     * @param array $in
     * @param string $key_by
     * @return mixed
     */
    public static function getAllBasic($in=[], $key_by = '') {
        $data = self::select([
            "id",
            self::ID_KEY,
            "pb_code AS code",
            "category_id"
        ])
            ->where('pb_status', '!=', env('STATUS_DELETE', -1));
        if (count($in) > 0) $data = $data->whereIn(self::ID_KEY, $in);
        $data = $data->with(['category' => function($building) {
                $building->select([
                    'id', BOCategory::ID_KEY, 'parent_id', 'cb_code AS code', 'cb_title AS title'
                ])->with([
                    'sibling' => function($project) {
                        $project->select([
                            'id', BOCategory::ID_KEY, 'parent_id', 'cb_code AS code', 'cb_title AS title'
                        ]);
                    }
                ]);
            }])
            ->get();
        if ($key_by) $data = $data->keyBy($key_by);
        return $data;
    }

    /**

     * @Auth: HuyNN
     * @Des: Get apartment by Building
     * @Since: 18/09/2018
     */
    public function getAllApartment($bid) {
        $a_DataApartment = DB::table('b_o_products')->where('category_id',$bid)->where('pb_status','!=',-1)->get();
        return $a_DataApartment;
    }

}
