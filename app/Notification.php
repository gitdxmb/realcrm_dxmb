<?php

namespace App;

use App\Notifications\ApartmentLocked;
use App\Notifications\ApartmentLockRequest;
use App\Notifications\ApartmentUnlocked;
use App\Notifications\BillCreated;
use App\Notifications\BillStatusUpdated;
use App\Notifications\PaymentCreated;
use App\Notifications\PostCreated;
use App\Notifications\TransactionCustomerAdded;
use Illuminate\Notifications\DatabaseNotification;

class Notification extends DatabaseNotification
{
    /** @var array */
    const OBJECTS = [
        /** Product Notification Group */
        [
            ApartmentUnlocked::class,
            ApartmentLocked::class
        ],
        [
            PostCreated::class
        ],
        ['Forum'],
        /** Transaction Notification Group */
        [
            TransactionCustomerAdded::class,
            ApartmentLockRequest::class
        ],
        /** Bill Notification Group */
        [
            BillCreated::class,
            BillStatusUpdated::class,
            \App\Notifications\Customer\BillStatusUpdated::class,
            PaymentCreated::class
        ]
    ];

    const SOUNDS = [
        'dxmb',
        'lock_1',
        'lock_2',
        'lock_3',
        'unlock_1',
        'unlock_2',
        'unlock_3',
        'success_1',
        'success_2',
        'fail_1',
    ];

    protected $appends = ['image', 'event'];

    /**
     * @param $value
     * @return null|string
     */
    public function getCreatedAtAttribute($value) {
        return $value? Util::timeAgo($value) : null;
    }

    /**
     * @param $value
     * @return array|null
     */
    public function getDataAttribute($value) {
        return $value? self::getData(json_decode($value, true), $this->notifiable_type) : null;
    }

    /**
     * @return string
     */
    public function getImageAttribute() {
        switch ($this->type) {
            case ApartmentLockRequest::class:
                return asset('images/lock-request.png');
            case ApartmentUnlocked::class:
                return asset('images/unlock.png');
            case ApartmentLocked::class:
                return asset('images/lock.png');
            case TransactionCustomerAdded::class:
                return asset('images/new-customer.png');
            case BillCreated::class:
                return asset('images/new-bill.png');
            case \App\Notifications\Customer\BillStatusUpdated::class:
            case BillStatusUpdated::class:
                return asset('images/bill.png');
            case PaymentCreated::class:
                return asset('images/payment.png');
            case PostCreated::class:
                return $this->data&&isset($this->data['image'])? asset($this->data['image']) : asset('images/dxmb.png');
            default:
                return asset('images/dxmb.png');
        }
    }

    /**
     * @return string
     */
    public function getEventAttribute() {
        return $this->type? self::getMessage($this->type) : '';
    }

    /**
     * @param string $class
     * @return string
     */
    private static function getMessage($class = '') {
        switch ($class) {
            case ApartmentUnlocked::class:
                return 'Bung lock';
            case ApartmentLocked::class:
                return 'Lock thành công bởi NVKD';
            case TransactionCustomerAdded::class:
                return 'Bổ sung khách hàng';
            case BillCreated::class:
                return 'Khởi tạo thành công';
            case \App\Notifications\Customer\BillStatusUpdated::class:
            case BillStatusUpdated::class:
                return 'Chuyển sang tình trạng';
            case PaymentCreated::class:
                return 'YC thanh toán mới từ NVKD';
            case ApartmentLockRequest::class:
                return 'Yêu cầu lock căn';
            case PostCreated::class:
            default:
                return '';
        }
    }

    /** Manipulate notification data for List Display
     * @param array $data
     * @param string $notifiable_type
     * @return array
     */
    private static function getData($data, $notifiable_type = BOUser::class) {
        $item = null; $user = null; $value = null;
        if ($notifiable_type == BOCustomer::class) {
            if (isset($data['bill'])) {
                $value = isset($data['status'])? BOBill::STATUS_TEXTS[$data['status']] : null;
                $bill = BOBill::select([BOBill::ID_KEY, 'bill_code', 'bill_customer_info'])->where(BOBill::ID_KEY, $data['bill'])->first();
                if ($bill) {
                    $item = 'HĐ ' . $bill->bill_code;
                    $user = $data['by']&&$bill->bill_customer_info? $bill->bill_customer_info->name : null;
                }
            }
        } else {
            if (isset($data['by']) && $data['by'] > 0) {
                $user = BOUser::select([BOUser::ID_KEY, 'ub_title'])->where(BOUser::ID_KEY, $data['by'])->first();
                $user = $user? $user->ub_title : null;
            }
            if (isset($data['bill'])) {
                $value = isset($data['status'])? BOBill::STATUS_TEXTS[$data['status']] : null;
                $bill = BOBill::select([BOBill::ID_KEY, 'bill_code', 'bill_customer_info'])->where(BOBill::ID_KEY, $data['bill'])->first();
                if ($bill) {
                    $item = 'HĐ ' . $bill->bill_code;
                }
            } elseif (isset($data['transaction'])) {
                $value = isset($data['status']) ? BOTransaction::STATUSES[$data['status']] : null;
                $item = 'GD #' . $data['transaction'];
            } elseif (isset($data['payment'])) {
                $value = $user;
                $payment = BOPayment::select(['bill_code', 'pb_code'])->where(BOPayment::ID_KEY, $data['payment'])->first();
                $item = 'GDTT ' . ($payment? $payment->pb_code : '#'.$data['payment']);
            } elseif (isset($data['apartment'])) {
                $apartment = BOProduct::select('pb_code')->where(BOProduct::ID_KEY, $data['apartment'])->first();
                $item = 'Căn hộ ' . ($apartment? $apartment->pb_code : '#' . $data['apartment']);
            } elseif (isset($data['post'])) {
                $post = Post::find($data['post']);
                $item = $post? $post->title : 'Bài viết đã bị xóa';
                $data['image'] = $post? $post->image : '';
            }
        }
        return array_merge($data, ['item' => $item, 'user' => $user, 'value' => $value]);
    }

    /** Get App Payload for Loading Screen
     * @param string $class
     * @param array $data
     * @return array
     */
    public static function getAppDataByType($class = '', $data = []) {
        switch ($class) {
            case ApartmentUnlocked::class:
            case ApartmentLocked::class:
                return [
                    'screen' => 'ApartmentDetails',
                    'params' => ['id' => $data['apartment'], 'name' => $data['apartment']]
                ];
            case ApartmentLockRequest::class:
            case TransactionCustomerAdded::class:
                return [
                    'screen' => 'TransactionDetails',
                    'params' => ['id' => $data['transaction']]
                ];
            case BillCreated::class:
            case BillStatusUpdated::class:
                return [
                    'screen' => 'ContractDetails',
                    'params' => ['id' => $data['bill']]
                ];
            case \App\Notifications\Customer\BillStatusUpdated::class:
                return [
                    'screen' => 'CustomerBillDetails',
                    'params' => ['id' => $data['bill']]
                ];
            case PaymentCreated::class:
                if ($data['payment'] > 0) {
                    $payment = BOPayment::select([BOPayment::ID_KEY, 'bill_code'])->where(BOPayment::ID_KEY, $data['payment'])
                                ->with(['bill' => function($bill) {
                                    $bill->select([BOBill::ID_KEY, 'bill_code']);
                                }])->first();
                    if ($payment && $payment->bill) return [
                        'screen' => 'ContractDetails',
                        'params' => ['id' => $payment->bill->{BOBill::ID_KEY}, 'tab' => 2]
                    ];
                }
                return compact('payment');
            case PostCreated::class:
                return [
                    'screen' => 'NewsDetails',
                    'params' => ['id' => $data['post']]
                ];
            default:
                return [];
        }
    }
}
