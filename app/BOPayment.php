<?php

namespace App;

use App\Http\Controllers\API\AuthController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Auth;
use Psr\Log\NullLogger;
use App\Http\Controllers\API\APIController;
class BOPayment extends Model
{
    const ID_KEY = "pb_id";
    const STATUS = [
        'REFUSED' => -1,
        'PENDING' => 0,
        'APPROVED' => 1
    ];
    const STATUS_TEXTS = [
        '-1' => 'Từ chối',
        '0' => 'Chờ xác nhận',
        '1' => 'Thành công'
    ];

    const TYPE_PAYMENT_TAVICO = ['DCHO' => 1, 'CANCEL_DCHO' => 2, 'DCO' => 3];

    protected $casts = [
        'request_time' => 'datetime:H:i d-m-Y',
        'response_time' => 'datetime:H:i d-m-Y',
        'pb_response_time' => 'datetime:H-i d-m-Y',
        'pb_images' => 'array'
    ];

    /**
     * @param $value
     * @return mixed|null
     */
    public function getPbImagesAttribute($value) {
        if (!$value) return null;
        $value = json_decode($value);
        foreach ($value as &$item) {
            $item = url($item);
        }
        return $value;
    }

    /**
     * @param $value
     * @return string
     */
    public function getRequestMoneyAttribute($value) {
        return $value? number_format($value) : $value;
    }

    /**
     * @param $value
     * @return string
     */
    public function getResponseMoneyAttribute($value) {
        return $value? number_format($value) : $value;
    }

    /**
     * @param $value
     * @return string
     */
//    public function getPbRequestMoneyAttribute($value)
//    {
//        return $value? number_format($value) : $value;
//    }

    /**
     * @param $value
     * @return string
     */
//    public function getPbResponseMoneyAttribute($value)
//    {
//        return $value? number_format($value) : $value;
//    }

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed|void
     */
    public function setAttribute($key, $value) {
        $id = time();
        switch ($key) {
            case self::ID_KEY:
                $this->attributes[$key] = $value?? $id;
                break;
            case 'pb_code':
                $this->attributes[$key] = $value?? 'OBP'.$id;
                break;
            case 'pb_images':
                $this->attributes[$key] = $value? json_encode($value) : null;
                break;
            default:
                $this->attributes[$key] = $value;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bill() {
        return $this->belongsTo(BOBill::class, 'bill_code', 'bill_code');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer() {
        return $this->belongsTo(BOCustomer::class, 'customer_id', BOCustomer::ID_KEY);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function requestStaff() {
        return $this->belongsTo(BOUser::class, 'request_staff_id', BOUser::ID_KEY);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function responseStaff() {
        return $this->belongsTo(BOUser::class, 'response_staff_id', BOUser::ID_KEY);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function staff() {
        return $this->belongsTo(BOUser::class, 'staff_id', BOUser::ID_KEY);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bankInfo() {
        return $this->belongsTo(BankInfo::class, 'pb_bank_number_id', BankInfo::ID_KEY);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentMethod() {
        return $this->belongsTo(PaymentMethod::class, 'method_id', PaymentMethod::ID_KEY);
    }
    
    /**
     * @Auth: Dienct
     * @Des: 
     * @Since: 2/3/2017
     */
    public function getPaymentById($id) {
        $a_Data = DB::table('b_o_payments')->where('id', $id)->first();
        $bill = BOBill::where('bill_code',$a_Data->bill_code)->first();
        $a_Data->bill = array(
            'id' => $bill->id,
            'bill_code' => $bill->bill_code
        );
        $customerInfo = $bill->bill_customer_info;
        $a_Data->CusName = $customerInfo->name.' - '.$customerInfo->phone;

        if($a_Data->request_staff_id){
            $o_Staff = DB::table('b_o_users')->where('ub_id', $a_Data->request_staff_id)->first();
            $a_Data->StaffName = $o_Staff->ub_title;
        }
        $bill = BOBill::where('bill_code',$a_Data->bill_code)->first();
        $product = BOProduct::where('pb_id',$bill->product_id)->first();
        $a_Data->product = array(
            'id' => $product->id,
            'code' => $product->pb_code
        );

        $cate = BOCategory::where('cb_id',$product->category_id)->first();
        $a_Data->building = array(
            'id' => $cate->id,
            'title' => $cate->cb_title,
            'code' => $cate->cb_code
        );

        $project = BOCategory::find($cate->parent_id);
        $a_Data->project = array(
            'id' => $project->id,
            'title' => $project->cb_title,
            'code' => $project->pb_code,
        );

        $type = $a_Data->type == 1 ? 'đặt cọc' : 'đặt chỗ';
        $a_Data->pb_note2 = $a_Data->pb_note2 == null ?  'Khách hàng '. $a_Data->CusName.' '.$type.' căn '.$product->pb_code :$a_Data->pb_note2;
        $array_images = json_decode($a_Data->pb_images);
        $a_Data->pb_images = $array_images;
        return $a_Data;
    }

    public function getAllSearch() {
        $buildings = BOCategory::where('cb_level',2)->get();
        foreach ($buildings as $building){
            $arrBuilding[$building->cb_id] = $building->cb_code;
        }

        $projects = BOCategory::where('cb_level',1)->get();
        foreach ($projects as $project){
            $arrProject[$project->id] = $project->cb_title;
        }

        $arrBillPaidOnceCode =  array();
        $billPaidOnce = BOBill::where('status_id', BOBill::STATUSES['STATUS_PAID_ONCE'])->get();
        if($billPaidOnce){
            foreach ($billPaidOnce as $bill){
                $arrBillPaidOnceCode[] = $bill->bill_code;
            }
        }

        $a_data = array();
        $o_Db = DB::table('b_o_payments')->select('*')->whereNotIn('bill_code', $arrBillPaidOnceCode);
//        $a_data = $o_Db->where('pb_status', '!=', -1);
        $a_search = array();
        //search

        $pb_code = Input::get('pb_code', '');
        if ($pb_code != '') {
            $a_search['pb_code'] = $pb_code;
            $a_data = $o_Db->where('pb_code', $pb_code);
        }
        $staff_id = Input::get('staff_id', '');
        if ($staff_id != '') {
            $a_search['staff_id'] = $staff_id;
            $a_data = $o_Db->where('request_staff_id', $staff_id);
        }
        
        $paid_status = Input::get('paid_status',2);
        if ($paid_status == 1) {
            $a_search['paid_status'] = $paid_status;
            $a_data = $o_Db->where('pb_status','!=', 0);
        }else if($paid_status == 0) {
            $a_search['paid_status'] = $paid_status;
            $a_data = $o_Db->where('pb_status', 0);
        }
        $a_data = $o_Db->orderBy('updated_at', 'desc')->get();

        if(count((array) $a_data) > 0){
            foreach ($a_data as $key => &$val) {
                $val->stt = $key + 1;
//                $cus = DB::table('user_customer_mappings')->where('id_customer', $val->customer_id)->first();
//                $val->CusName = $cus ? $cus->c_name : '';
                $user = DB::table('b_o_users')->where('ub_id', $val->response_staff_id)->first();
                $val->StaffName  = $user ? $user->ub_title : '';
                $val->CusName = '';
                if($val->bill_code){
                    $bill = BOBill::where('bill_code',$val->bill_code)->first();
                    $customer_info = $bill->bill_customer_info;
                    $val->CusName = $customer_info->name?? '';
                    $product = BOProduct::where('pb_id',$bill->product_id)->first();
                    $val->product  = $product ? $product->pb_code : '';
                    $val->building  = $product && isset($arrBuilding[$product->category_id]) ? $arrBuilding[$product->category_id] : '';
                    if($product){
                        $building = BOCategory::where(BOCategory::ID_KEY,$product->category_id )->first();
                        $val->project  = isset($arrProject[$building->parent_id]) ? $arrProject[$building->parent_id] : '';
                    }else{
                        $val->project = '';
                    }
                    if(file_exists(public_path().'/pdf/hop-dong-'.$bill->bill_id.'.pdf')){
                        $val->pdf = '/pdf/hop-dong-'.$bill->bill_id.'.pdf';
                    }
                }
                $type = $val->type == 1 ? 'đặt cọc' : 'đặt chỗ';
                $val->pb_note2 = $val->pb_note2??
                    ('Khách hàng: '. ($val->CusName?? '(Không rõ)').' - '.$type.' - Căn: '.($product->pb_code??'(Không rõ)') );
            }
        }
        $a_return = array('a_data' => $a_data, 'a_search' => $a_search);
        return $a_return;
    }

    /**

     * @Auth: Dienct
     * @Des: Add/edit project
     * @Since: 02/03/2017
     */
    
    public function ProcessPayMent($id) {        
        $a_DataInsert = array();
        $a_DataUpdate = array();
        $a_DataInsert['bill_code'] = Input::get('bill_code');
        $a_DataInsert['customer_id'] = Input::get('customer_id');
        $a_DataInsert['request_staff_id'] = Auth::guard('loginfrontend')->user()->ub_id; // ke toan thu tien
        $a_DataInsert['response_staff_id'] = Auth::guard('loginfrontend')->user()->ub_id; // ke toan thu tien
        $a_DataInsert['pb_note'] = Input::get('pb_note');
        $a_DataInsert['pb_note2'] = Input::get('pb_note2');
        $a_DataInsert['pb_response_money'] = (int)str_replace(' ','',Input::get('pb_response_money'));
        $a_DataInsert['method_id'] = Input::get('method_id');
        $a_DataInsert['bank_id'] = Input::get('bank_id') ? Input::get('bank_id') : null;
        $a_DataInsert['pb_bank_number_id'] = Input::get('pb_bank_number_id') ? Input::get('pb_bank_number_id') : '';
        $a_DataInsert['pb_bank_number_id'] = Input::get('pb_bank_number_id') ? Input::get('pb_bank_number_id') : '';
        $a_DataInsert['pb_status'] = Input::get('pb_status') == 'on' ? 1 : 0;


        $a_DataUpdate['pb_bank_number_id'] = Input::get('pb_bank_number_id');
        $a_DataUpdate['method_id'] = Input::get('method_id');
        $a_DataUpdate['pb_note'] = Input::get('pb_note');
        $a_DataUpdate['pb_note2'] = Input::get('pb_note2') ? Input::get('pb_note2') : '';
        $a_DataUpdate['bank_id'] = Input::get('bank_id') ? Input::get('bank_id') : null;
        if (is_numeric($id) == true && $id != 0) {
            $a_DataUpdate['pb_response_time'] = date('Y-m-d H:i:s', time());
            DB::table('b_o_payments')->where('id', $id)->update($a_DataUpdate);
        } else {
            $a_DataInsert['pb_id'] = time();
            $a_DataInsert['pb_code'] = 'OBP'. $a_DataInsert['pb_id'];
            $a_DataInsert['request_time'] = date('Y-m-d H:i:s', time());
            $a_DataInsert['pb_response_time'] = date('Y-m-d H:i:s', time());
            $insertId = DB::table('b_o_payments')->insertGetId($a_DataInsert);

            $referenceCode = self::createPaymentTvc($a_DataInsert);
            DB::table('b_o_payments')->where('id', $insertId)->update(array('reference_code' => $referenceCode));
        }
        // check thu du tien hay chua
        $a_allPaymentByBillCode = DB::table('b_o_payments')->where('bill_code', $a_DataInsert['bill_code'])->get();
        $totalResponMoney = 0;
        foreach($a_allPaymentByBillCode as $val){
            $totalResponMoney += (int) $val->pb_response_money;
        }

        if($totalResponMoney >= (int) Input::get('bill_required_money')){
            DB::table('b_o_bills')->where('bill_code', $a_DataInsert['bill_code'])->update(['status_id'=>BOBill::STATUSES['STATUS_PAID']]);
        }else{
            DB::table('b_o_bills')->where('bill_code', $a_DataInsert['bill_code'])->update(['status_id'=>BOBill::STATUSES['STATUS_PAYING']]);
        }
    }


    /**
     * @Auth: HuyNN
     * @Des: load Payment By BillCode
     * @Since: 09/10/2018
     */

    public static function loadPaymentByBillCode($billCode) {
        $a_data = BOPayment::select('*')->where(['bill_code' => $billCode])->with(
            [
                "customer" => function($customers) {
                    return $customers->select(["cb_id","cb_name","cb_email", "cb_phone", "cb_permanent_address"]);
                },
                "paymentMethod" => function($paymentMethod) {
                    return $paymentMethod->select(["pm_id","pm_title","pm_code"]);
                },
                "bankInfo" => function($bankInfo) {
                    return $bankInfo->select(["bank_id","bank_title","bank_code","bank_holder","bank_number"]);
                },
                "requestStaff" => function($requestStaff)  {
                    return $requestStaff->select(["ub_id", "ub_account_name AS account", "ub_title AS name", "group_ids"])
                        ->with(["group" => function($group) {
                            return $group->select(['gb_id', 'gb_title AS name']);
                        }]);
                },
            ]
        )->orderBy('request_time','desc')->get();

        return $a_data;
    }

}
