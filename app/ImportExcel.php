<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Product;
use Illuminate\Support\Facades\Auth;

class ImportExcel extends Model
{
    public function ImportExcelProject($dirFile) {
        set_time_limit(600);
        $a_Error = array();
        //Get all users from db
        

        $o_Result = Excel::selectSheetsByIndex(0)->load($dirFile, function($reader) use ( &$a_Respon) {
            $a_NewUsers = array();
            $k = time();
            foreach ($reader->toArray() as $key => $row) {
                $a_userInsert = array();
                $a_userInsert['parent_id'] = 0;
                $a_userInsert['cb_id'] = (int) $k += 1;
                $a_userInsert['cb_status'] = 1;
                $a_userInsert['cb_code'] = $row['code'];
                $a_userInsert['reference_code'] = $row['code'];
                $a_userInsert['cb_title'] = $row['tavico_name'];
                $a_userInsert['cb_level'] = 1;
                $a_userInsert['ub_created_time'] = date('Y-m-d H:i:s', time());
                $a_userInsert['ub_updated_time'] = date('Y-m-d H:i:s', time());
                $a_NewUsers[] = $a_userInsert;
                unset($a_userInsert);                   
            }
            //end check duplicate in file
            //start process
            if (!isset($a_Respon)) {
                if (isset($a_NewUsers) && Util::b_fCheckArray($a_NewUsers)) {
                    //Get total of new users
                    $i_TotalNewUsers = count($a_NewUsers);
                    //Insert new user into db
                    if (DB::table('b_o_categories')->insert($a_NewUsers)) {
                        $a_Respon['insert'] = "Inserted $i_TotalNewUsers successfully! <br/>";
                    } else {
                        $a_Respon['insert'] = "Insert new users failed! <br/>";
                    }
                } else {
                    $a_Respon['insert'] = "No any new users found! <br/>";
                }
                
            }
            //end process

        });
        return $a_Respon;

    }
    /**

     * @auth: Dienct
     * @since: 12/09/2018
     * @des: process import building
     *      */
    public function ImportExcelBuilding($dirFile) {
        set_time_limit(600);
        $a_Error = array();
        //Get all users from db
        $o_Result = Excel::selectSheetsByIndex(0)->load($dirFile, function($reader) use ( &$a_Respon) {
            $a_NewUsers = array();
            $k = time();
            $o_Project = DB::table('b_o_categories')->where('cb_status', 1)->get();
            foreach ($o_Project as $val){
                if(strlen($val->cb_code) == 3){
                    $a_Project[$val->cb_code] = $val->id;
                }
            }
            foreach ($reader->toArray() as $key => $row) {
                if(strlen($row['block']) == 6){
                    $a_userInsert = array();
                    $keyProject = substr($row['block'], 0, -3);
                    $a_userInsert['parent_id'] = isset($a_Project[$keyProject]) ? $a_Project[$keyProject] : null;
                    $a_userInsert['cb_id'] = (int) $k += 1;
                    $a_userInsert['cb_status'] = 1;
                    $a_userInsert['cb_code'] = $row['block'];
                    $a_userInsert['reference_code'] = $row['block'];
                    $a_userInsert['cb_title'] = $row['tavico_name'];
                    $a_userInsert['cb_level'] = 2;
                    $a_userInsert['ub_created_time'] = date('Y-m-d H:i:s', time());
                    $a_userInsert['ub_updated_time'] = date('Y-m-d H:i:s', time());
                    $a_NewUsers[] = $a_userInsert;
                    unset($a_userInsert);
                }
            }
            //end check duplicate in file
            //start process
            if (!isset($a_Respon)) {
                if (isset($a_NewUsers) && Util::b_fCheckArray($a_NewUsers)) {
                    //Get total of new users
                    $i_TotalNewUsers = count($a_NewUsers);
                    //Insert new user into db
                    if (DB::table('b_o_categories')->insert($a_NewUsers)) {
                        $a_Respon['insert'] = "Inserted $i_TotalNewUsers successfully! <br/>";
                    } else {
                        $a_Respon['insert'] = "Insert new users failed! <br/>";
                    }
                } else {
                    $a_Respon['insert'] = "No any new users found! <br/>";
                }
                
            }
            //end process

        });
        return $a_Respon;

    }
    /**

     * @auth: Dienct
     * @since: 12/09/2018
     * @des: process import building
     *      */
    public function ImportExcelProduct($dirFile) {
        set_time_limit(5500);
        $a_Error = array();
        //Get all users from db
        $o_Result = Excel::selectSheetsByIndex(0)->load($dirFile, function($reader) use ( &$a_Respon) {
            $a_NewUsers = array();
            $k = time();
            $o_building = DB::table('b_o_categories')->where('cb_status', 1)->where('cb_level', 2)->get();
            foreach ($o_building as $val){
                if(strlen($val->cb_code) == 6){
                    $a_Building[$val->cb_code] = $val->cb_id;
                }
            }
            foreach ($reader->toArray() as $key => $row) {
                if(strlen($row['property']) == 12 ){
                    $a_userInsert = array();
                    $a_userInsert['pb_id'] = (int) $k += 1;
                    $a_userInsert['pb_status'] = 1;
                    $a_userInsert['pb_title'] = 'title';
                    $a_userInsert['status_id'] = Product::STATUSES[$row['status']];
                    $a_userInsert['pb_code'] =  $row['property'];
                    $a_userInsert['category_id'] =  isset($a_Building[$row['block']]) ? $a_Building[$row['block']] : null;
                    DB::table('b_o_products')->insert($a_userInsert);
//                    $a_NewUsers[] = $a_userInsert;
                    unset($a_userInsert);
                }
            }
            //end check duplicate in file
            //start process
            if (!isset($a_Respon)) {
                if (isset($a_NewUsers) && Util::b_fCheckArray($a_NewUsers)) {
                    //Get total of new users
                    $i_TotalNewUsers = count($a_NewUsers);
                    //Insert new user into db
                    if (DB::table('b_o_products')->insert($a_NewUsers)) {
                        $a_Respon['insert'] = "Inserted $i_TotalNewUsers successfully! <br/>";
                    } else {
                        $a_Respon['insert'] = "Insert new users failed! <br/>";
                    }
                } else {
                    $a_Respon['insert'] = "No any new users found! <br/>";
                }
                
            }
            //end process

        });
        return $a_Respon;

    }
    
    public function ImportExcelCustomer($dirFile, $fileName) {
        set_time_limit(600);
        $projectId = Input::get('project_id');
        $assign_id = Input::get('assign_id');        
        $note = Input::get('note');
        
        $a_Error = array();
        //Get all users from db
        $o_Result = Excel::selectSheetsByIndex(0)->load($dirFile, function($reader) use ($projectId, $assign_id, $note, $fileName, &$a_Respon) {
            // check Data excel
            $a_Respon = array();
            
            foreach ($reader->toArray() as $key => $row) {
                if($row['phone'] == '' ){
                    $a_Respon[] = 'Kiểm tra trường Phone dòng '. ($key + 1) .'<br/>';
                }
            }
            if(count($a_Respon) == 0){
                $o_CustomerData = DB::table('tmp_customer_data')->where('tc_created_by', Auth::guard('loginfrontend')->user()->ub_id)
                                                        ->where('tc_phone', $reader->toArray()[rand(0, count($reader->toArray()))]['phone'])
                                                        ->get();
                if(count($o_CustomerData) != 0)  $a_Respon[] = 'Có vẻ như bạn đã tải nhập một file cùng tên, và chứa nội dung tương tự file này';      
            }
            if(count($a_Respon) == 0){
                // insert to file table
                $k = $fileID = time();
                $assignerName = '';
                $userInGroup = array();
                if(count($assign_id) >0){
                    foreach($assign_id as $val){
                        $userInGroup[$val] = $val;
                        $assignerName .= isset(DB::table('b_o_users')->where('ub_id', $val)->first()->ub_account_tvc) ? '__'.DB::table('b_o_users')->where('ub_id', $val)->first()->ub_account_tvc : '';
                    }
                }                
                $json_assign_user_id = json_encode($userInGroup);
                
                $ary_customerFile = [
                    'cf_id' => $fileID,
                    'cf_title' => $fileName,
                    'pj_id' => $projectId,
                    'cf_assign_user_id' => $json_assign_user_id,
                    'cf_note' => $note,
                    'cf_created_by'=> Auth::guard('loginfrontend')->user()->ub_id ,
                    'cf_create_time' => date('Y-m-d H:i:s', time()),
                    'cf_assign_name' => $assignerName
                ];
                DB::table('b_o_customer_files')->insert($ary_customerFile);
                
                $a_NewUsers = array();
                
                foreach ($reader->toArray() as $key => $row) {
                        $a_CustomerInsert = array();
                        $a_CustomerInsert['tc_id'] = (int) $k += 1;
                        $a_CustomerInsert['tc_name'] = $row['name'];
                        $a_CustomerInsert['tc_email'] = $row['email'];
                        $a_CustomerInsert['tc_phone'] = $row['phone'];
                        $a_CustomerInsert['tc_status'] = 1;
                        $a_CustomerInsert['project_id'] = $projectId;
                        $a_CustomerInsert['tc_file_name'] = $fileName;
                        $a_CustomerInsert['tc_staff_ids'] = $json_assign_user_id;
                        $a_CustomerInsert['cf_id'] = $fileID;
                        $a_CustomerInsert['tc_created_by'] = Auth::guard('loginfrontend')->user()->ub_id;
                        $a_CustomerInsert['tc_create_time'] = date('Y-m-d H:i:s', time());
                        $a_CustomerInsert['cf_assign_name'] = $assignerName;
                        $a_NewUsers[] = $a_CustomerInsert;
                        unset($a_CustomerInsert);
                }                
                if (isset($a_NewUsers) && Util::b_fCheckArray($a_NewUsers)) {
                    //Get total of new Customers
                    $i_TotalNewUsers = count($a_NewUsers);
                    //Insert new Customer into db
                    if (DB::table('tmp_customer_data')->insert($a_NewUsers)) {
                        $a_Respon['insert'] = "Inserted $i_TotalNewUsers successfully! <br/>";
                    } else {
                        $a_Respon['insert'] = "Insert new Customers failed! <br/>";
                    }
                } else {
                    $a_Respon['insert'] = "No any new Customers found! <br/>";
                }
            }
        });
        return $a_Respon;

    }
    
    
    
    
}
