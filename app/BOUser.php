<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Illuminate\Support\Facades\Input;

class BOUser extends Authenticatable implements JWTSubject
{
    use Notifiable;
    public $timestamps = false;
    const ID_KEY = 'ub_id';

    const ACTIONS =  [
        'view' => 'VIEW',
        'approve' => 'APPROVE',
        'book' => 'BOOK',
        'update' => 'UPDATE',
        'export' => 'EXPORT',
        'mod' => 'MOD',
        'admin'=> 'ADMIN'
    ];
//    protected $table = "b_o_users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ub_id', 'ub_email', 'password', 'ub_status', 'ub_title', 'ub_account_name', 'ub_created_time'
    ];
    protected $casts = [
        'group_ids' => 'array'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * @param $value
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getUbAvatarAttribute($value) {
        return $value? url($value) : Util::generateAvatarByNameInitials($this->ub_title, substr($this->ub_id, -1));
    }

    /**
     * @param $value
     * @return string
     */
    public function getAvatarAttribute($value) {
        if ($value) return asset($value);
        return $this->name? Util::generateAvatarByNameInitials($this->name, substr($this->ub_id, -1)) : '';
    }


    /**
     * @param string $key
     * @param mixed $value
     * @return \Illuminate\Support\Carbon|mixed
     */
    public function setAttribute($key, $value) {
        switch ($key) {
            case 'ub_last_logged_time':
            case 'ub_created_time':
                $this->attributes[$key] = $value?? now();
                break;
            case self::ID_KEY:
                $this->attributes[$key] = $value?? time();
                break;
            case 'tvc_key':
                $this->attributes[$key] = $value?? null;
                break;
            default:
                $this->attributes[$key] = $value;
        }
    }

    /** Join staff department
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group() {
        return $this->belongsTo(BOUserGroup::class, "group_ids", BOUserGroup::ID_KEY);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @return mixed
     */
    private static function getAdminGroup() {
        return BORoleGroup::find(1);
    }

    /**
     * @param bool $api_gate
     * @return bool
     */
    public static function is_admin($api_gate=false) {
        $role_group = self::getAdminGroup();
        if (!$role_group) return false;
        $staff = $role_group->rg_staff_ids? array_values($role_group->rg_staff_ids) : [];
        return in_array(self::getCurrentUID($api_gate), $staff);
    }

    public function ImportExcel($dirFile) {
        set_time_limit(600);
        $a_Error = array();
        //Get all users from db
        $a_DbUser = DB::table('b_o_users')->select('ub_staff_code', 'ub_account_tvc')->get();
        $a_ub_account_tvc = array();
        $a_DbUsersCode = array();
        if (Util::b_fCheckArray($a_DbUser)) {
            foreach ($a_DbUser as $o_DbUser) {
                $a_ub_account_tvc[] = trim($o_DbUser->ub_account_tvc);
                $a_DbUsersCode[] = trim($o_DbUser->ub_staff_code);
            }
        }

        $o_Result = Excel::selectSheetsByIndex(0)->load($dirFile, function($reader) use ($a_ub_account_tvc, $a_DbUsersCode, &$a_Respon) {
            $a_NewUsers = array();
            $a_NeedUpdateUsers = array();

            $k = time();
            foreach ($reader->toArray() as $key => $row) {

                //check field
                $i_Line = $key + 2;
                if (isset($row['ub_tvc_code']) && $row['ub_tvc_code'] != "") {
                    if (!isset($row['ub_staff_code']) || $row['ub_staff_code'] ==''){
                        $a_Respon['Err_code'] = "Kiểm tra lại trường ub_staff_code trong file excel dòng $i_Line <br/>";
                    }

                    if (in_array(trim($row['ub_account_tvc']), $a_ub_account_tvc)) {
                        // Array Update for user
                            $a_userUpdate = array();
                            $a_userUpdate['ub_title'] = $row['ub_title'];
                            $a_userUpdate['ub_staff_code'] = $row['ub_staff_code'];
                            $a_userUpdate['ub_account_tvc'] = $row['ub_account_tvc'];
                            $a_userUpdate['ub_tvc_code'] = $row['ub_tvc_code'];
                            $a_userUpdate['ub_account_name'] = $row['username'];
                            $a_userUpdate['ub_email'] = $row['email'];
                            $a_NeedUpdateUsers[] = $a_userUpdate;
                            unset($a_userUpdate);

                    } else {
                        // Array Insert new user
                                $a_userInsert = array();
                                $a_userInsert['ub_title'] = $row['ub_title'];
                                $a_userInsert['ub_id'] = (int) $k += 1;
                                $a_userInsert['ub_status'] = 1;
                                $a_userInsert['ub_staff_code'] = $row['ub_staff_code'];
                                $a_userInsert['ub_account_tvc'] = $row['ub_account_tvc'];
                                $a_userInsert['ub_tvc_code'] = $row['ub_tvc_code'];
                                $a_userInsert['ub_account_name'] = $row['username'];
                                $a_userInsert['ub_email'] = $row['email'];
                                $a_userInsert['ub_created_time'] = date('Y-m-d H:i:s', time());

                                $a_NewUsers[] = $a_userInsert;
                                unset($a_userInsert);
                    }

                }
            }



            //end check duplicate in file
            //start process
            if (!isset($a_Respon)) {
                if (isset($a_NewUsers) && Util::b_fCheckArray($a_NewUsers)) {
                    //Get total of new users
                    $i_TotalNewUsers = count($a_NewUsers);
                    //Insert new user into db
                    if (DB::table('b_o_users')->insert($a_NewUsers)) {
                        $a_Respon['insert'] = "Inserted $i_TotalNewUsers successfully! <br/>";
                    } else {
                        $a_Respon['insert'] = "Insert new users failed! <br/>";
                    }
                } else {
                    $a_Respon['insert'] = "No any new users found! <br/>";
                }
                //Check to update
                if (isset($a_NeedUpdateUsers) && Util::b_fCheckArray($a_NeedUpdateUsers)) {
                    //Get total of new users
                    $i_UpdateSuccessfully = 0;
                    $i_UpdateFail = 0;
                    foreach ($a_NeedUpdateUsers as $a_UpdateUser) {
                        $sz_WhereEmail = trim($a_UpdateUser['ub_staff_code']);
                        unset($a_UpdateUser['ub_staff_code']);
                        if (DB::table('b_o_users')->where('ub_staff_code', $sz_WhereEmail)->update($a_UpdateUser)) {
                            $i_UpdateSuccessfully++;
                        } else {
                            $i_UpdateFail++;
                        }
                    }
                    $a_Respon['update'] = "Updated $i_UpdateSuccessfully user(s) successfully! And $i_UpdateFail failed! <br/>";
                } else {
                    $a_Respon['update'] = "No any existed user to update found!<br/>";
                }
            }
            //end process

        });
        return $a_Respon;

    }
    public function ImportUserGroups($dirFile) {
        set_time_limit(600);
        $a_Error = array();
        //Get all users from db
        

        $o_Result = Excel::selectSheetsByIndex(0)->load($dirFile, function($reader) use ( &$a_Respon) {
            $a_NewUsers = array();
            $a_NeedUpdateUsers = array();

            $k = time();
            foreach ($reader->toArray() as $key => $row) {
                $a_userInsert = array();
                $a_userInsert['reference_code'] = $row['tvc_code'];
                $a_userInsert['gb_id'] = (int) $k += 1;
                $a_userInsert['gb_status'] = 1;
                $a_userInsert['gb_title'] = $row['title'];
                $a_userInsert['gb_created_time'] = date('Y-m-d H:i:s', time());
                $a_userInsert['gb_updated_time'] = date('Y-m-d H:i:s', time());
                $a_NewUsers[] = $a_userInsert;
                unset($a_userInsert);                
            }
            
            
            //end check duplicate in file
            //start process
            if (!isset($a_Respon)) {
                if (isset($a_NewUsers) && Util::b_fCheckArray($a_NewUsers)) {
                    //Get total of new users
                    $i_TotalNewUsers = count($a_NewUsers);
                    //Insert new user into db
                    if (DB::table('b_o_user_groups')->insert($a_NewUsers)) {
                        $a_Respon['insert'] = "Inserted $i_TotalNewUsers successfully! <br/>";
                    } else {
                        $a_Respon['insert'] = "Insert new users failed! <br/>";
                    }
                } else {
                    $a_Respon['insert'] = "No any new users found! <br/>";
                }
                
            }
            //end process

        });
        return $a_Respon;

    }
    
    public function ImportUserChild($dirFile) {
        set_time_limit(600);
        $a_Error = array();
        //Get all users from db
        

        $o_Result = Excel::selectSheetsByIndex(0)->load($dirFile, function($reader) use ( &$a_Respon) {
            $a_NewUsers = array();
            $a_NeedUpdateUsers = array();
            $a_groups = DB::table('b_o_user_groups')->select('gb_id', 'reference_code')->where('gb_status', 1)->get();
            foreach($a_groups as $val){
                $a_UserGroup[$val->reference_code] = $val->gb_id;
            }
            
            $k = (int)time() + 3600;
            
            foreach ($reader->toArray() as $key => $row) {
                $a_userInsert = array();
                $a_userInsert['ub_title'] = $row['ub_title'];
                $a_userInsert['ub_id'] = (int) $k += 1;
                $a_userInsert['ub_status'] = 1;
                $a_userInsert['ub_tvc_code'] = $row['ub_tvc_code'];
                $a_userInsert['ub_account_tvc'] = $row['ub_account_tvc'];
                $a_userInsert['group_ids'] = $a_UserGroup[$row['san']];
                $a_userInsert['ub_created_time'] = date('Y-m-d H:i:s', time());
                $a_NewUsers[] = $a_userInsert;
                unset($a_userInsert);                
            }
            
            
            //end check duplicate in file
            //start process
            if (!isset($a_Respon)) {
                if (isset($a_NewUsers) && Util::b_fCheckArray($a_NewUsers)) {
                    //Get total of new users
                    $i_TotalNewUsers = count($a_NewUsers);
                    //Insert new user into db
                    if (DB::table('b_o_users')->insert($a_NewUsers)) {
                        $a_Respon['insert'] = "Inserted $i_TotalNewUsers successfully! <br/>";
                    } else {
                        $a_Respon['insert'] = "Insert new users failed! <br/>";
                    }
                } else {
                    $a_Respon['insert'] = "No any new users found! <br/>";
                }
                
            }
            //end process

        });
        return $a_Respon;

    }
    /**

     * @auth: Dienct
     * @des: get all user in bo
     * @since: 08/09/2018
     *      */
    public static function getAllUserInBo(){        
        return DB::table('b_o_users')->select('*')->where('ub_status', 1)->get();
    }

    /**
     * @param bool $api_gate
     * @return int
     */
    public static function getCurrentUID($api_gate=true) {
        return $api_gate? (Auth::guard("api")->user()["ub_id"]?? 0) : (Auth::guard("loginfrontend")->user()->ub_id?? 0);
    }

    /**
     * @param null $attribute
     * @param bool $api_gate
     * @return null
     */
    public static function getCurrent($attribute = null, $api_gate=true) {
        if ($api_gate) {
            return $attribute? (Auth::guard("api")->user()[$attribute]?? null) : Auth::guard("api")->user();
        }else{
            return $attribute? (Auth::guard("loginfrontend")->user()->{$attribute}?? null) : Auth::guard("loginfrontend")->user();
        }
    }

    /**
     * @param null $field
     * @param bool $api_gate
     * @return mixed
     */
    public static function getCurrentDepartment($field = null, $api_gate=true) {
        $group_id = self::getCurrent('group_ids', $api_gate);
        /** @var $group */
        $group = BOUserGroup::where(BOUserGroup::ID_KEY, $group_id)->first();
        if ($field) {
            return $group->{$field};
        }
        return $group;
    }


    public static function getUserByCode($code=''){

        //$user = [];
        if($code)
        {
            return [];
        }
        $code = trim($code);
        $user = BOUser::where('ub_staff_code',$code )->where('ub_status','!=',-1)->first();
        return $user;

    }
    
    /**

     * @Auth: Dienct
     * @Des: Add/edit project
     * @Since: 17/09/2017
     */
    
    public function ProcessAddEditUser($id) {
        $a_DataUpdate = array();
        
        $a_DataUpdate['ub_title'] = Input::get('ub_title');
        $a_DataUpdate['group_ids'] = Input::get('group_ids');
        $a_DataUpdate['ub_email'] = Input::get('ub_email');
        $aryAcount = explode("@",Input::get('ub_email'));
        $a_DataUpdate['ub_account_name'] = $aryAcount[0];
        $a_DataUpdate['ub_staff_code'] = Input::get('ub_staff_code');
        $a_ub_info_images = Input::get('ub_info_images');
        $a_filename = Input::get('filename');
        $a_deleteUrl = Input::get('deleteUrl');
        if(is_array($a_ub_info_images) && count($a_ub_info_images) > 0){
            foreach ($a_ub_info_images as $key => $val){
                $aryIMG['img'] = $val;
                $aryIMG['filename'] = $a_filename[$key];
                $aryIMG['deleteUrl'] = $a_deleteUrl[$key];
                $aryReturn[] = $aryIMG;
            }
        }
        $a_DataUpdate['password'] =  Input::get('password') != '' ? bcrypt(Input::get('password')) : '';
        if(isset($aryReturn)){
            $a_DataUpdate['ub_info_images'] = json_encode($aryReturn);
        }
        
        $a_DataUpdate['ub_status'] = Input::get('ub_status') == 'on' ? 1 : 0;
        
        if (is_numeric($id) == true && $id != 0) {
            DB::table('b_o_users')->where('id', $id)->update($a_DataUpdate);
            
        } else {
            $a_DataUpdate['ub_id'] = time();
            $a_DataUpdate['ub_created_time'] = date('Y-m-d H:i:s', time());
            DB::table('b_o_users')->insert($a_DataUpdate);
        }

    }
    
    /**
     * @Auth: Dienct
     * @Des: 
     * @Since: 2/3/2017
     */
    public function getUserById($id) {
        
        $a_Data = DB::table('b_o_users')->where('id', $id)->first();
        $a_Data->ub_info_images  = json_decode($a_Data->ub_info_images);
        return $a_Data;
    }
    public function getUserByUbId($id) {
        
        $a_Data = DB::table('b_o_users')->where('ub_id', $id)->first();
        $a_Data->ub_info_images  = json_decode($a_Data->ub_info_images);        
        return $a_Data;
    }
    /**
     * @Auth:Dienct
     * @Des: get all record news table
     * @Since: 16/08/2018
     */
    public function getAllSearch() {
        $a_data = array();
        $o_Db = DB::table('b_o_users')->select('*');
        $a_data = $o_Db->where('ub_status', '!=', -1);
        $a_search = array();
        //search

        $sz_pm_title = Input::get('ub_title', '');
        if ($sz_pm_title != '') {
            $a_search['ub_title'] = $sz_pm_title;
            $a_data = $o_Db->where('ub_title', 'like', '%' . $sz_pm_title . '%');
        }
        
        $i_branch = Input::get('branch_id', '');
        if ($i_branch != '') {
            $a_search['branch_id'] = $i_branch;
            $a_data = $o_Db->where('group_ids', $i_branch);
        }
        
        $a_data = $o_Db->orderBy('ub_created_time', 'desc')->paginate(30);

        foreach ($a_data as $key => &$val) {
            $val->stt = $key + 1;
        }
        $a_return = array('a_data' => $a_data, 'a_search' => $a_search);
        return $a_return;
    }
    
    /**

     * @author DienChuTien
     * @des: login user frontend
     * @since: 07/09/2017
     *      */
    public function loginUser($username,$password){
        if (Auth::guard('loginfrontend')->attempt(['ub_account_tvc' => $username, 'password' => $password ])) {
            return true;
        }else{
            return false;
        }
        
        
        
    }
    
}
