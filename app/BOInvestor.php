<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BOInvestor extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories() {
        return $this->hasMany(BOCategory::class, 'investor_id', 'id');
    }
}
