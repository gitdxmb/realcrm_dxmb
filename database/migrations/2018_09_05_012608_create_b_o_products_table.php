<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBOProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_o_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pb_id')->unique();
            $table->integer('pb_status')->default(0);
            $table->string('pb_title');
            $table->integer('product_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->string('pb_code')->nullable();
            $table->double('pb_required_money')->nullable();
            $table->json('viewed_group_ids')->nullable()->comment('nhóm quyền view');
            $table->json('booked_group_ids')->nullable()->comment('nhóm quyền book');
            $table->json('approved_group_ids')->nullable()->comment('nhóm quyền duyệt');
            $table->integer('category_id')->nullable();
            $table->string('pd_reference_codes')->nullable();
            $table->string('pb_door_direction')->nullable();
            $table->string('pb_balcony_direction')->nullable();
            $table->json('extra_ids')->nullable();
            $table->double('pb_price_total')->nullable();
            $table->double('pb_price_per_s')->nullable();
            $table->double('pb_used_s')->nullable();
            $table->double('pb_built_up_s')->nullable();
            $table->json('permission_group_ids')->nullable();
            $table->json('permission_staff_ids')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_o_products');
    }
}
