<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBOUSerPhone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_users', function (Blueprint $table) {
            $table->string('ub_phone')->nullable()->after('ub_email')->comment('SĐT người dùng');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_users', function (Blueprint $table) {
            $table->dropColumn('ub_phone');
        });
    }
}
