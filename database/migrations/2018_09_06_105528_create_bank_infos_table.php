<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->unique();
            $table->tinyInteger('bank_status')->default(0);
            $table->string('bank_code')->nullable();
            $table->string('bank_title');
            $table->string('bank_holder')->nullable()->comment('Chủ tài khoản');
            $table->string('bank_number')->nullable()->comment('Số tài khoản');
            $table->integer('branch_id')->nullable()->comment('ID chi nhánh ngân hàng');
            $table->json('payment_method_ids')->nullable()->comment('Phương thức thanh toán');
            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->timestamp('bank_created_time')->nullable();
            $table->timestamp('bank_updated_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_infos');
    }
}
