<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBOCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_o_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cb_id');
            $table->integer('cb_status')->default(0);
            $table->integer('user_id')->nullable();
            $table->string('cb_name')->nullable();
            $table->integer('source_id')->nullable()->comment('nguồn khách hàng (thư ký phân bổ/ tự nhập,..)');
            $table->string('cb_account')->nullable()->comment('= sđt');
            $table->integer('cb_staff_id')->nullable()->comment('nhân viên quản lý');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_o_customers');
    }
}
