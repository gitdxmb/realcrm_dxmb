<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBONotificationTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_o_notification_targets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('target_id');
            $table->integer('target_type')->default(0)->comment('0 - bo_user; 1 - boCustomer');
            $table->integer('notification_id');
            $table->boolean('is_read')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_o_notification_targets');
    }
}
