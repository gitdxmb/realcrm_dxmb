<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBOPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_o_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pb_id')->unique();
            $table->tinyInteger('pb_status')->default(0);
            $table->string('pb_code')->nullable();
            $table->string('reference_code')->nullable()->comment('= payment tavico (nếu có)');
            $table->string('bill_code')->nullable();
            $table->text('pb_note')->nullable();
            $table->integer('customer_id');
            $table->integer('request_staff_id');
            $table->timestamp('request_time')->nullable();
            $table->double('pb_request_money')->nullable();
            $table->timestamp('pb_response_time')->nullable();
            $table->integer('response_staff_id')->nullable();
            $table->double('pb_response_money')->nullable();
            $table->integer('method_id')->nullable();
            $table->string('pb_bank_number_id')->nullable();
            $table->string('pb_images')->nullable();
            $table->text('pb_log_edit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_o_payments');
    }
}
