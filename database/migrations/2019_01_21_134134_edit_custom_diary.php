<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditCustomDiary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_customer_diaries', function (Blueprint $table) {
            $table->integer('tmp_id')->nullable();
            $table->integer('cd_category')->nullable()->comment('Dự án liên quan');
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_customer_diaries', function (Blueprint $table) {
            $table->dropColumn('tmp_id');
            $table->dropColumn('cd_category');
            $table->dropColumn('created_at');
        });
    }
}
