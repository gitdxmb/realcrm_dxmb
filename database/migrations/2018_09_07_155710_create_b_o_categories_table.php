<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBOCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_o_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cb_id')->unique();
            $table->tinyInteger('cb_status')->default(1);
            $table->tinyInteger('parent_id')->nullable()->comment('Id danh mục cha');
            $table->string('cb_code')->nullable();
            $table->string('reference_code')->nullable()->comment('Code trên tavico');
            $table->string('cb_title')->nullable();
            $table->text('cb_description')->nullable();
            $table->json('extra_ids')->nullable()->comment('Thông tin thêm (nối với bảng Extra)');
            $table->integer('updated_user_id')->nullable();
            $table->timestamp('ub_updated_time')->nullable();
            $table->integer('created_user_id')->nullable();
            $table->timestamp('ub_created_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_o_categories');
    }
}
