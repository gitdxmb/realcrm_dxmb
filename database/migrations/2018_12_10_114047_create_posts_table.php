<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description')->nullable()->comment('Mô tả ngắn');
            $table->text('content')->comment('Nội dung bài đăng/trích dẫn');
            $table->tinyInteger('type')->default(0)->comment('0-tin ảnh; 1-tin video; 2-quote');
            $table->tinyInteger('priority')->default(0)->comment('0-không ưu tiên; 1-nổi bật; 2-ghim lên đầu');
            $table->string('image')->nullable()->comment('Ảnh mô tả');
            $table->json('bo_category_ids')->nullable()->comment('Dự án liên quan');
            $table->string('tags')->nullable()->comment('Thẻ');
            $table->integer('creator_department')->nullable()->comment('Phòng ban người tạo');
            $table->integer('feedback_type')->default(0)->comment('Loại phản hồi: 0-chỉ văn bản, 1-bình chọn, 2-Check in');
            $table->json('feedback_options')->nullable()->comment('Các lựa chọn để vote');
            $table->tinyInteger('status')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
