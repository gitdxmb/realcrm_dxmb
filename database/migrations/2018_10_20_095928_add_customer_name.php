<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_customer_mappings', function (Blueprint $table) {
            $table->string('c_name')->nullable()->after('c_title')->comment('Tên khách hàng trên CMND');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_customer_mappings', function (Blueprint $table) {
            $table->dropColumn('c_name');
        });
    }
}
