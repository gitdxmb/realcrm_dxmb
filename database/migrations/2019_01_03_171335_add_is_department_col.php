<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsDepartmentCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('template_email', function (Blueprint $table) {
            if (!Schema::hasColumn('template_email', 'is_apartment')) {
                $table->tinyInteger('is_apartment')->nullable()->comment('true: Chung cư; false: Đất nền');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_email', function (Blueprint $table) {
            $table->dropColumn('is_apartment');
        });
    }
}
