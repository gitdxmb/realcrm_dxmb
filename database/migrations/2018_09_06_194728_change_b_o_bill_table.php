<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBOBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_bills', function (Blueprint $table) {
            $table->dropColumn("book_request_id");
            $table->tinyInteger("type")->default(0)->comment("0: Đặt chỗ; 1: Chuyển đặt cọc");
            $table->dropColumn("trans_log_edit");
            $table->json("bill_log_edit")->nullable()->comment("Log thay đổi trạng thái, loại hóa đơn");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_bills', function (Blueprint $table) {
            $table->integer("book_request_id");
            $table->text("trans_log_edit");
            $table->dropColumn("bill_log_edit");
            $table->dropColumn("type");
        });
    }
}
